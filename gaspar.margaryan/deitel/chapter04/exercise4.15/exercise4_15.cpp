/// Exercise4_15
/// Program for company to pay its salespeople on a commission basis.
#include <iostream>
#include <iomanip>   /// input output manipulation header for using setprecision () manipulator
#include <unistd.h>

int
main()
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            /// prompting user to input sales or -1 to exit the program
            std::cout << "Enter sales in dollars or -1 to quit! " << std::endl;
        }
        double grossSales = 0;
        std::cin >> grossSales;  /// it is able to input values with decimal point
        if (grossSales < -1) {
            /// when inputed invalid value the program ends
            std::cout << "Error 1: Invalid sales value. The value should be 0 or above! " << std::endl;
            return 1;
        }
        if (-1 == grossSales) {
            /// end the program when inputes -1 sentinel number
            std::cout << "Thank you for using our program " << std::endl;
            return 0;
        }
        double salary = (200.00 + grossSales * 0.09);
        std::cout << "Salary is $" << std::setprecision (2) << std::fixed << salary << std::endl;
        /// prints value with double precision after decimal point
    }
    return 0; /// indicate successful termination
} /// end main

