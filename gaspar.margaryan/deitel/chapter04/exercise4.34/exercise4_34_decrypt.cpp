/// Exercise 4.34
/// Program that input digit integer value, encrypt and decrypt it back
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) { /// prompt user to enter encrypted number
        std::cout << "Please enter encrypted value " << std::endl;
    }
    int decrypt;
    std::cin >> decrypt;
    if (decrypt < 0) { /// if entered integer is less than 0 stop the program and print error message
        /// inputed number less than 4 digit will be read as 4 digit (e.g. 12 means 0012)
        std::cout << "Error 2: Invalid number! " << std::endl;
        return 1;
    }
    if (decrypt > 9999) {
        std::cout << "Error 2: Invalid number! " << std::endl;
        return 1;
    }
    int decDigit1 = decrypt % 10;
    int decDigit2 = decrypt % 100 / 10;
    int decDigit3 = decrypt % 1000 / 100;
    int decDigit4 = decrypt / 1000;
    std::cout << "Decrypted number is ";
    std::cout << (decDigit2 < 7 ? decDigit2 + 3 : decDigit2 - 7);
    std::cout << (decDigit1 < 7 ? decDigit1 + 3 : decDigit1 - 7);
    std::cout << (decDigit4 < 7 ? decDigit4 + 3 : decDigit4 - 7);
    std::cout << (decDigit3 < 7 ? decDigit3 + 3 : decDigit3 - 7) << std::endl;
    return 0; /// indicate successful termination for wrong result
} /// end main

