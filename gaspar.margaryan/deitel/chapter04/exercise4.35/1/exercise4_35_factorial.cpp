/// Exercise 4.35
/// Program that input an nonnegative integer value and make some mathematical calculations
#include <iostream>   /// input output standard library header
#include <iomanip>   ///  include input output manipulator
#include <unistd.h>

int
main()
{
    std::cout << "1. A program that compute and print the factorial of entered number \n" << std::endl;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter nonnegative number: " << std::endl;
    } /// prompt user to enter nonnegative number

    int nValue;
    std::cin >> nValue;
    if (nValue < 0) { /// if entered integer is less than 0 stop the program and print error message
        std::cout << "Error 1: Invalid number! The number should be positive. " << std::endl;
        return 1;
    }
    int controlNumber = nValue;  /// integer for iteration
    int nFactorial = 1;  /// initialize factroial number for entered "n" number
    while (controlNumber != 0) {
        nFactorial *= controlNumber;
        controlNumber--;
    }
    if (nFactorial <= 0) { ///  when entered number's factorial get value out of rage of integer type
        std::cout << "Error 2: Out of limit for variable type integer. " << std::endl;
        return 2;
    }
    std::cout << "The factorial of number " << nValue << " = " << nFactorial << std::endl;

    return 0; /// indicate successful termination for wrong result
} /// end main

