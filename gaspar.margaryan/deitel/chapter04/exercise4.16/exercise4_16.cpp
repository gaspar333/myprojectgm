/// Exercise4_16
/// Program for determining gross payment for employees
#include <iostream>
#include <iomanip>
#include <unistd.h>

int
main()
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            /// prompting user to input hours worked or -1 to exit the program
            std::cout << "Enter hours worked or -1 to quit! " << std::endl;
        }
        double hoursWorked = 0;
        std::cin >> hoursWorked;
        if (hoursWorked < -1) {
            /// when inputed invalid value the program ends
            std::cout << "Error 1: Invalid working hours value. The value should be 0 or above! " << std::endl;
            return 1;
        }
        if (-1 == hoursWorked) {
            /// end the program when inputes -1 sentinel number
            std::cout << "Thank you for using our program. " << std::endl;
            return 0;
        }
        if (::isatty(STDIN_FILENO)) {
            /// prompting user to input hourly rate
            std::cout << "Enter hourly rate of the worker ($00.00) " << std::endl;
        }
        double hourRate = 0;
        std::cin >> hourRate;  /// can be inputed value with decimal point
        if (hourRate < 0) {
            /// when inputed invalid value the program ends
            std::cout << "Error 2: Invalid hourly rate value: The value should be positive! " << std::endl;
            return 2;
        }
        double salary = hourRate * (hoursWorked <= 40 ? hoursWorked : hoursWorked * 1.5 - 20);
    
        std::cout << "Salary is $" << std::setprecision (2) << std::fixed << salary << std::endl;
        /// prints value with double precision after decimal point
    }
    return 0; /// indicate successful termination
} /// end main

