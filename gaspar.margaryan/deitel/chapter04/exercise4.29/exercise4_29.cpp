/// Exercise 4.29
/// Program that a program that prints the powers of the integer 2
#include <iostream>
#include <unistd.h>

int
main()
{
    int number = 2; /// initialize number
    while (true) { /// loop
        std::cout << number << ", ";
        number *= 2;
        if (0 == number) {
            std::cout << "\nError 1: Integer overflow " << std::endl;
            return 1;
        }
    } /// end while
    return 0; /// indicate successful termination
} /// end main

