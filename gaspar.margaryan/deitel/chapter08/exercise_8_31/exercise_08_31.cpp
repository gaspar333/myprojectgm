/// Exercise_08_31.cpp
/// program that input two strings from user
/// get a number of characters and compares strings
/// up to that characters number
#include <iostream>
#include <cstring>
#include <unistd.h>

const int CAPACITY = 20;

void getStrings(char* const& string1, char* const& string2);
void compareStrings(const char* const& string1, const char* const& string2);

int
main()
{
    char string1[CAPACITY] = {};
    char string2[CAPACITY] = {};
    getStrings(string1, string2);
    compareStrings(string1, string2);
    return 0; 
}

inline void
getStrings(char* const& string1, char* const& string2)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input two strings to compare: ";
    }
    std::cin.getline(string1, CAPACITY, '\n');
    std::cin.getline(string2, CAPACITY, '\n');
}

inline void
compareStrings(const char* const& string1, const char* const& string2)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input number of characters to compare: ";
    }
    size_t number;
    std::cin >> number;
    char str1Trunc[CAPACITY] = {};
    char str2Trunc[CAPACITY] = {};
    std::strncat(str1Trunc, string1, number);
    std::strncat(str2Trunc, string2, number);
    const int result = std::strncmp(string1, string2, number);
    if (result > 0) {
        std::cout << "String1 \"" << str1Trunc << "\" " << "is grater than String2 \"" << str2Trunc << "\".";
        return;
    }
    if (result < 0) {
        std::cout << "String1 \"" << str1Trunc << "\" " << "is less than String2 \"" << str2Trunc << "\".";
        return;
    }
    std::cout << "String1 \"" << str1Trunc << "\" " << "and String2 \"" << str2Trunc << "\" are equal.";
    return;
}

