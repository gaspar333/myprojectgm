/// Exercise_08_13.cpp
/// Card shuffling and dealing program.
/// Deal two hands and compare them
#include <iostream>
#include <cstdlib> /// prototypes for rand
#include <unistd.h>

#include "DeckOfCards.hpp" /// DeckOfCards class definition

int
main()
{
    int seed;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input seed for random number generation: ";
    }
    std::cin >> seed;
    std::srand(seed);
    DeckOfCards deckOfCards; /// create DeckOfCards object
    deckOfCards.shuffleAndDeal(); /// shuffle the cards in the deck and deal hands
    deckOfCards.compareHands();
    
    return 0; /// indicates successful termination
} /// end main

