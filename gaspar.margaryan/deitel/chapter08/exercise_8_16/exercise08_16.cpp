/// Exercise_08_13.cpp
/// Member-function definitions for class DeckOfCards that simulates
/// the shuffling and dealing of a deck of playing cards.
#include <iostream>
#include <iomanip>
#include <cstdlib> /// prototypes for rand
#include <cassert>

#include "DeckOfCards.hpp" /// DeckOfCards class definition
/// DeckOfCards default constructor initializes deck

DeckOfCards::DeckOfCards()
{
    topCardOfDeck_ = deck_; /// a pointer to the top card of the deck
    for (int card = 0; card < CARDS_COUNT_; ++card) {
        deck_[card] = card; /// initialize not shuffled slot of deck by order 
    }
    for (int card = 0; card < HAND_COUNT_; ++card) {
        firstHand_[card] = -1; /// initialize empty slot of the first hand to -1
        secondHand_[card] = -1; /// initialize empty slot of the second hand to -1
    }
} /// end DeckOfCards default constructor

void
DeckOfCards::shuffleAndDeal()
{
    shuffle();
    dealAHand(firstHand_);
    dealAHand(secondHand_);
}

void 
DeckOfCards::compareHands()
{
    const Rank firstHandRank = getRank(firstHand_);
    const Rank secondHandRank = getRank(secondHand_);
    if (ROYAL_FLUSH == firstHandRank && ROYAL_FLUSH == secondHandRank) {
        std::cout << "It's draw! The pot is split!" << std::endl;
    }
    if (firstHandRank > secondHandRank) {
        std::cout << "First hand beats!" << std::endl;
        return;
    }
    if (firstHandRank < secondHandRank) {
        std::cout << "Second hand beats!" << std::endl;
        return;
    }
    const int firstHandHigherFace = getHighestFace(firstHand_, firstHandRank);
    const int secondHandHigherFace = getHighestFace(secondHand_, secondHandRank);
    if (firstHandHigherFace > secondHandHigherFace) {
        std::cout << "First hand beats with higher face!" << std::endl;
        return;
    }
    if (firstHandHigherFace < secondHandHigherFace) {
        std::cout << "Second hand beats with higher face!" << std::endl;
        return;
    }
    std::cout << "It's draw!" <<std::endl;
    return;
}

/// shuffle cards in deck
void
DeckOfCards::shuffle()
{
    /// for each of the 52 cards, choose a slot of the deck randomly
    for (int card = 0; card < CARDS_COUNT_; ++card) {
        int randomCard = std::rand() % CARDS_COUNT_;
        /// swap current card with random placed card
        std::swap(deck_[card], deck_[randomCard]);
    } /// end for
} /// end function shuffle

/// deal a hand of cards from deck
void
DeckOfCards::dealAHand(int hand[])
{
    /// assert if deck is not dealed or fully dealed
    assert(-1 != *deck_ || -1 != *(deck_ + CARDS_COUNT_));
    /// assert if hand is dealed
    assert(-1 == *hand);
    /// deal a hand of cards from deck's bottom                       
    for (int handIndex = 0; handIndex < HAND_COUNT_; ++handIndex) {
        /// swap empty slots of the hand with dealed cards in the deck
        /// as if taking cards out from deck
        std::swap(*topCardOfDeck_, hand[handIndex]);
        ++topCardOfDeck_; /// this keeps track of the undealed top card of the deck
    }
    sortAHand(hand); /// sort the hand by face by increasing order
} /// end function deal

void
DeckOfCards::printHandInfo(const int hand[])
{
    assert(-1 != *hand); /// assert if the hand is not dealed
    static const char *SUITS[] = {"Hearts", "Diamonds", "Clubs", "Spades"};                                  
    static const char *FACES[] =
        {"Deuce", "Three", "Four", "Five", "Six", "Seven",
         "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"};
    /// print the hand of cards
    for (int card = 0; card < HAND_COUNT_; ++card) {
        std::cout << std::setw(5) << std::right << FACES[getFace(hand[card])]   
                  << " of " << std::setw(8) << std::left << SUITS[getSuit(hand[card])]
                  << '\n';
    }
    /// print checking info about winning ranked cards
    const Rank rank = getRank(hand);
    printHandRank(rank);
}

void
DeckOfCards::printHandRank(const Rank rank)
{   
    switch (rank) {
    case NO_RANK:        std::cout << "The hand is not ranked." << std::endl;                break;
    case PAIR:           std::cout << "There is a pair in the hand." << std::endl;           break;
    case TWO_PAIRS:      std::cout << "There are two pairs in the hand." << std::endl;       break;
    case THREE_OF_KIND:  std::cout << "There are three of a kind in the hand." << std::endl; break;
    case STRAIGHT:       std::cout << "There is straight in the hand." << std::endl;         break;
    case FLUSH:          std::cout << "There is flush in the hand." << std::endl;            break;
    case FULL_HOUSE:     std::cout << "There full house in the hand." << std::endl;          break;
    case FOUR_OF_KIND:   std::cout << "There are four of a kind in the hand." << std::endl;  break;
    case STRAIGHT_FLUSH: std::cout << "There is straight flush in the hand." << std::endl;   break;
    case ROYAL_FLUSH:    std::cout << "There is royal flush in the hand." << std::endl;      break;
    default: assert(0);                                                                      break;
    }
}

void
DeckOfCards::sortAHand(int hand[])
{
    assert(-1 != *hand); /// assert if hand is not dealed
    /// sorting a hand by faces
    for (int i = 0; i < HAND_COUNT_ - 1; ++i) {
        for (int j = i + 1; j < HAND_COUNT_; ++j) {
            if (getFace(hand[i]) > getFace(hand[j])) {
                std::swap(hand[i], hand[j]);
            }
        }
    }
    printHandInfo(hand);
}

inline int
DeckOfCards::getFace(const int card)
{
    assert(-1 != card);
    const int face = card % FACE_COUNT_;
    return face;
}

inline int
DeckOfCards::getSuit(const int card)
{
    assert(-1 != card);
    const int suit = card / FACE_COUNT_;
    return suit;
}

DeckOfCards::Rank
DeckOfCards::getRank(const int hand[])
{
    assert(-1 != *hand); /// assert if hand is not dealed
    if (havePair(hand)) {
        return PAIR;
    }
    if (haveTwoPairs(hand)) {
        return TWO_PAIRS;
    }
    if (haveThreeOfAKind(hand)) {
        return THREE_OF_KIND;
    }
    if (haveStraight(hand)) {
        return STRAIGHT;
    }
    if (haveFlush(hand)) {
        return FLUSH;
    }
    if (haveFullHouse(hand)) {
        return FULL_HOUSE;
    }
    if (haveFourOfAKind(hand)) {
        return FOUR_OF_KIND;
    }
    if (haveStraightFlush(hand)) {
        return STRAIGHT_FLUSH;
    }
    if (haveRoyalFlush(hand)) {
        return ROYAL_FLUSH;
    }
    return NO_RANK;
}

int
DeckOfCards::getHighestFace(const int hand[], const Rank rank)
{
    switch (rank) {
    case PAIR:
    case TWO_PAIRS:      return getPairsHighestFace(hand);
    case THREE_OF_KIND:
    case FULL_HOUSE:     return getThreeOfKindHighestFace(hand);
    case FOUR_OF_KIND:   return getFace(hand[2]);
    case NO_RANK:
    case FLUSH:
    case STRAIGHT:
    case STRAIGHT_FLUSH: return getFace(hand[HAND_COUNT_ - 1]);
    default:             assert(0);
    }
}

int
DeckOfCards::getPairsHighestFace(const int hand[])
{
    int highestFace = -1;
    for (int i = 0; i < HAND_COUNT_ - 1; ++i) {
        if (getFace(hand[i]) == getFace(hand[i + 1])) {
            highestFace = getFace(hand[i + 1]);
        }
    }
    return highestFace;
}

int
DeckOfCards::getThreeOfKindHighestFace(const int hand[])
{
    if (getFace(hand[0]) == getFace(hand[2])) {
        return getFace(hand[2]);
    }
    if (getFace(hand[1]) == getFace(hand[3])) {
        return getFace(hand[3]);
    }
    return getFace(hand[4]);
}

bool
DeckOfCards::havePair(const int hand[])
{
    assert(-1 != *hand); /// assert if hand is not dealed
    int pairIndex = 0;
    for (int i = 0; i < HAND_COUNT_ - 1; ++i) {
        if (getFace(hand[i]) == getFace(hand[i + 1])) {
            ++pairIndex;
            if (pairIndex > 1) {
                return false;
            } /// return false if there are more than one pair
        }
    }
    assert(0 != pairIndex || 1 != pairIndex);
    return pairIndex; /// aka return 1 or zero
}

bool
DeckOfCards::haveTwoPairs(const int hand[])
{
    assert(-1 != *hand); /// assert if hand is not dealed
    /// condition which returns false if the hand has three or four of a kind
    /// (for not being mistaken two pairs inside three and four of kinds)
    if ((getFace(hand[0]) == getFace(hand[2]) || getFace(hand[1]) == getFace(hand[3])) || getFace(hand[2]) == getFace(hand[4])) {
        return false;
    }
    int twoPairsIndex = 0;
    for (int i = 0; i < HAND_COUNT_ - 1; ++i) {
        if (getFace(hand[i]) == getFace(hand[i + 1])) {
            ++twoPairsIndex;
        }
    }
    return 2 == twoPairsIndex;
}

bool
DeckOfCards::haveThreeOfAKind(const int hand[])
{
    assert(-1 != *hand); /// assert if hand is not dealed
    /// condition which returns false if the hand has four of a kind
    /// (for not being mistaken the three of kind inside four of kind)
    if (getFace(hand[0]) == getFace(hand[3]) || getFace(hand[1]) == getFace(hand[4])) {
        return false;
    }
    /// for not being mistaken with full house
    if (getFace(hand[0]) == getFace(hand[1]) && getFace(hand[3]) == getFace(hand[4])) {
        return false;
    }
    if ((getFace(hand[0]) == getFace(hand[2]) || getFace(hand[1]) == getFace(hand[3])) || getFace(hand[2]) == getFace(hand[4])) {
        return true;
    }
    return false;
}

bool
DeckOfCards::haveStraight(const int hand[])
{
    assert(-1 != *hand); /// assert if hand is not dealed
    const int straightIndex = getFace(hand[0]);
    const int suit = getSuit(hand[0]);
    int suitIndex = 0;
    for (int i = 1; i < HAND_COUNT_; ++i) {
        if (suit == getSuit(hand[i])) {
            ++suitIndex;
        }
        /// return false if the hand has straight flush
        if (getFace(hand[i]) != straightIndex + i || SUIT_COUNT_ == suitIndex) {
            return false;
        }
    }
    return true;
}

bool
DeckOfCards::haveFlush(const int hand[])
{
    assert(-1 != *hand); /// assert if hand is not dealed
    const int flushIndex = getSuit(hand[0]);
    for (int i = 1; i < HAND_COUNT_; ++i) {
        if (flushIndex != getSuit(hand[i])) {
            return false;
        }
    }
    /// return false if the hand has royal flush
    if (getFace(hand[0]) + SUIT_COUNT_ == getFace(hand[HAND_COUNT_ - 1])) {
        return false;
    }
    return true;
}

bool
DeckOfCards::haveFullHouse(const int hand[])
{
    assert(-1 != *hand); /// assert if hand is not dealed
    /// for not to mistaken with four of kind
    if (getFace(hand[0]) == getFace(hand[3]) || getFace(hand[1]) == getFace(hand[4])) {
        return false;
    }
    int fullHouseIndex = 0;
    for (int i = 1; i < HAND_COUNT_; ++i) {
        if (getFace(hand[i - 1]) == getFace(hand[i])) {
            ++fullHouseIndex;
        }
    }
    /// return false if the hand has royal flush
    return 3 == fullHouseIndex;
}

bool
DeckOfCards::haveFourOfAKind(const int hand[])
{
    assert(-1 != *hand); /// assert if hand is not dealed
    if (getFace(hand[0]) == getFace(hand[3]) || getFace(hand[1]) == getFace(hand[4])) {
        return true;
    }
    return false;
}

bool
DeckOfCards::haveStraightFlush(const int hand[])
{
    assert(-1 != *hand); /// assert if hand is not dealed
    const int straightIndex = getFace(hand[0]);
    const int suit = getSuit(hand[0]);
    for (int i = 1; i < HAND_COUNT_; ++i) {
        /// check whether we have flush and straight the same time
        if (getFace(hand[i]) != straightIndex + i || suit != getSuit(hand[i])) {
            return false;
        }
    }
    /// checks if straight flush but not royal flush
    if (12 == getFace(hand[HAND_COUNT_ - 1])) {
        return false;
    }
    return true;
}

bool
DeckOfCards::haveRoyalFlush(const int hand[])
{
    assert(-1 != *hand); /// assert if hand is not dealed
    const int straightIndex = getFace(hand[0]);
    const int suit = getSuit(hand[0]);
    /// check whether we have straight flush and the last card is not ace
    if (12 != getFace(hand[HAND_COUNT_ - 1])) {
        return false;
    }
    for (int i = 1; i < HAND_COUNT_; ++i) {
        /// check whether we have flush and straight the same time
        if (getFace(hand[i]) != straightIndex + i || suit != getSuit(hand[i])) {
            return false;
        }
    }
    return true;
}

