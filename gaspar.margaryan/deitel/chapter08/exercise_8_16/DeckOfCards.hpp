/// DeckOfCards.hpp
/// Definition of class DeckOfCards that
/// represents a deck of playing cards.
/// DeckOfCards class definition
class DeckOfCards
{
public:
    enum Rank {NO_RANK, PAIR, TWO_PAIRS, THREE_OF_KIND, STRAIGHT, FLUSH, FULL_HOUSE, FOUR_OF_KIND, STRAIGHT_FLUSH, ROYAL_FLUSH};
    static const int SUIT_COUNT_ = 4;
    static const int FACE_COUNT_ = 13;
    static const int CARDS_COUNT_ = SUIT_COUNT_ * FACE_COUNT_;
    static const int HAND_COUNT_ = 5;
public:
    DeckOfCards();
    void shuffleAndDeal();
    void compareHands();
private:
    void shuffle(); /// shuffles cards in deck
    void dealAHand(int hand[]); /// deals cards in deck
    void printHandInfo(const int hand[]);
    void printHandRank(const Rank rank);
    void sortAHand(int hand[]);
    Rank getRank(const int hand[]);
    int getHighestFace(const int hand[], const Rank rank);
    int getPairsHighestFace(const int hand[]);
    int getThreeOfKindHighestFace(const int hand[]);
    int getFace(const int card);
    int getSuit(const int card);
    bool havePair(const int hand[]);
    bool haveTwoPairs(const int hand[]);
    bool haveThreeOfAKind(const int hand[]);
    bool haveStraight(const int hand[]);
    bool haveFlush(const int hand[]);
    bool haveFullHouse(const int hand[]);
    bool haveFourOfAKind(const int hand[]);
    bool haveStraightFlush(const int hand[]);
    bool haveRoyalFlush(const int hand[]);
private:
    int deck_[CARDS_COUNT_]; /// represents deck of cards
    int firstHand_[HAND_COUNT_]; /// represents the first hand of cards
    int secondHand_[HAND_COUNT_]; /// represents the second hand of cards
    int* topCardOfDeck_; /// pointer representing the top not empty card of the deck
}; /// end class DeckOfCards

