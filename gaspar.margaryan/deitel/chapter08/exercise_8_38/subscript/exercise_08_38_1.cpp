/// Exercise_08_38_1.cpp
/// use string copy and concatinate functions with
/// array subbscripting
#include <iostream>
#include <cstring>
#include <unistd.h>

const int LENGTH = 30;

int
main()
{
    /// using strcpy() function
    char name1[LENGTH] = {};
    char surname1[LENGTH] = {};
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input your first place favorite actors name and surname: ";
    }
    std::cin >> name1 >> surname1;
    const int nameSize1 = std::strlen(name1);
    char favoriteActor1[LENGTH] = {};
    std::strcpy(&favoriteActor1[0], name1);
    std::strcpy(&favoriteActor1[nameSize1], " ");
    std::strcpy(&favoriteActor1[nameSize1 + 1], surname1);
    /// using strcat() function
    char name2[LENGTH] = {};
    char surname2[LENGTH] = {};
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input your second place favorite actors name and surname: ";
    }
    std::cin >> name2 >> surname2;
    char favoriteActor2[LENGTH] = {};
    const int nameSize2 = std::strlen(name2);
    std::strcat(&favoriteActor2[0], name2);
    std::strcat(&favoriteActor2[nameSize2], " ");
    std::strcat(&favoriteActor2[nameSize2 + 1], surname2);
    /// print copied and concatinated names
    std::cout << "Your first place favorite actor is " << favoriteActor1 << std::endl;
    std::cout << "Your second place favorite actor is " << favoriteActor2 << std::endl;

    return 0; 
}


