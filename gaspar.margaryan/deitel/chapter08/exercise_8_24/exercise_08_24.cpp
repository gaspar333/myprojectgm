/// Exercise_08_24.cpp
/// recursive quick sort
#include <iostream>
#include <cstdlib> /// prototypes for rand
#include <unistd.h>

const int ARRAY_SIZE = 20;
void getArrayElements(int array[]);
void quickSort(int* const start, int* const end);
int* partition(int* left, int* right);
void printArray(const int array[]);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input value for random number seed: ";
    }
    int seed;
    std::cin >> seed;
    std::srand(seed);
    int quickSortArray[ARRAY_SIZE];
    getArrayElements(quickSortArray);
    quickSort(quickSortArray, quickSortArray + ARRAY_SIZE - 1);
    printArray(quickSortArray);

    return 0; 
}

void
getArrayElements(int array[])
{
    for (int i = 0; i < ARRAY_SIZE; ++i) {
        array[i] = std::rand() % 50;
    }
}

void
quickSort(int* const start, int* const end)
{
    if (start == end) {
        return;
    }
    const int* pivot = partition(start, end);
    quickSort(start, pivot - 1);
    quickSort(pivot, end);
}   

int*
partition(int* left, int* right)
{
    int* pivot = left++;
    while (right != left) {
        while (*right >= *pivot) {
            if (right == left) {
               break;
            }
            --right;
        }
        while (*left <= *pivot) {
            if (left == right) {
                break;
            }
            ++left;
        }
        if (*right <= *pivot && *left >= *pivot) {
            std::swap(*right, *left);
        }
    }
    if (*pivot >= *left) {
        std::swap(*left, *pivot);
    }
    pivot = left;
    return pivot;
}

void
printArray(const int array[])
{
    for (int i = 0; i < ARRAY_SIZE; ++i) {
        std::cout << array[i] << " ";
    }
}

