/// Exercise_08_34.cpp
/// a program that encodes English language phrases into pig Latin
#include <iostream>
#include <cstring>
#include <unistd.h>

const int CAPACITY = 80;
const int WORD_LENGTH = 15;

void getSentenceByUser(char* sentence);
void printPigLatinSentence(char* sentence);

int
main()
{
    char sentence[CAPACITY] = {};
    getSentenceByUser(sentence);
    printPigLatinSentence(sentence);
    
    return 0; 
}

void
getSentenceByUser(char* sentence)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input a sentence: ";
    }
    std::cin.getline(sentence, CAPACITY, '\n');
}

void
printPigLatinSentence(char* sentence)
{
    char* token = std::strtok(sentence, " ");
    while (NULL != token) {
        char word[WORD_LENGTH] = {};
        std::strcat(word, token + 1);
        std::strncat(word, token, 1);
        std::strcat(word, "ly ");
        std::cout << word;
        token = std::strtok(NULL, " ");
    }
}

