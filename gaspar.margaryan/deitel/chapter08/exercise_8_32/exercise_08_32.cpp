/// Exercise_08_32.cpp
/// program generating sentences
/// Article -> noun -> verb -> preposition -> article -> noun. 
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>

const int CAPACITY = 40;
const int SENTENCE_COUNT = 20;
const int ITEM_COUNT = 5;
static const char* ARTICLE[] = {"the", "a", "one", "some", "any"};
static const char* NOUN[] = {"boy", "girl", "dog", "town", "car"};
static const char* VERB[] = {"drove", "jumped", "ran", "walked", "skipped"};
static const char* PREPOSITION[] = {"to", "from", "over", "under", "on"};

void getSentence(char* sentence);
void createSentences();
void printSentences(const char array[][CAPACITY]);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input seed for random number generation: ";
    }
    int seed;
    std::cin >> seed;
    std::srand(seed);
    createSentences();
    return 0; 
}

void
createSentences()
{
    char sentences[SENTENCE_COUNT][CAPACITY] = {};
    for (int i = 0; i < SENTENCE_COUNT; ++i) {
        getSentence(*(sentences + i));
    }
    printSentences(sentences);
}

void
getSentence(char* sentence)
{
    std::strcat(sentence, ARTICLE[std::rand() % ITEM_COUNT]);
    std::strcat(sentence, " ");
    std::strcat(sentence, NOUN[std::rand() % ITEM_COUNT]);
    std::strcat(sentence, " ");
    std::strcat(sentence, VERB[std::rand() % ITEM_COUNT]);
    std::strcat(sentence, " ");
    std::strcat(sentence, PREPOSITION[std::rand() % ITEM_COUNT]);
    std::strcat(sentence, " ");
    std::strcat(sentence, ARTICLE[std::rand() % ITEM_COUNT]);
    std::strcat(sentence, " ");
    std::strcat(sentence, NOUN[std::rand() % ITEM_COUNT]);
    std::strcat(sentence, ".");
    sentence[0] = std::toupper(sentence[0]);
}

void
printSentences(const char array[][CAPACITY])
{
    for (int j = 0; j < SENTENCE_COUNT; ++j) {
        std::cout << array[j] << std::endl;
    }
}

