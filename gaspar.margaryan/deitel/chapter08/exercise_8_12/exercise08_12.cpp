/// Fig. 8.26: DeckOfCards.cpp
/// Member-function definitions for class DeckOfCards that simulates
/// the shuffling and dealing of a deck of playing cards.
#include <iostream>
#include <iomanip>
#include <cstdlib> /// prototypes for rand
#include <cassert>

#include "DeckOfCards.hpp" /// DeckOfCards class definition
/// DeckOfCards default constructor initializes deck

DeckOfCards::DeckOfCards()
{
    for (int card = 0; card < CARDS_COUNT_; ++card) {
        deck_[card] = -1; /// initialize empty slot of deck to -1
    }
    for (int card = 0; card < HAND_COUNT_; ++card) {
        hand_[card] = -1; /// initialize empty slot of hand to -1
    }
} /// end DeckOfCards default constructor

/// shuffle cards in deck
void
DeckOfCards::shuffle()
{
    /// for each of the 52 cards, choose a slot of the deck randomly
    for (int card = 0; card < CARDS_COUNT_; ++card) {
        int randomCard;
        do { /// choose a new random location until unoccupied slot is found
            randomCard = std::rand() % CARDS_COUNT_; /// randomly select the card
        } while (deck_[randomCard] != -1); /// end do...while
        /// place card number in chosen slot of deck
        deck_[randomCard] = card;
    } /// end for
} /// end function shuffle

/// deal a hand of cards from deck
void
DeckOfCards::deal()
{
    assert(-1 != *deck_); /// assert if deck is not dealed
    /// deal a hand of cards from deck's bottom                       
    for (int card = 0; card < HAND_COUNT_; ++card) {
        /// swap empty slots of the hand with dealed cards in the deck
        /// as if taking cards out from deck
        std::swap(deck_[card], hand_[card]);
    }
    sortAHand(); /// sort the hand by face by increasing order
} /// end function deal

void
DeckOfCards::printHandInfo()
{
    assert(-1 != *hand_); /// assert if the hand is not dealed
    /// initialize suits array 
    static const char *suits[] = {"Hearts", "Diamonds", "Clubs", "Spades"};
    /// initialize faces array                                     
    static const char *faces[] =
        {"Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven",
         "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
    /// print the hand of cards
    for (int card = 0; card < HAND_COUNT_; ++card) {
        std::cout << std::setw(5) << std::right << faces[getFace(hand_[card])]   
                  << " of " << std::setw(8) << std::left << suits[getSuit(hand_[card])]
                  << '\n';
    }
    /// print checking info about inning chance cards
    std::cout << "There is a pair in the hand: \t\t" << std::boolalpha << havePair() << std::endl;
    std::cout << "There are two pairs in the hand: \t" << std::boolalpha << haveTwoPairs() << std::endl;
    std::cout << "There are three of a kind in the hand: \t" << std::boolalpha << haveThreeOfAKind() << std::endl;
    std::cout << "There are four of a kind in the hand: \t" << std::boolalpha << haveFourOfAKind() << std::endl;
    std::cout << "There is flush in the hand: \t\t" << std::boolalpha << haveFlush() << std::endl;
    std::cout << "There is straight in the hand: \t\t" << std::boolalpha << haveStraight() << std::endl;
}

void
DeckOfCards::sortAHand()
{
    assert(-1 == *deck_); /// assert if hand is not dealed
    /// sorting a hand by faces
    for (int i = 0; i < HAND_COUNT_ - 1; ++i) {
        for (int j = i + 1; j < HAND_COUNT_; ++j) {
            if (getFace(hand_[i]) > getFace(hand_[j])) {
                std::swap(hand_[i], hand_[j]);
            }
        }
    }
}

inline int
DeckOfCards::getFace(const int card)
{
    const int face = card % FACE_COUNT_;
    return face;
}

inline int
DeckOfCards::getSuit(const int card)
{
    const int suit = card / FACE_COUNT_;
    return suit;
}

bool
DeckOfCards::havePair()
{
    assert(-1 != *hand_); /// assert if hand is not dealed
    int pairIndex = 0;
    for (int i = 0; i < HAND_COUNT_ - 1; ++i) {
        if (getFace(hand_[i]) == getFace(hand_[i + 1])) {
            ++pairIndex;
            if (pairIndex > 1) {
                return false;
            } /// return false if there are more than one pair
        }
    }
    assert(0 != pairIndex || 1 != pairIndex);
    return pairIndex; /// aka return 1 or zero
}

bool
DeckOfCards::haveTwoPairs()
{
    assert(-1 != *hand_); /// assert if hand is not dealed
    /// condition which returns false if the hand has three or four of a kind
    /// (for not being mistaken two pairs inside three and four of kinds)
    if ((getFace(hand_[0]) == getFace(hand_[2]) || getFace(hand_[1]) == getFace(hand_[3])) || getFace(hand_[2]) == getFace(hand_[4])) {
        return false;
    }
    int twoPairsIndex = 0;
    for (int i = 0; i < HAND_COUNT_ - 1; ++i) {
        if (getFace(hand_[i]) == getFace(hand_[i + 1])) {
            ++twoPairsIndex;
        }
    }
    return 2 == twoPairsIndex;
}

bool
DeckOfCards::haveThreeOfAKind()
{
    assert(-1 != *hand_); /// assert if hand is not dealed
    /// condition which returns false if the hand has four of a kind
    /// (for not being mistaken the three of kind inside four of kind)
    if (getFace(hand_[0]) == getFace(hand_[3]) || getFace(hand_[1]) == getFace(hand_[4])) {
        return false;
    }
    if ((getFace(hand_[0]) == getFace(hand_[2]) || getFace(hand_[1]) == getFace(hand_[3])) || getFace(hand_[2]) == getFace(hand_[4])) {
        return true;
    }
    return false;
}

bool
DeckOfCards::haveFourOfAKind()
{
    assert(-1 != *hand_); /// assert if hand is not dealed
    if (getFace(hand_[0]) == getFace(hand_[3]) || getFace(hand_[1]) == getFace(hand_[4])) {
        return true;
    }
    return false;
}

bool
DeckOfCards::haveFlush()
{
    assert(-1 != *hand_); /// assert if hand is not dealed
    const int flushIndex = getSuit(hand_[0]);
    for (int i = 1; i < HAND_COUNT_; ++i) {
        if (flushIndex != getSuit(hand_[i])) {
            return false;
        }
    }
    return true;
}

bool
DeckOfCards::haveStraight()
{
    assert(-1 != *hand_); /// assert if hand is not dealed
    const int straightIndex = getFace(hand_[0]);
    for (int i = 1; i < HAND_COUNT_; ++i) {
        if (getFace(hand_[i]) != straightIndex + i) {
            return false;
        }
    }
    return true;
}

