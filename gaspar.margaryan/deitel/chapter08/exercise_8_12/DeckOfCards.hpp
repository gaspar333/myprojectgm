/// DeckOfCards.hpp
/// Definition of class DeckOfCards that
/// represents a deck of playing cards.
/// DeckOfCards class definition
class DeckOfCards
{
public:
    static const int SUIT_COUNT_ = 4;
    static const int FACE_COUNT_ = 13;
    static const int CARDS_COUNT_ = SUIT_COUNT_ * FACE_COUNT_;
    static const int HAND_COUNT_ = 5;
public:
    DeckOfCards();
    void shuffle(); /// shuffles cards in deck
    void deal(); /// deals cards in deck
    void printHandInfo();
private:
    void sortAHand();
    int getFace(const int card);
    int getSuit(const int card);
    bool havePair();
    bool haveTwoPairs();
    bool haveThreeOfAKind();
    bool haveFourOfAKind();
    bool haveFlush();
    bool haveStraight();
private:
    int deck_[CARDS_COUNT_]; /// represents deck of cards
    int hand_[HAND_COUNT_]; /// represents a hand of cards
}; /// end class DeckOfCards

