/// Exercise_08_12.cpp
/// Card shuffling and dealing program.
#include <iostream>
#include <cstdlib> /// prototypes for rand
#include <unistd.h>

#include "DeckOfCards.hpp" /// DeckOfCards class definition

int
main()
{
    int seed;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input seed for random number generation: ";
    }
    std::cin >> seed;
    std::srand(seed);
    DeckOfCards deckOfCards; /// create DeckOfCards object
    deckOfCards.shuffle(); /// shuffle the cards in the deck
    deckOfCards.deal(); /// deal the cards in the deck
    deckOfCards.printHandInfo(); /// prints about winning condition chance
    
    return 0; /// indicates successful termination
} /// end main

