/// Exercise_08_30.cpp
/// program that input two strings from user
/// and compares
#include <iostream>
#include <cstring>
#include <unistd.h>

const int CAPACITY = 80;

void getStrings(char* const& string1, char* const& string2);
void compareStrings(const char* const& string1, const char* const& string2);

int
main()
{
    char string1[CAPACITY] = {};
    char string2[CAPACITY] = {};
    getStrings(string1, string2);
    compareStrings(string1, string2);
    return 0; 
}

inline void
getStrings(char* const& string1, char* const& string2)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input two strings to compare: ";
    }
    std::cin.getline(string1, CAPACITY, '\n');
    std::cin.getline(string2, CAPACITY, '\n');
}

inline void
compareStrings(const char* const& string1, const char* const& string2)
{
    const int result = std::strcmp(string1, string2);
    if (result < 0) {
        std::cout << "String1 \"" << string1 << "\" " << "is less than String2 \"" << string2 << "\".";
        return;
    }
    if (result > 0) {
        std::cout << "String1 \"" << string1 << "\" " << "is grater than String2 \"" << string2 << "\".";
        return;
    }
    std::cout << "String1 \"" << string1 << "\" " << "and String2 \"" << string2 << "\" are equal.";
    return;
}

