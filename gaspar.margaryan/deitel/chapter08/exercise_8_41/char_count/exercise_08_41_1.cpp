/// Exercise_08_41_1.cpp
/// a program counting character occurance from text
/// and pritnts table of alphabet with occurance count
#include <iostream>
#include <iomanip>
#include <cstring>
#include <unistd.h>

const int BUFFER_SIZE = 200;
const int LETTER_COUNT = 26;

void getLetterOccurance(const char* text, int* array);
void printLetterOccurance(const char* text);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input text to check letter occurence: ";
    }
    char text[BUFFER_SIZE] = {};
    std::cin.getline(text, BUFFER_SIZE, '\n');
    printLetterOccurance(text);

    return 0; 
}

void
printLetterOccurance(const char* text)
{
    int letterOccurence[LETTER_COUNT] = {};
    getLetterOccurance(text, letterOccurence);
    std::cout << "Letter\t" << "Occurence" << std::endl;
    for (int i = 0; i < LETTER_COUNT; ++i) {
        std::cout << std::setw(6) << static_cast<char>(i + 'a') << "\t"
                  << std::setw(9) << letterOccurence[i] << std::endl;
    }
}

inline void
getLetterOccurance(const char* text, int* array)
{
    const int textLength = std::strlen(text);
    for (int i = 0; i < textLength; ++i) {
        if ((text[i] >= 'A' || text[i] <= 'Z') || (text[i] >= 'a' || text[i] <= 'z')) {
            ++array[static_cast<int>(std::tolower(text[i]) - 'a')];
        }
    }
}

