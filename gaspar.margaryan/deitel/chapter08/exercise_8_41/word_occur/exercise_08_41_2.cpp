/// Exercise_08_41_1.cpp
/// a program counting words' length from text
/// and prints the table of words lengths' occurance
#include <iostream>
#include <iomanip>
#include <cstring>
#include <unistd.h>

const int BUFFER_SIZE = 200;
const int MAX_WORD_LENGTH = 10;

void getWordLengthOccurance(char* text, int* array);
void printWordLengthOccurance(char* text);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input text to check word length occurence: ";
    }
    char text[BUFFER_SIZE] = {};
    std::cin.getline(text, BUFFER_SIZE, '\n');
    printWordLengthOccurance(text);

    return 0; 
}

void
printWordLengthOccurance(char* text)
{
    int wordLengthOccurence[MAX_WORD_LENGTH] = {};
    getWordLengthOccurance(text, wordLengthOccurence);
    std::cout << "Word\nLength\t" << "Occurence" << std::endl;
    for (int i = 1; i < MAX_WORD_LENGTH; ++i) {
        std::cout << std::setw(6) << i << "\t" << std::setw(9)
                  << wordLengthOccurence[i] << std::endl;
    }
}

inline void
getWordLengthOccurance(char* text, int* array)
{
    char* token = std::strtok(text, " :,.()");
    while (NULL != token) {
        const int wordLength = std::strlen(token);
        ++array[wordLength];
        token = std::strtok(NULL, " :,.()");
    }
}

