/// Exercise_08_41_3.cpp
/// a program countind different words occurence
/// and prints the table of that words occurance
#include <iostream>
#include <iomanip>
#include <cstring>
#include <unistd.h>

const int BUFFER_SIZE = 200;
const int MAX_WORD_COUNT = 30;
const int MAX_WORD_LENGTH = 10;

void getWords(char* text1, char* text2, char (*text3)[10]);
void printWordOccurance(char (*text)[10]);
void sortWords(char (*text)[10]);
size_t getWordsCount(char (*text)[10]);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input a text: ";
    }
    char text1[BUFFER_SIZE] = {};
    std::cin.getline(text1, BUFFER_SIZE, '\n');
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input next text: ";
    }
    char text2[BUFFER_SIZE] = {};
    std::cin.getline(text2, BUFFER_SIZE, '\n');
    char text3[MAX_WORD_COUNT][MAX_WORD_LENGTH] = {};
    getWords(text1, text2, text3);
    sortWords(text3);
    printWordOccurance(text3);

    return 0; 
}

void
printWordOccurance(char (*text)[10])
{
    const size_t wordsCount = getWordsCount(text);
    std::cout << "The Word\t" << "Occurence" << std::endl;
    for (size_t i = 0; i < wordsCount; ++i) {
        if (i >= 1) {
            for (size_t j = 0; j < i; ++j) {
                if (0 == std::strcmp(text[i], text[j])) {
                    ++i;
                }
            }
        }
        int occurence = 1;
        for (size_t k = i + 1; k < wordsCount; ++k) {
            if (0 == std::strcmp(text[i], text[k])) {
                ++occurence;
            }
        }
        std::cout << text[i] << (std::strlen(text[i]) >= 8 ? "\t" : "\t\t")
                  << occurence << std::endl;
    }
}

inline void
getWords(char* text1, char* text2, char (*text3)[10])
{
    char mergedTexts[BUFFER_SIZE] = {};
    std::strcpy(mergedTexts, text1);
    std::strcat(mergedTexts, text2);
    size_t index = 0;
    char* token = std::strtok(mergedTexts, " :,.()");
    while (NULL != token) {
        const size_t wordLength = std::strlen(token);
        for (size_t i = 0; i < wordLength; ++i) {
            if (token[i] >= 'A' || token[i] <= 'Z') {
                token[i] = std::tolower(token[i]);
            }
        }
        std::strcpy(text3[index++], token);        
        token = std::strtok(NULL, " :,.()");
    }
}

size_t
getWordsCount(char (*text)[10])
{
    size_t counter = 0;
    for (size_t i = 0; i < MAX_WORD_COUNT; ++i) {
        if ('\0' != *text[i]) {
            ++counter;
        }
    }
    return counter;
}


inline void
sortWords(char (*text)[10])
{
    size_t wordsCount = getWordsCount(text);
    for (size_t i = 0; i < wordsCount; ++i) {
        for (size_t j = 1; j < wordsCount - i; ++j) {
            if (std::strcmp(text[j - 1], text[j]) > 0) {
                std::swap(text[j - 1], text[j]);
            }
        }
    }
}


