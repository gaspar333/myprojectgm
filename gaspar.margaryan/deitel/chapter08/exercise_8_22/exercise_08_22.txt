/// Exercise_08_22.txt
/// What does this program do?
#include <iostream>

int mystery2( const char *s ); /// prototype

int
main()
{
    char string1[80];

    std::cout << "Enter a string: ";
    std::cin >> string1;
    std::cout << mystery2(string1) << std::endl;

    return 0; /// indicates successful termination
} /// end main

/// What does this function do?
int
mystery2(const char *s)
{
    int x;
    for (x = 0; *s != '\0'; s++) {
        ++x;
    }
    return x;
} /// end function mystery2

This program gets a string from user, passes the pointer aka address to the function.
The function loops the string by characters to the null '\0' ending element at the same time counting the characters of the string and the program prints it as an output.