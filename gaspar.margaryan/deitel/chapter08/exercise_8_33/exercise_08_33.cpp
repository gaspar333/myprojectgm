/// Exercise_08_33.cpp
/// program generating limericks
#include <iostream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <cassert>

const int CAPACITY = 50;
const int SENTENCE_COUNT = 5;
static const char* RHYME_NOUN[] = {"tail", "nail", "jail", "mail", "rail", "snail", "ale", "gale", "pail", "veil", "trail"};
static const char* ADVERB[] = {"on Monday", "on Tuesday", "on Wedensday", "on Thuesday", "on Friday", "on Saturday", "on Sunday", "today", "yesterday"};
static const char* RHYME_ADVERB[] = {"brutally", "totally", "randomly", "painfully", "loudly", "cherfully", "proudly"};
static const char* ARTICLE[] = {"the", "a", "one"};
static const char* NOUN[] = {"boy", "girl", "dog", "hedgehog", "cat", "shark", "dolphin", "bird", "clown", "cow", "horse", "cammel"};
static const char* VERB[] = {"picked", "piked", "bit", "hacked", "broke", "licked", "drove", "touched", "drinked", "swallowed"};

void getSentence(char* sentence, const int line);
void createLimericks();
void printLimericks(const char array[][CAPACITY]);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input seed for random number generation: ";
    }
    int seed;
    std::cin >> seed;
    std::srand(seed);
    createLimericks();
    return 0; 
}

void
createLimericks()
{
    char sentences[SENTENCE_COUNT][CAPACITY] = {};
    for (int i = 0; i < SENTENCE_COUNT; ++i) {
        getSentence(*(sentences + i), i);
    }
    printLimericks(sentences);
}

void
getSentence(char* sentence, const int line)
{
    const int adverbSize = sizeof(ADVERB) / sizeof(ADVERB[0]);
    const int articleSize = sizeof(ARTICLE) / sizeof(ARTICLE[0]);
    const int rhymeNounSize = sizeof(RHYME_NOUN) / sizeof(RHYME_NOUN[0]);
    const int rhymeAdverbSize = sizeof(RHYME_ADVERB) / sizeof(RHYME_ADVERB[0]);
    const int verbSize = sizeof(VERB) / sizeof(VERB[0]);
    const int nounSize = sizeof(NOUN) / sizeof(NOUN[0]);
    switch (line) {
    case 0:
    case 1:
    case 4:
        std::strcat(sentence, ADVERB[std::rand() % adverbSize]);
        std::strcat(sentence, " ");
        std::strcat(sentence, ARTICLE[std::rand() % articleSize]);
        std::strcat(sentence, " ");
        std::strcat(sentence, NOUN[std::rand() % nounSize]);
        std::strcat(sentence, " ");
        std::strcat(sentence, VERB[std::rand() % verbSize]);
        std::strcat(sentence, " ");
        std::strcat(sentence, ARTICLE[std::rand() % articleSize]);
        std::strcat(sentence, " ");
        std::strcat(sentence, NOUN[std::rand() % nounSize]);
        std::strcat(sentence, "'s ");
        std::strcat(sentence, RHYME_NOUN[std::rand() % rhymeNounSize]);
        sentence[0] = std::toupper(sentence[0]);
        if (4 == line) {
            std::strcat(sentence, ".");
        }
        return;
    case 2:
        std::strcat(sentence, ARTICLE[std::rand() % articleSize]);
        std::strcat(sentence, " ");
        std::strcat(sentence, RHYME_NOUN[std::rand() % rhymeNounSize]);
        std::strcat(sentence, " was ");
        std::strcat(sentence, VERB[std::rand() % verbSize]);
        std::strcat(sentence, " so ");
        std::strcat(sentence, RHYME_ADVERB[std::rand() % rhymeAdverbSize]);
        std::strcat(sentence, " ");
        sentence[0] = std::toupper(sentence[0]);
        return;
    case 3:
        std::strcat(sentence, "that ");
        std::strcat(sentence, ARTICLE[std::rand() % articleSize]);
        std::strcat(sentence, " ");
        std::strcat(sentence, NOUN[std::rand() % nounSize]);
        std::strcat(sentence, " was ");
        std::strcat(sentence, VERB[std::rand() % verbSize]);
        std::strcat(sentence, " by ");
        std::strcat(sentence, ARTICLE[std::rand() % articleSize]);
        std::strcat(sentence, " ");
        std::strcat(sentence, NOUN[std::rand() % nounSize]);
        std::strcat(sentence, " ");
        std::strcat(sentence, RHYME_ADVERB[std::rand() % rhymeAdverbSize]);
        std::strcat(sentence, " ");
        sentence[0] = std::toupper(sentence[0]);
        return;
    default: assert(0); return;
    }
}

void
printLimericks(const char array[][CAPACITY])
{
    for (int j = 0; j < SENTENCE_COUNT; ++j) {
        std::cout << array[j] << std::endl;
    }
}

