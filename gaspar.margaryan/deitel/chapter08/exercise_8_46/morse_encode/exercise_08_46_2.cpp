/// Exercise_08_46_2.cpp
/// a program converting words into Morse code
#include <iostream>
#include <cstring>
#include <unistd.h>

const int BUFFER_SIZE = 100;
const char* morseDigits[] = {
    "-----", ".----", "..---", "...--", "....-",
    ".....", "-....", "--...", "---..", "----."
};
const char* morseCharacters[] = {
    ".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
    "....", "..", ".---", "-.-", ".-..", "--", "-.",
    "---", ".--.", "--.-", ".-.", "...", "-", "..-",
    "...-", ".--", "-..-", "-.--", "--.."
};

void printMorse(char* string);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input string to encode into Morse code: ";
    }
    char string[BUFFER_SIZE] = {};
    std::cin.getline(string, BUFFER_SIZE, '\n');
    printMorse(string);
    return 0; 
}

inline void
printMorse(char* string)
{
    const int stringLength = std::strlen(string);
    /// ignores all other punctuations and symbols
    for (int i = 0; i < stringLength; ++i) {
        if (string[i] >= '0' && string[i] <= '9') {
            std::cout << morseDigits[string[i] - '0'] << ' ';
        }
        if ((string[i] >= 'A' && string[i] <= 'Y') || (string[i] >= 'a' && string[i] <= 'y')) {
            std::cout << morseCharacters[std::toupper(string[i]) - 'A'] << ' ';
        }
        if (' ' == string[i]) {
            std::cout << "  ";
        }
    }
}
