/// Exercise_08_46_1.cpp
/// a program converting Morse code into words
#include <iostream>
#include <cstring>
#include <unistd.h>

const int BUFFER_SIZE = 200;
const char* morseDigits[] = {
    "-----", ".----", "..---", "...--", "....-",
    ".....", "-....", "--...", "---..", "----."
};
const char* morseCharacters[] = {
    ".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
    "....", "..", ".---", "-.-", ".-..", "--", "-.",
    "---", ".--.", "--.-", ".-.", "...", "-", "..-",
    "...-", ".--", "-..-", "-.--", "--.."
};

void printMorse(const char* morse);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input morse code to decode into string: ";
    }
    char morseCode[BUFFER_SIZE] = {};
    std::cin.getline(morseCode, BUFFER_SIZE, '\n');
    printMorse(morseCode);
    return 0; 
}

inline void
printMorse(const char* morse)
{
    const int digitsCount = static_cast<int>('9' - '0' + 1);
    const int charsCount = static_cast<int>('Y' - 'A' + 1);
    const char* pointer = morse; /// for getting spaces between words
    char morseDuplicate[BUFFER_SIZE] = {};
    std::strcpy(morseDuplicate, morse);
    char* token = std::strtok(morseDuplicate, " ");
    while (NULL != token) {
        const int tokLength = std::strlen(token);
        pointer += tokLength;
        if (5 == tokLength) {
            for (int i = 0; i < digitsCount; ++i) {
                if (0 == std::strcmp(token, morseDigits[i])) {
                    std::cout << static_cast<char>(i + '0');
                    break;
                }
            }
        }
        for (int j = 0; j < charsCount; ++j) {
            if (0 == std::strcmp(token, morseCharacters[j])) {
                std::cout << static_cast<char>(j + 'A');
                break;
            }
        }
        if (' ' == *(pointer + 1)) {
            std::cout << ' ';
            pointer += 2;
        }
        ++pointer;
        token = std::strtok(NULL, " ");
    }
}

