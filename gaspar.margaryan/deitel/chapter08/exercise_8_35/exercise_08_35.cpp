/// Exercise_08_35.cpp
/// a program that prints phone number string
#include <iostream>
#include <cstring>
#include <unistd.h>

const int CAPACITY = 16;

void getPhoneNumberStrByUser(char* numberStr);
int getNumbersStr(char* numberStr);
void printPhoneNumberStr(const char* areaCode, const char* numberStr);
bool isDigit(const char* number);

int
main()
{
    char phoneNumberStr[CAPACITY] = {};
    getPhoneNumberStrByUser(phoneNumberStr);
    return getNumbersStr(phoneNumberStr);
}

void
getPhoneNumberStrByUser(char* numberStr)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input phone number in \"(XXX) XXX-XXXX\" format: ";
    }
    std::cin.getline(numberStr, CAPACITY, '\n');
}

int
getNumbersStr(char* numberStr)
{
    if ('(' != numberStr[0] || ')' != numberStr[4] || ' ' != numberStr[5]) {
        std::cerr << "Error 2: Wrong format phone number.";
        return 2;
    }
    char phoneNumber[CAPACITY] = {};
    const char* areaCode = std::strtok(numberStr + 1, ")"); /// ignoring parentheses
    if (3 != std::strlen(areaCode) || !isDigit(areaCode)) {
        std::cerr << "Error 1: Wrong data format phone number.";
        return 1;
    }
    const char* firstThreeDigits = std::strtok(NULL, " -"); /// tokenizing to '-' ignoring space
    if (3 != std::strlen(firstThreeDigits) || !isDigit(firstThreeDigits)) {
        std::cerr << "Error 1: Wrong data format phone number.";
        return 1;
    }
    std::strcat(phoneNumber, firstThreeDigits);
    const char* lastFourDigits = std::strtok(NULL, " ");
    if (4 != std::strlen(lastFourDigits) || !isDigit(lastFourDigits)) {
        std::cerr << "Error 1: Wrong data format phone number.";
        return 1;
    }
    std::strcat(phoneNumber, lastFourDigits);
    printPhoneNumberStr(areaCode, phoneNumber);
    return 0;
}

void
printPhoneNumberStr(const char* areaCode, const char* numberStr)
{
    std::cout << "The area code is " << areaCode << '\n';
    std::cout << "The phone number is " << numberStr;
}

bool
isDigit(const char* number)
{
    const size_t length = std::strlen(number);
    for (size_t i = 0; i < length; ++i) {
        if (number[i] < '0' || number[i] > '9') {
            return false;
        }
    }
    return true;
}

