/// Exercise_08_37.cpp
/// sort cities by alphabetical order
/// using some sorting skills from chapter 7
#include <iostream>
#include <cstring>
#include <unistd.h>

const int COUNT = 15;
const int LENGTH = 10;

void getCitiesByUser(char (*cities)[LENGTH]);
void sortCitiesByAlphabet(char (*cities)[LENGTH]);
void printCities(const char (*cities)[LENGTH]);

int
main()
{
    char cities[COUNT][LENGTH] = {};
    getCitiesByUser(cities);
    sortCitiesByAlphabet(cities);
    printCities(cities);
    return 0; 
}

inline void
getCitiesByUser(char (*cities)[LENGTH])
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input " << COUNT << " cities: ";
    }
    for (int i = 0; i < COUNT; ++i) {
        std::cin.getline(cities[i], LENGTH, '\n');
    }
}

inline void
sortCitiesByAlphabet(char (*cities)[LENGTH])
{
    for (size_t i = 0; i < COUNT; ++i) {
        for (size_t j = 1; j < COUNT - i; ++j) {
            if (std::strcmp(cities[j - 1], cities[j]) > 0) {
                std::swap(cities[j - 1], cities[j]);
            }
        }
    }
}

inline void
printCities(const char (*cities)[LENGTH])
{
    for (int i = 0; i < COUNT; ++i) {
        std::cout << cities[i] << '\n';
    }
}

