/// Exercise_08_17.cpp
/// Simulation: The Tortoise and the Hare
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <unistd.h>

const int TRACK_LENGTH = 70;

void executeRace(const char track[]);
void fillTrack(char track[]);
int getTortoiseMove(const int tick);
int getHareMove(const int tick);
void printPositionLine(const char track[], const char* tortoisePtr, const char* harePtr);
void printInfo(const std::string& info);

int
main()
{
    int seed;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input number for random number seed: ";
    }
    std::cin >> seed;
    std::srand(seed);
    char track[TRACK_LENGTH];
    fillTrack(track);
    executeRace(track);
    return 0;
}

void
fillTrack(char track[])
{
    for (int i = 0; i < TRACK_LENGTH; ++i) {
        track[i] = ' ';
    }
}

void
executeRace(const char track[])
{
    const char* tortoisePtr = track;
    const char* harePtr = track;
    printInfo("BANG !!!!!\nAND THEY'RE OFF !!!!!\n");
    while (true) {
        const int timeTick = std::rand() % 10 + 1;
        /// change pointer address by a move size
        tortoisePtr += getTortoiseMove(timeTick);
        /// go to start position if slip back more than start point
        if (tortoisePtr < track) {
            tortoisePtr = track;
        }
        /// change pointer address by a move size
        harePtr += getHareMove(timeTick);
        /// go to start position if slip back more than start point
        if (harePtr < track) {
            harePtr = track;
        }
        printPositionLine(track, tortoisePtr, harePtr);
        if (tortoisePtr >= &track[TRACK_LENGTH] && harePtr >= &track[TRACK_LENGTH]) {
            printInfo("It's a tie.\n");
            tortoisePtr = track;
            harePtr = track;
        }
        if (tortoisePtr >= &track[TRACK_LENGTH]) {
            printInfo("TORTOISE WINS!!! YAY!!!\n");
            return;
        }
        if (harePtr >= &track[TRACK_LENGTH]) {
            printInfo("Hare wins. Yuch.\n");
            return;
        }
    }
}

void
printPositionLine(const char track[], const char* tortoisePtr, const char* harePtr)
{
    for (int i = 0; i < TRACK_LENGTH; ++i) {
        if (tortoisePtr == &track[i] && harePtr == &track[i]) {
            printInfo("OUCH!!!");
            continue;
        }
        if (tortoisePtr == &track[i]) {
            printInfo("T");
            continue;
        }
        if (harePtr == &track[i]) {
            printInfo("H");
            continue;
        }
        std::cout << track[i];
    }
    std::cout << '\n';
}

int
getTortoiseMove(const int tick)
{
    assert(tick > 0 && tick <= 10);
    if (tick >= 1 && tick <= 5) {
        return 3; /// fast plod 50% 3 steps forward
    }
    if (tick >= 6 && tick <= 7) {
        return -6; /// slip 20% 6 steps backward
    }
    return 1; /// slow plod 30% 1 steps forward
}

int
getHareMove(const int tick)
{
    assert(tick > 0 && tick <= 10);
    if (tick >= 3 && tick <= 4) {
        return 9; /// fast plod 50% 3 steps forward
    }
    if (5 == tick) {
        return -12; /// big slip 10% 12 steps backward
    }
    if (tick >= 6 && tick <= 8) {
        return 1; /// small hop 20% 1 steps forward
    }
    if (tick >= 9 && tick <= 10) {
        return -2; /// small slip 20% 2 steps backward
    }
    return 0; /// sleep 20% no move
}

void
printInfo(const std::string& info)
{
    std::cout << info;
}

