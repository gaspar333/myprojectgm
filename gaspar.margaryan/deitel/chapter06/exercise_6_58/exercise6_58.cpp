#include <iostream>
#include <unistd.h>

template <typename T>
T min(const T& value1, const T& value2);

int
main()
{
    const bool isInteractive = ::isatty(STDIN_FILENO);
    if (isInteractive) {
        std::cout << "Input two integers to determine the minimum value: ";
    }
    int intValue1;
    int intValue2;
    std::cin >> intValue1 >> intValue2;
    std::cout << "The minimum integer value is: " << min(intValue1, intValue2) << std::endl;

    if (isInteractive) {
        std::cout << "Input two floating point numbers to determine the minimum value: ";
    }
    float floatValue1;
    float floatValue2;
    std::cin >> floatValue1 >> floatValue2;
    std::cout << "The minimum floating point value is: " << min(floatValue1, floatValue2) << std::endl;

    if (isInteractive) {
        std::cout << "Input two characters to determine the minimum value: ";
    }
    char charValue1;
    char charValue2;
    std::cin >> charValue1 >> charValue2;
    std::cout << "The minimum character value is: " << min(charValue1, charValue2) << std::endl;

    return 0;
}

template <typename T>
T 
min(const T& value1, const T& value2)
{
    return (value1 < value2) ? value1 : value2;
}

