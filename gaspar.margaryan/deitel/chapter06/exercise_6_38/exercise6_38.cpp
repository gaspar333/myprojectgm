#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

int randomNumber();
void guessTheNumber();

int
main()
{
    std::srand(std::time(0));
    while (true) {
        guessTheNumber();
        std::cout << "Play again? (y/n)";
        char option;
        std::cin >> option;
        if ('n' == option) {
            break;
        }
        if ('y' != option) {
            std::cerr << "Error 1: Wrong input." << std::endl;
            return 1;
        }
    }

    return 0;
}

inline int
getRandomNumber()
{
    return 1 + std::rand() % 1000;
}

inline void
guessTheNumber()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "I have a number between 1 and 1000.\n"
                  << "Can you guess my number?\n" << "Type your first guess: ";
    }
    const int number = getRandomNumber();
    while (true) {
        int answer;
        std::cin >> answer;
        if (number == answer) {
            std::cout << "Excellent! You guessed the number!" << std::endl;
            break;
        } 
        if (answer < number) {
            std::cout << "Too low. Try again.\n";
        } else {
            std::cout << "Too high. Try again.\n";
        }
    }
}

