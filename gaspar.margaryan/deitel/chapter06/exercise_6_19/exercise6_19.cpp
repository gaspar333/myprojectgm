#include <iostream>
#include <iomanip>
#include <cmath>

double hypotenuse(const double side1, const double side2);

int
main ()
{
    std::cout << "Triangle \t" << "Side1 \t" << "Side2 \t" << "Hypotenuse " << std::endl;
    std::cout << std::setw(10) << "1 \t" << std::setw(7) << "3.0 \t" << std::setw(7)
              << "4.0 \t" << std::setw(10) << hypotenuse(3.0, 4.0) << std::endl;
    std::cout << std::setw(10) << "2 \t" << std::setw(7) << "5.0 \t" << std::setw(7)
              << "12.0 \t" << std::setw(10) << hypotenuse(5.0, 12.0) << std::endl;
    std::cout << std::setw(10) << "3 \t" << std::setw(7) << "8.0 \t" << std::setw(7)
              << "15.0 \t" << std::setw(10) << hypotenuse(8.0, 15.0) << std::endl;

    return 0;
}

inline double
hypotenuse(const double side1, const double side2)
{
    const double hypotenuseSize = std::sqrt(std::pow(side1, 2) + std::pow(side2, 2));
    return hypotenuseSize;
}

