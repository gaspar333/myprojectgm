#include <iostream>
#include <cmath>
#include <unistd.h>

int gcd(int number1, int number2); /// greatest common divisor

int main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input two integers to determine the greatest common divisor: ";
    }
    int number1;
    int number2;
    std::cin >> number1 >> number2;
    std::cout << "The greatest common divisor of numbers " << number1 << " and "
              << number2 << " is " << gcd(number1, number2) << std::endl;

    return 0;
}

inline int
gcd(int number1, int number2)
{
    number1 = std::abs(number1);
    number2 = std::abs(number2);
    while (number1 != number2) {
        if (0 == number1) {
            return number2;
        }
        if (0 == number2) {
            return number1;
        }
        if (number1 > number2) {
            number1 %= number2;
        } else {
            number2 %= number1;
        }
    }
    return number1;
}

