#include <iostream>
#include <unistd.h>
/// The exponent must be equal to or greater than zero
/// in case of negative exponent, the function behavior is unexpected.
/// In case of huge exponent numbers, the result will overflow.

int integerPower(int base, const unsigned int exponent);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input base number for power calculation: ";
    }
    int base;
    std::cin >> base;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input exponent number for power calculation: ";
    }
    unsigned int exponent;
    std::cin >> exponent;
    std::cout << base << " ^ " << exponent
              << " = " << integerPower(base, exponent) << std::endl;

    return 0;
}

inline int
integerPower(int base, const unsigned int exponent)
{
    unsigned int result = 1; /// in case of power base 0 return value 1
    for (int i = exponent; i > 0; i /= 2) {
        if (1 == (i & 1)) {
            result *= base;
        }
        base *= base;
    }
    return result;
}

