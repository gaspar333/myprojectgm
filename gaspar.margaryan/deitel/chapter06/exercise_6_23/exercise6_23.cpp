#include <iostream>
#include <cassert>
#include <unistd.h>

void printPattern(const int side, const char shape);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input pattern side size: ";
    }
    int sideSize;
    std::cin >> sideSize;
    if (sideSize < 0) {
        std::cerr << "Error 1: The size should be positive." << std::endl;
        return 1;
    }

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input character for pattern: ";
    }
    char fillCharacter;
    std::cin >> fillCharacter;

    printPattern(sideSize, fillCharacter);

    return 0;
}

inline void
printPattern(const int sideSize, const char fillCharacter)
{
    assert(sideSize >= 0);
    for (int i = 0; i < sideSize; ++i) {
    	for (int j = 0; j < sideSize; ++j) {
    	    std::cout << fillCharacter;
    	}
    	std::cout << std::endl;
    }
}

