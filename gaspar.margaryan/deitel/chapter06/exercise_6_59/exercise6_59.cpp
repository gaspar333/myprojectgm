#include <iostream>
#include <unistd.h>

template <typename T>
T max(const T& value1, const T& value2, const T& value3);

int
main()
{
    const bool isInteractive = ::isatty(STDIN_FILENO);

    if (isInteractive) {
        std::cout << "Input three integers to determine the maximum value: ";
    }
    int intValue1;
    int intValue2;
    int intValue3;
    std::cin >> intValue1 >> intValue2 >> intValue3;
    std::cout << "The maximum integer value is: " << max(intValue1, intValue2, intValue3) << std::endl;

    if (isInteractive) {
        std::cout << "Input three floating point numbers to determine the maximum value: ";
    }
    float floatValue1;
    float floatValue2;
    float floatValue3;
    std::cin >> floatValue1 >> floatValue2 >> floatValue3;
    std::cout << "The maximum floating point value is: " << max(floatValue1, floatValue2, floatValue3) << std::endl;
    
    if (isInteractive) {
        std::cout << "Input three characters to determine the maximum value: ";
    }
    char charValue1;
    char charValue2;
    char charValue3;
    std::cin >> charValue1 >> charValue2 >> charValue3;
    std::cout << "The maximum character value is: " << max(charValue1, charValue2, charValue3) << std::endl;

    return 0;
}

template <typename T>
T
max(const T& value1, const T& value2, const T& value3)
{
    T largest = value1;
    if (value2 > largest) {
        largest = value2;
    }
    if (value3 > largest) {
        largest = value3;
    }
    return largest;
}

