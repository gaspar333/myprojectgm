#include <iostream>
#include <unistd.h>
#include <cmath>
#include <cassert>

void hanoiTowerIterative(const int diskAmount);
int getDiskNumber(const int stepNumber, const int diskAmount);
void printDirection(const int step, const int diskNumber, const int start, const int end);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input the amount of disks: ";
    }
    int diskAmount;
    std::cin >> diskAmount;
    if (diskAmount < 0) {
        std::cerr << "Error 1: wrong data input 1! \n";
        return 1;
    }
    hanoiTowerIterative(diskAmount);

    return 0;
}

inline void
hanoiTowerIterative(const int diskAmount)
{
    assert(diskAmount >= 0);
    /// the possible move count for exact disk sizes
    const int iterationSize = std::pow(2, diskAmount) - 1;

    const int d1 = 1; /// direction 1
    /// if the amount of disks are even transpose the directions
    const int d2 = (0 == diskAmount % 2) ? 2 : 3; /// direction 2
    const int d3 = (0 == diskAmount % 2) ? 3 : 2; /// direction 3

    for (int index = 1; index <= iterationSize; ++index) {
        /// getting disk number for n-th step where n = index
        const int diskNumber = getDiskNumber(index, diskAmount);
        /// generate looping step numbers from 1 to 3 
        const int stepNumber = (0 == index % 3) ? 3 : index % 3;
        switch (stepNumber) {
        case 1:
            /// step number 1
            printDirection(stepNumber, diskNumber, d1, d2);
            break;
        case 2:
            /// step number 2
            printDirection(stepNumber, diskNumber, d1, d3);
            break;
        case 3:
            /// step number 3
            printDirection(stepNumber, diskNumber, d2, d3);
            break;
        default: assert(0); break;
        }
    }   
}

inline int
getDiskNumber(const int stepsNumber, const int diskAmount)
{
    assert(stepsNumber >= 0 || diskAmount > 0);
    int diskNumber = 0;
    for (int i = 1; i <= diskAmount; ++i) {
        const int powerOfTwo = std::pow(2,i);
        if (std::pow(2,i-1) == stepsNumber % powerOfTwo) {
            diskNumber = i;
            break;
        }
    }
    return diskNumber;
}

inline void 
printDirection(const int step, const int diskNumber, const int start, const int end)
{
    const bool isEvenDiskNumber = step % 2 == diskNumber % 2;
    const int startPeg = isEvenDiskNumber ? start : end;
    const int finishPeg = isEvenDiskNumber ? end : start;
    std::cout << startPeg << " --> " << finishPeg << std::endl;
}

/// there is reguarity at the exact number of disk and the consequent steps
/// first rule is that the first disk make move at every odd numbered step
/// so if the step number is odd that means we move the disk No 1
/// at secont (the first step with even number) step we move the diks No 2 and it is repeated every 4 steps
/// at forth step (the second step with even number) we move the disk No 3 and it is repeated every 8 steps
/// so we have a formula which is generated in getDiskNumber function

