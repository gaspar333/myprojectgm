#include <iostream>
#include <unistd.h>
#include <cassert>

 int power(const int base, const int exponent);

int
main()
{
    const bool isInteractive = ::isatty(STDIN_FILENO);
    if (isInteractive) {
        std::cout << "Input a base number to make power calculation: ";
    }
    int base;
    std::cin >> base;
    if (isInteractive) {
        std::cout << "Input an exponentation number for power calculation: ";
    }
    int exponent;
    std::cin >> exponent;
    if (exponent < 1) {
        std::cerr << "Error 1: exponentation number must be higher 1!" << std::endl;
        return 1;
    }
    const int result = power(base, exponent);
    std::cout << "The result is " << result << std::endl;

    return 0;
}

inline int
power(const int base, const int exponent)
{
    assert(exponent > 0);
    if (1 == exponent) {
        return base;
    }
    if (0 == exponent % 2) {
        return power(base * base, exponent / 2);
    }
    return base * power(base, exponent - 1);
}

