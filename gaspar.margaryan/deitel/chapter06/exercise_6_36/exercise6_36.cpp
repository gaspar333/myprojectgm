#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cassert>

int randomNumber();
int randomAnswerNumber();
void multiplicationQuiz();
void rightAnswerResponse();
void wrongAnswerResponse();

int
main()
{
    std::srand(std::time(0));
    while (true) {
        multiplicationQuiz();
        std::cout << "Play again? (y/n) ";
        char option;
        std::cin >> option;
        if ('n' == option) {
            break;
        }
        if ('y' != option) {
            std::cerr << "Error 1: Wrong input." << std::endl;
            return 1;
        }
    }

    return 0;
}

inline int
randomNumber()
{
    const int number = std::rand() % 10;
    assert(number >= 0 && number <= 9);
    return number;
}

inline int
randomAnswerNumber()
{
    const int number = std::rand() % 4;
    assert(number >= 0 && number <= 3);
    return number;
}

inline void
multiplicationQuiz()
{
    const int number1 = randomNumber();
    const int number2 = randomNumber();
    while (true) {
        std::cout << "How much is " << number1 << " times " << number2 << "? ";
        int answer;
        std::cin >> answer;
        if (number1 * number2 == answer) {
            rightAnswerResponse();
            break;
        }
        wrongAnswerResponse();
    }
}

inline void
rightAnswerResponse()
{
    switch (randomAnswerNumber()) {
    case 0: std::cout << "Very good!\n";             break;
    case 1: std::cout << "Excellent!\n";             break;
    case 2: std::cout << "Nice work!\n";             break;
    case 3: std::cout << "Keep up the good work!\n"; break;
    default: assert(0);                              break;
    }
}

inline void
wrongAnswerResponse()
{
    switch (randomAnswerNumber()) {
    case 0: std::cout << "No. Please try again.\n"; break;
    case 1: std::cout << "Wrong. Try once more.\n"; break;
    case 2: std::cout << "Don't give up!\n";        break;
    case 3: std::cout << "No. Keep trying.\n";      break;
    default: assert(0);                             break;
    }
}

