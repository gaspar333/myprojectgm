#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <cmath>

double calculateCharges(const double hours);

int
main ()
{
    double totalParkingHours = 0.0;
    double totalCharge = 0.0;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter parking hours for 3 cars (1 - 24): " << std::endl;
    }
    std::cout << "Car \t" << "Hours \t" << "Charge" << std::endl;
    for (int i = 1; i <= 3; ++i) {
        double parkingHours;
        std::cin >> parkingHours;
        if (parkingHours <= 0 || parkingHours > 24) {
            std::cerr << "Error 1: wrong data input " << std::endl;
            return 1;
        }
        const double charge = calculateCharges(parkingHours);
        std::cout << std::fixed << i << std::setw(12) << std::setprecision(1) 
                  << parkingHours << std::setw(9) << std::setprecision(2) 
                  << charge << std::endl;
        totalParkingHours += parkingHours;
        totalCharge += charge;
    }
    std::cout << "TOTAL " << std::setw(7) << std::setprecision(1) 
              << totalParkingHours << std::setw(9) 
              << std::setprecision(2) << totalCharge << std::endl;
    
    return 0;
}

inline double
calculateCharges(const double hours)
{
    if (hours >= 19) {
        return 10.0;
    } 
    double charges = 2.0;
    if (hours > 3) {
        charges += std::ceil(hours - 3.0) * 0.5;
    }
    return charges;
}

