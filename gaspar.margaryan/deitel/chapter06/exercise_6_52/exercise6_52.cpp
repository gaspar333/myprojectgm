#include <iostream>
#include <cmath>
#include <cstdlib>
#include <iomanip>

int
main()
{
    std::cout << std::setprecision(3);
    std::cout << std::setw(13) << "Parameter" << std::setw(7) << "1.0" << std::setw(7) << "2.0" << std::setw(7) << "3.0"
              << std::setw(7) << "4.0" << std::setw(7) << "5.0" << std::setw(7) << "6.0" << std::endl;
    std::cout << "Math function" << std::endl;

    std::cout << std::setw(13) << "sin(x)" ;
    for (double i = 1.0; i < 7.0; ++i) {
        std::cout << std::setw(7) << std::sin(i);
    }
    std::cout << std::endl;
    
    std::cout << std::setw(13) << "cos(x)";
    for (double i = 1.0; i < 7.0; ++i) {
        std::cout << std::setw(7) << std::cos(i);
    }
    std::cout << std::endl;

    std::cout << std::setw(13) << "tan(x)";
    for (double i = 1.0; i < 7.0; ++i) {
        std::cout << std::setw(7) << std::tan(i);
    }
    std::cout << std::endl;

    std::cout << std::setw(13) << "log(x)";
    for (double i = 1.0; i < 7.0; ++i) {
        std::cout << std::setw(7) << std::log(i);
    }
    std::cout << std::endl;

    std::cout << std::setw(13) << "log10(x)";
    for (double i = 1.0; i < 7.0; ++i) {
        std::cout << std::setw(7) << std::log10(i);
    }
    std::cout << std::endl;

    std::cout << std::setw(13) << "exp(x)";
    for (double i = 1.0; i < 7.0; ++i) {
        std::cout << std::setw(7) << std::exp(i);
    }
    std::cout << std::endl;

    std::cout << std::setw(13) << "pow(x,y)";
    for (double i = 1.0; i < 7.0; ++i) {
        std::cout << std::setw(7) << std::pow(i, 2);
    }
    std::cout << std::endl;

    std::cout << std::setw(13) << "sqrt(x)";
    for (double i = 1.0; i < 7.0; ++i) {
        std::cout << std::setw(7) << std::sqrt(i);
    }
    std::cout << std::endl;

    std::cout << std::setw(13) << "ceil(x)";
    for (double i = 1.0; i < 7.0; ++i) {
        double randNumber = std::rand() % 10;
        double randFloatNumber = i + randNumber / 10.0;
        std::cout << std::setw(7) << std::ceil(randFloatNumber);
    }
    std::cout << std::endl;

    std::cout << std::setw(13) << "floor(x)";
    for (double i = 1.0; i < 7.0; ++i) {
        double randNumber = std::rand() % 10;
        double randFloatNumber = i + randNumber / 10.0;
        std::cout << std::setw(7) << std::floor(randFloatNumber);
    }
    std::cout << std::endl;

    std::cout << std::setw(13) << "fmod(x, y)";
    for (double i = 1.0; i < 7.0; ++i) {
        double randNumber1 = i + std::rand() % 10 / 10.0;
        double randNumber2 = i + std::rand() % 10 / 10.0;
        std::cout << std::setw(7) << std::fmod(randNumber1, randNumber2);
    }
    std::cout << std::endl;

    std::cout << std::setw(13) << "fabs(x)";
    for (double i = -1.0 ; i > -7.0; --i) {
        std::cout << std::setw(7) << std::fabs(i);
    }
    std::cout << std::endl;

    return 0;
}

