#include <iostream>
#include <unistd.h>

void hanoiTower(const int diskAmount, const int start, const int temp, const int finish);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input the amount of disks: ";
    }
    int diskAmount;
    std::cin >> diskAmount;
    if (diskAmount < 0) {
        std::cerr << "Error 1: wrong data input 1! \n";
        return 1;
    }
    const int start = 1;
    const int temp = 2;
    const int finish = 3;
    hanoiTower(diskAmount, start, finish, temp);

    return 0;
}

inline void
hanoiTower(const int diskAmount, const int start, const int finish, const int temp)
{
    if (1 == diskAmount) {
        std::cout << start << " --> " << finish << std::endl;
        return;
    }
    hanoiTower(diskAmount - 1, start, temp, finish);
    std::cout << start << " --> " << finish << std::endl;
    hanoiTower(diskAmount - 1, temp, finish, start);
}

