#include <iostream>
#include <cmath>
#include <unistd.h>

double distance(const double x1, const double y1, const double x2, const double y2);

int main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input coordinates (x,y) for the first point: ";
    }
    double x1;
    double y1;
    std::cin >> x1 >> y1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input coordinates (x,y) for the second point: ";
    }
    double x2;
    double y2;
    std::cin >> x2 >> y2;
    const double theDistance = distance(x1, y1, x2, y2);
    std::cout << "The distance between two points is " << theDistance << std::endl;

    return 0;
}

inline double
distance(const double x1, const double y1, const double x2, const double y2)
{
    const double deltaX = x1 - x2;
    const double deltaY = y1 - y2;
    const double value = std::sqrt(deltaX * deltaX + deltaY * deltaY);
    return value;
}

