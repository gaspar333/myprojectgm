#include <iostream>
#include <cmath>
#include <cassert>
#include <unistd.h>

bool isPrime(const int number);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The list of prime numbers from range 1 - 10000." << std::endl;
    }
    for (int i = 2; i <= 10000; ++i) {
        if (isPrime(i)) {
            std::cout  << i << std::endl;
        }
    }

    return 0;
}

/// Time complexity of function isPrime() is O(logN) compared to
/// simple iteration which time complexity is O(N).
/// In mathematics each number has it's square root as integer
/// or floating point number. For this task integer type square
/// roots are must, so the largest divisor every number
/// have is it's square root. It's pointless to iterate
/// the loop higher than the square root of the number.
inline bool
isPrime(const int number)
{
    assert(number > 1);
    const int sqrtNumber = static_cast<int>(std::sqrt(number));
    for (int i = 2; i <= sqrtNumber; ++i) {
        if (0 == number % i) {
            return false;
        }
    }
    return true;
}

