#include <iostream>
#include <unistd.h>

unsigned long factorial(const unsigned long number);
void printRecursionSteps(const unsigned long number);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input an integer for factorial calculation: ";
    }
    unsigned long number;
    std::cin >> number;
    for(unsigned long i = 0; i <= number; ++i) {
        const unsigned long factorialValue = factorial(i);
        std::cout << i << "! = " << factorialValue << std::endl;
    }

    return 0;
}

inline unsigned long
factorial(const unsigned long number)
{
    printRecursionSteps(number);
    if (number <= 1) {
        return 1;
    }
    return (number * factorial(number - 1));
}

inline void
printRecursionSteps(const unsigned long number)
{
    std::cout << "parameter = " << number << std::endl;
}

