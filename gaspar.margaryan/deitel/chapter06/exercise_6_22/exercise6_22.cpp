#include <iostream>
#include <cassert>
#include <unistd.h>

void printPattern(const int sideSize);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input the size of pattern side: ";
    }
    int sideSize;
    std::cin >> sideSize;
    if (sideSize < 0) {
        std::cerr << "Error 1: The size must be positive. " << std::endl;
        return 1;
    }
    printPattern(sideSize);
    
    return 0;
}

inline void
printPattern(const int sideSize)
{
    assert(sideSize >= 0);
    for (int i = 0; i < sideSize; ++i ) {
    	for (int j = 0; j < sideSize; ++j) {
    		std::cout << '*';
    	}
    	std::cout << std::endl;
    }
}

