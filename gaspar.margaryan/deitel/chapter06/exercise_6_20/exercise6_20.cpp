#include <iostream>
#include <unistd.h>
#include <cassert>

bool isMultipleTo(const int number1, const int number2);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input number 1: ";
    }
    int number1;
    std::cin >> number1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input number 2: ";
    }
    int number2;
    std::cin >> number2;
    if (0 == number2) {
        std::cerr << "Error 1: The second number can't be equal to 0." << std::endl;
        return 1;
    }
    std::cout << number2 << " is multiple of " << number1 << " = " << std::boolalpha
              << isMultipleTo(number1, number2) << std::endl;
    return 0;
}

inline bool
isMultipleTo(const int number1, const int number2)
{
    assert(number2 != 0);
    return 0 == number1 % number2;
}

