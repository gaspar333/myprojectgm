#include <iostream>
#include <cassert>
#include <unistd.h>
#include <stdlib.h>

enum ShapeType {RIGHT_TRIANGLE, RECTANGULAR, SOLID_RECTANGULAR, RHOMBUS, CIRCLE, X_SHAPE, Z_SHAPE, SHAPE_COUNT};

void drawShape(const ShapeType shape);
void drawRightTriangle(const int sideLength, const char fillSymbol);
void drawRectangle(const int heigth, const int length, const char fillSymbol);
void drawSolidRectangle(const int heigth, const int length, const char fillSymbol);
void drawRhombus(const int sideLength, const char fillSymbol);
void drawCircle(const int radius, const char fillSymbol);
void drawX(const int sideLength, const char fillSymbol);
void drawZ(const int sideLength, const char fillSymbol);
int getSideLength();
char getFillSymbol();
void outputError(const unsigned int errorNumber, const std::string& message);

ShapeType getInstructions();

int
main()
{
    ShapeType type;
    while ((type = getInstructions()) != SHAPE_COUNT) {
        drawShape(type);
    }

    return 0;
}

inline void
drawShape(const ShapeType type)
{
    switch (type) {
    case RIGHT_TRIANGLE: {
        const int sideLength = getSideLength();
        const char fillSymbol = getFillSymbol();
        drawRightTriangle(sideLength, fillSymbol);
    }
        break;
    case RECTANGULAR: {
        const int sideLength1 = getSideLength();
        const int sideLength2 = getSideLength();
        const char fillSymbol = getFillSymbol();
        drawRectangle(sideLength1, sideLength2, fillSymbol);
    }
        break;
    case SOLID_RECTANGULAR: {
        const int sideLength1 = getSideLength();
        const int sideLength2 = getSideLength();
        const char fillSymbol = getFillSymbol();
        drawSolidRectangle(sideLength1, sideLength2, fillSymbol);
    }
        break;
    case RHOMBUS: {
        const int sideLength = getSideLength();
        const char fillSymbol = getFillSymbol();
	drawRhombus(sideLength, fillSymbol);
    }
        break;
    case CIRCLE: {
        const int radius = getSideLength();
        const char fillSymbol = getFillSymbol();
	drawCircle(radius, fillSymbol);
    }
        break;
    case X_SHAPE: {
        const int sideLength = getSideLength();
        const char fillSymbol = getFillSymbol();
        drawX(sideLength, fillSymbol);
    }
        break;
    case Z_SHAPE: {
        const int sideLength = getSideLength();
        const char fillSymbol = getFillSymbol();
	drawZ(sideLength, fillSymbol);
    }
        break;
    default: outputError(1, "Wrong option input!"); break;
    }
}

inline void
drawRightTriangle(const int sideLength, const char fillSymbol)
{
    assert(sideLength >= 0);
    for (int i = 0; i < sideLength; ++i ) {
        for (int j = 0; j < sideLength; ++j) {
            if (j <= i) {
                std::cout << fillSymbol << ' ';
            }
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

inline void
drawRectangle(const int heigth, const int length, const char fillSymbol)
{
    assert(heigth >= 0 || length >= 0);
    for (int i = 1; i <= heigth; ++i ) {
        for (int j = 1; j <= length; ++j) {
            if ((j == 1 || j == length) || (i == 1 || i == heigth)) {
                std::cout << fillSymbol << ' ';
            } else {
                std::cout << "  ";
            }
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

inline void
drawSolidRectangle(const int heigth, const int length, const char fillSymbol)
{
    assert(heigth >= 0 || length >= 0);
    for (int i = 0; i < heigth; ++i ) {
        for (int j = 0; j < length; ++j) {
            std::cout << fillSymbol << ' ';
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

inline void
drawRhombus(const int sideLength, const char fillSymbol)
{
    assert(sideLength >= 0);
    int z = 0;
    const int rhombusSize = sideLength * 2 - 1;
    for (int i = 1; i <= rhombusSize; ++i ) {
        const int width = sideLength + z;
        const int spaceCount = sideLength - z;
        for (int j = 1; j <= width; ++j) {
            if (j < spaceCount) {
                std::cout << "  ";
            } else {
                std::cout << fillSymbol << ' ';
            }
        }
        z += (i <= (sideLength - 1)) ? 1 : -1;
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

inline void
drawCircle(const int radius, const char fillSymbol)
{
    assert(radius >= 0);
    const int radiusSquare = radius * radius;
    const int coef = radius / 2 + 1;
    for (int i = -radius; i <= radius; ++i) {
        for (int j = -radius; j <= radius; ++j) {
            const int pythSquare = i * i + j * j; /// pythagoras theorem
            const int delta = radiusSquare - pythSquare;
            if (delta > -coef && delta < coef) {
                std::cout << fillSymbol << " ";
            } else {
                std::cout << "  ";
            }
        }
        std::cout << std::endl;
    }
}

inline void
drawX(const int sideLength, const char fillSymbol)
{
    assert(sideLength >= 0);
    for (int i = 0; i < sideLength; ++i ) {
        for (int j = 0; j < sideLength; ++j) {
            if (j == sideLength - 1 - i) {
                std::cout << fillSymbol << ' ';
            } else if (j != i) {
                std::cout << "  ";
            } else {
                std::cout << fillSymbol << ' ';
            }
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

inline void
drawZ(const int sideLength, const char fillSymbol)
{
    assert(sideLength >= 0);
    for (int i = 0; i < sideLength; ++i ) {
        for (int j = sideLength - 1; j >= 0; --j) {
            if (i == 0 || i == sideLength - 1 || i == j) {
                std::cout << fillSymbol << ' ';
            } else {
                std::cout << "  ";
            }
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

inline void
outputError(const unsigned int errorNumber, const std::string& message)
{
    std::cerr << "Error " << errorNumber << ": " << message << std::endl;
    ::exit(errorNumber);
}

inline int
getSideLength()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input pattern's side length: ";
    }
    int sideLength;
    std::cin >> sideLength;
    if (sideLength < 0) {
    	outputError(2, "The side length must be positive!");
    }
    assert(sideLength >= 0);
    return sideLength;
}

inline char
getFillSymbol()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input character for pattern: ";
    }
    char fillCharacter;
    std::cin >> fillCharacter;
    return fillCharacter;
}

inline ShapeType
getInstructions()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input a number to draw a shape\n"
                  << "\t0 - Right Triangle\n"
                  << "\t1 - Rectangle\n"
                  << "\t2 - Solid rectangle\n"
                  << "\t3 - Rhombus\n"
                  << "\t4 - Circle\n"
                  << "\t5 - X shape\n"
                  << "\t6 - Z shape\n"
                  << "\t7 - Quit\n"
                  << "\tNumber: ";
    }
    int number;
    std::cin >> number;
    return static_cast<ShapeType>(number);
}

