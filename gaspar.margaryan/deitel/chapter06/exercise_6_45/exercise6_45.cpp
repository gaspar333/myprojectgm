#include <iostream>
#include <cmath>
#include <cassert>
#include <unistd.h>

/// for the function gcd algorithm 'x' must be larger than 'y'
int gcd(const int x, const int y); /// greatest common divisor
void outputWarning(const unsigned int warningNumber, const std::string& message);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input two numbers to determine the greatest common divisor: ";
    }
    int x;
    int y;
    std::cin >> x >> y;
    if (y > x) {
        std::cerr << "Error 1: The value of 'x' should be larger than 'y'.";
        return 1;
    }
    const int gCDivisor = gcd(x, y);
    std::cout << "The greatest common divisor of numbers " << x << " and "
              << y << " is " << gCDivisor << std::endl;

    return 0;
}

inline int
gcd(const int x, const int y)
{
    assert(x > y);
    if (0 == y) {
        return x;
    }
    return gcd(y, x % y);
}

