#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

int getRandomNumber();
int guessNumber();
void printResultInfo(const int counter);

int
main()
{
    std::srand(std::time(0));
    while (true) {
        printResultInfo(guessNumber());
        std::cout << "Play again? (y/n)";
        char option;
        std::cin >> option;
        if ('n' == option) {
            break;
        }
        if ('y' != option) {
            std::cerr << "Error 1: Wrong input." << std::endl;
            return 1;
        }
    }


    return 0;
}

inline int
getRandomNumber()
{
    return 1 + std::rand() % 1000;
}

inline int
guessNumber()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "I have a number between 1 and 1000.\n"
                  << "Can you guess my number?\n"
                  << "Please type your first guess: ";
    }
    const int number = getRandomNumber();
    int counter = 0;
    while (true) {
        int answer;
        std::cin >> answer;
        ++counter;
        if (number == answer) {
            std::cout << "Excellent! You guessed the number!\n";
            break;
        } 
        if (answer < number) {
            std::cout << "Too low. Try again.\n";
        } else {
            std::cout << "Too high. Try again.\n";
        }
    }

    return counter;
}

inline void
printResultInfo(const int counter)
{
    if (counter <= 10) {
        std::cout << "Either you know the secret or you got lucky!" << std::endl;
        if (10 == counter) {
            std::cout << "Ahah! You know the secret!" << std::endl;
        }
    } else {
        std::cout << "You should be able to do better!" << std::endl;
    }
}

/// consider the program generated number 756, step by step we input guesses
/// by searching the number between last inputed maximal and minimal range.
/// We should round down or round up divisions into integers.
/// 1. 500 (1000/2) first hint ->
/// 2. 750 (750+(1000-500)/2)  ->
/// 3. 875 (750+(875-750)/2)   ->
/// 4. 813 (750+(813-750)/2)   ->
/// 5. 782 (750+(813-750)/2)   ->
/// 6. 766 (750+(782-750)/2)   ->
/// 7. 758 (750+(766-750)/2)   ->
/// 8. 754 (750+(758-750)/2)   ->
/// 9. 756 (754+(758-754)/2)   -> guessed!
/// In this way we can guess the number within maximal 10 guesses.

