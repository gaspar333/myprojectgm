#include <iostream>
#include <unistd.h>

bool isEven(const int number);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input a number: ";
    }
    int number;
    std::cin >> number;
    std::cout << "Number " << number << " is even = "
              << std::boolalpha << isEven(number) << std::endl;

    return 0;
}

inline bool
isEven(const int number)
{
    return 0 == (number & 1);
}

