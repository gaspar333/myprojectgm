#include <iostream>
#include <cstdlib>
#include <ctime>

int
main()
{
    std::srand(std::time(0));

    std::cout << "Random number in the range 1 -> 2: \n";
    int n = 1 + std::rand() % 2;
    std::cout << n << std::endl;

    std::cout << "Random number in the range 1 -> 100: \n";
    n = 1 + std::rand() % 100;
    std::cout << n << std::endl;           

    std::cout << "Random number in the range 0 -> 9: \n";
    n = std::rand() % 10;
    std::cout << n << std::endl;

    std::cout << "Random number in the range 1000 -> 1112: \n";
    n = 1000 + std::rand() % 113;
    std::cout << n << std::endl;

    std::cout << "Random number in the range -1 -> 1: \n";
    n = -1 + std::rand() % 3;
    std::cout << n << std::endl;

    std::cout << "Random number in the range -3 -> 11: \n";
    n = -3 + std::rand() % 15;
    std::cout << n << std::endl;

    return 0;
}

