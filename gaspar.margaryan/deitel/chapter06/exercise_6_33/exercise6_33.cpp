#include <iostream>
#include <unistd.h>
#include <cassert>

int qualityPoints(const int average); 

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input student's average to determine the grade point: ";
    }
    int average;
    std::cin >> average;
    if (average < 0 || average > 100) {
        std::cerr << "Error 1: wrong average value! " << std::endl;
        return 1;
    }
    std::cout << "The quality point of the student is " << qualityPoints(average) << std::endl;

    return 0;
}

inline int
qualityPoints(const int average)
{
    assert(average >= 0 && average <= 100);
    int rate = average / 10 - 5;
    if (100 == average) {
        rate = average / 10 - 6;
    }
    if (average < 60) {
        return 0;
    }
    return rate;
}

