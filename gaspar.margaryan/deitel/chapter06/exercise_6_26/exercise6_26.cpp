#include <iostream>
#include <cmath>
#include <unistd.h>
#include <cassert>

int timeInSeconds(const int hours, const int minutes, const int seconds);
bool isValidTime(const int hours, const int minutes, const int seconds);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input the first time /hours, minutes, seconds/: ";
    }
    int hours1;
    int minutes1;
    int seconds1;
    std::cin >> hours1 >> minutes1 >> seconds1;
    if (!isValidTime(hours1, minutes1, seconds1)) {
        std::cerr << "Error 1: Wrong time input." << std::endl;
        return 1;
    }
    const int firstTimeInSeconds = timeInSeconds(hours1, minutes1, seconds1);

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input the second time /hours, minutes, seconds/: ";
    }
    int hours2;
    int minutes2;
    int seconds2;
    std::cin >> hours2 >> minutes2 >> seconds2;
    if (!isValidTime(hours2, minutes2, seconds2)) {
        std::cerr << "Error 1: Wrong time input." << std::endl;
        return 1;
    }
    const int secondTimeInSeconds = timeInSeconds(hours2, minutes2, seconds2);
    const int timeDifference = std::fabs(firstTimeInSeconds - secondTimeInSeconds);
    std::cout << "Time difference in seconds: " << timeDifference << std::endl;

    return 0;
}

inline int 
timeInSeconds(const int hours, const int minutes, const int seconds)
{
    assert(isValidTime(hours, minutes, seconds));
    const int timeInSeconds = hours * 3600 + minutes * 60 + seconds;
    return timeInSeconds;
}

inline bool
isValidTime(const int hours, const int minutes, const int seconds)
{
    const bool validTime = (hours >= 0   && hours   <= 12) &&
                           (minutes >= 0 && minutes <= 59) &&
                           (seconds >= 0 && seconds <= 59);
    return validTime;
}

