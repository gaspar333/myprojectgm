#include <iostream>
#include <unistd.h>

double smallest(const double number1, const double number2, const double number3);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input three floting point numbers: ";
    }
    double numberD1;
    double numberD2;
    double numberD3;
    std::cin >> numberD1 >> numberD2 >> numberD3;
    const double smallestValue = smallest(numberD1, numberD2, numberD3); 
    std::cout << "The smallest number is " << std::fixed 
              << smallestValue << std::endl;

    return 0;
}

inline double
smallest(const double number1, const double number2, const double number3)
{
    double smallest = number1;
    if (number2 < smallest) {
        smallest = number2;
    }
    if (number3 < smallest) {
        smallest = number3;
    }
    return smallest;
}

