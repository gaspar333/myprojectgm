#include <iostream>
#include <iomanip>
#include <cassert>

double celsius(const double degree);
double fahrenheit(const double degree);
void printTemperatureTable();

int 
main()
{
    printTemperatureTable();
    return 0;
}

inline double
celsius(const double degree)
{
    assert(degree >= 0 && degree <= 100);
    const double result = degree * 9.0 / 5.0 + 32;
    return result;
}

inline double
fahrenheit(const double degree)
{
    assert(degree >= 32 && degree <= 212);
    const double result = (degree - 32.0) * 5.0 / 9.0;
    return result;
}

inline void
printTemperatureTable()
{
    std::cout << "The table of temperatures (celsius to fahrenheit): " << std::endl;
    std::cout << std::fixed << std::setprecision(1);
    for (int i = 0; i <= 100; ++i) {
        std::cout << std::setw(3) << i << " --> " << std::setw(3)
                  << celsius(static_cast<double>(i)) << std::endl;
    }
    std::cout << std::endl;
    std::cout << "The table of temperatures (fahrenheit to celsius): " << std::endl;
    for (int i = 32; i <= 212; ++i) {
        std::cout << std::setw(3) << i << " --> " << std::setw(3)
                  << fahrenheit(static_cast<double>(i)) << std::endl;
    }
}

