#include <iostream>
#include <cassert>
#include <iomanip>
#include <cmath>
#include <limits>

template <typename T>
T fibonacciIterative(T& counter, T& fibonacci0, T& fibonacci1);

int
main()
{
    int fibonacciI0 = 0;
    int fibonacciI1 = 1;
    int counterI = 1; /// this counter will count int fibonacci's n-th index for int_max
    const int fibonacciInt = fibonacciIterative(counterI, fibonacciI0, fibonacciI1);
    std::cout << "FibonacciInteger(" << counterI << ") = " << fibonacciInt << std::endl;
    double counterD = static_cast<double>(counterI); /// this counter will count double fibonacci's n-th index for double_max
    double fibonacciD0 = static_cast<double>(fibonacciI0);
    double fibonacciD1 = static_cast<double>(fibonacciI1);
    const double fibonacciD = fibonacciIterative(counterD, fibonacciD0, fibonacciD1);
    std::cout << std::setprecision(0) << std::fixed << "FibonacciDouble(" 
              << counterD << ") = " << fibonacciD << std::endl;

    return 0;
}

template <typename T>
inline T
fibonacciIterative(T& counter, T& fibonacci0, T& fibonacci1)
{
    while (true) {
        T fibonacciNumber = fibonacci0 + fibonacci1;
        if (fibonacciNumber > std::numeric_limits<T>::max() || fibonacciNumber < 0) {
            break;
        }
        fibonacci0 = fibonacci1;
        fibonacci1 = fibonacciNumber;
        ++counter;
        assert(fibonacciNumber < std::numeric_limits<T>::max() && fibonacciNumber >= 0);
    }
    return fibonacci1;
}

