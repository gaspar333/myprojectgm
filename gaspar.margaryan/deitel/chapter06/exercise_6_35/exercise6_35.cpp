#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

int getRandomNumber();
void multiplicationQuiz();

int 
main()
{
    std::srand(std::time(0));
    while (true) {
        multiplicationQuiz();
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Play again? (y/n) " << std::endl;
        }
        char option;
        std::cin >> option;
        if ('n' == option) {
            break;
        }
        if ('y' != option) {
            std::cerr << "Error 1: Wrong input." << std::endl;
            return 1;
        }
    }
    return 0;
}

inline int
getRandomNumber()
{
   return std::rand() % 10;
}

inline void
multiplicationQuiz()
{
    const int number1 = getRandomNumber();
    const int number2 = getRandomNumber();
    while (true) {
        std::cout << "How much is " << number1 << " times "
                  << number2 << "? ";
        int answer;
        std::cin >> answer;
        if (number1 * number2 == answer) {
            std::cout << "Very good! \n";
            break;
        }
        std::cout << "No. Please try again. \n";
    }
}

