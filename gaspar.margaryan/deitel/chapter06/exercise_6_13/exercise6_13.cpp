#include <iostream>
#include <cmath>
#include <unistd.h>

double roundNumber(const double x);

int
main()
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Please input a number or 'end of file' to exit. " << std::endl;
        }
        double number = 0.0;
        std::cin >> number;
        if (std::cin.eof()) {
            break;
        }
        const double roundedNumber = roundNumber(number);
        std::cout << "The original number is " << number << std::endl;
        std::cout << "The rounded number is  " << roundedNumber << std::endl;
    }

    return 0;
}

inline double
roundNumber(const double x)
{
    return std::floor(x + 0.5);
}

