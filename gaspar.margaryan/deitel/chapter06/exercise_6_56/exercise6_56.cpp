#include <iostream>
#include <unistd.h>

int tripleByValue(int number);
int tripleByReference(int &number);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input a count: ";
    }
    int count;
    std::cin >> count;
    std::cout << "The result of tripleByValue function " << tripleByValue(count) << std::endl;
    std::cout << "The value of count after tripleByValue function call " << count << std::endl;
    std::cout << "The result of tripleByReference function " << tripleByReference(count) << std::endl;
    std::cout << "The value of count after tripleByReference function call " << count << std::endl;

    return 0;
}

inline int
tripleByValue(int number)
{
    return number *= 3;
}

inline int
tripleByReference(int &number)
{
    return number *= 3;
}

