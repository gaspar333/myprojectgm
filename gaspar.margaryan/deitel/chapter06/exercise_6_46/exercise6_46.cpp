#include <iostream>

int
main()
{
    static int count = 1;
    std::cout << count << " ";
    ++count;
    if (10 == count) {
        return 0;
    }
    return main();
}

