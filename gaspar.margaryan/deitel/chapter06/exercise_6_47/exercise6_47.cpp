#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <unistd.h>
#include <stdlib.h>

enum Arithmetic {ADDITION = 1, SUBTRACTION, MULTIPLICATION, DIVISION, RANDOM};

int randomNumber(const int digit);
int randomResponse();
int addition(const int number1, const int number2);
int subtraction(const int number1, const int number2);
int multiplication(const int number1, const int number2);
int division(const int number1, const int number2);
void printCorrectAnswerResponse(const int randomNumber);
void printWrongAnswerResponse(const int randomNumber);
void outputError(const unsigned int errorNumber, const std::string& message);
int mathCalculation(const Arithmetic mathStatement, const int number1, const int number2);

Arithmetic getProblem();

int
main()
{
    std::srand(std::time(0));
    int wrongAnswers = 0;
    int correctAnswers = 0;
    while (correctAnswers + wrongAnswers < 10) {
        std::cout << "Input grade level for calculation (1 - 3): ";
        int gradeLevel;
        std::cin >> gradeLevel;
        if (gradeLevel < 1 || gradeLevel > 3) {
            outputError(1, "Wrong grade level!");
        }
        const int number1 = randomNumber(gradeLevel);
        const int number2 = randomNumber(gradeLevel);
        const int result = mathCalculation(getProblem(), number1, number2);
        while ((correctAnswers + wrongAnswers) < 10) {
            int answer;
            std::cin >> answer;
            if (result == answer) {
                ++correctAnswers;
                printCorrectAnswerResponse(randomResponse());
                break;
            }
            printWrongAnswerResponse(randomResponse());
            ++wrongAnswers;
        }
    }
    if (0 == correctAnswers + wrongAnswers) {
        outputError(2, "Zero division error!");
    }
    const int answerPercentage = correctAnswers * 100 / (correctAnswers + wrongAnswers);
    std::cout << "Your correct answer percentage is " << answerPercentage << "%" << std::endl;
    if (answerPercentage < 75) {
        std::cout << "Please ask your instructor for extra help." << std::endl;
    }

    return 0;
}

inline int
randomNumber(const int digit)
{ 
    int randomNumber;
    switch (digit) {
    case 1:  randomNumber = std::rand() % 10;        break;
    case 2:  randomNumber = 10 + std::rand() % 90;   break;
    case 3:  randomNumber = 100 + std::rand() % 900; break;
    default: assert(0);                              break;
    }
    assert(randomNumber >= 0 && randomNumber <= 999);
    return randomNumber;
}

inline int
randomResponse()
{
    return std::rand() % 4;
}

inline Arithmetic
getProblem()
{
    std::cout << "Choose an arithmetic problem:\n"
              << "\t1 - Addition\n"
              << "\t2 - Subtraction\n"
              << "\t3 - Multiplication\n"
              << "\t4 - Division\n"
              << "\t5 - Random"
              << std::endl;
    int number;
    std::cin >> number;
    return static_cast<Arithmetic>(number);
}

inline int
addition(const int number1, const int number2)
{
    std::cout << "How much is " << number1 << " added to " << number2 
              << " ?" << std::endl;
    return number1 + number2;
}

inline int
subtraction(const int number1, const int number2)
{
    std::cout << "How much is " << number1 << " subtracted to " << number2 
              << " ?" << std::endl;
    return number1 - number2;
}

inline int
multiplication(const int number1, const int number2)
{
    std::cout << "How much is " << number1 << " multiplied to " << number2 
              << " ?" << std::endl;
    return number1 * number2;
}

inline int
division(const int number1, const int number2)
{
    assert(number2 != 0);
    std::cout << "How much is " << number1 << " divided to " << number2 
              << " ?" << std::endl;
    return number1 / number2;
}

void
printCorrectAnswerResponse(const int randomNumber)
{
    switch (randomNumber) {
    case 0:  std::cout << "Very good!\n";             break;
    case 1:  std::cout << "Excellent!\n";             break;
    case 2:  std::cout << "Nice work!\n";             break;
    case 3:  std::cout << "Keep up the good work!\n"; break;
    default: assert(0);                               break;
    }
}

void
printWrongAnswerResponse(const int randomNumber)
{
    switch (randomNumber) {
    case 0:  std::cout << "No. Please try again.\n"; break;
    case 1:  std::cout << "Wrong. Try once more.\n"; break;
    case 2:  std::cout << "Don't give up!\n";        break;
    case 3:  std::cout << "No. Keep trying.\n";      break;
    default: assert(0);                              break;
    }
}

inline int 
mathCalculation(const Arithmetic mathStatement, const int number1, const int number2)
{
    int calcResult;
    switch (mathStatement) {
    case 1:  calcResult = addition(number1, number2);                                                 break;
    case 2:  calcResult = subtraction(number1, number2);                                              break;
    case 3:  calcResult = multiplication(number1, number2);                                           break;
    case 4:  calcResult = division(number1, number2);                                                 break;
    case 5:  calcResult = mathCalculation(static_cast<Arithmetic>(1 + rand() % 5), number1, number2); break;
    default: assert(0);                                                                               break;
    }
    return calcResult;
}

inline void
outputError(const unsigned int errorNumber, const std::string& message)
{
    std::cout << "Error " << errorNumber << ": " << message << std::endl;
    ::exit(errorNumber);
}

