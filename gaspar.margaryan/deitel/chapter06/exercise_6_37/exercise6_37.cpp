#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cassert>

int randomNumber();
int randomAnswerNumber();
void multiplicationQuiz();
void rightAnswerResponse();
void wrongAnswerResponse();
int getAnswerPercentage(const int correctAnswer, const int wrongAnswer);
void printResultInfo(const int percentage);

int
main()
{
    std::srand(std::time(0));

    multiplicationQuiz();

    return 0;
}

inline int
randomNumber()
{
    return std::rand() % 10;
}

inline int
randomAnswerNumber()
{
    return std::rand() % 4;
}

inline void
multiplicationQuiz()
{
    int wrongAnswers = 0;
    int correctAnswers = 0;
    while ((correctAnswers + wrongAnswers) < 10) {
        const int number1 = randomNumber();
        const int number2 = randomNumber();
        while ((correctAnswers + wrongAnswers) < 10) {
            std::cout << "How much is " << number1 << " times "
                      << number2 << "? ";
            int answer;
            std::cin >> answer;
            if (number1 * number2 == answer) {
                ++correctAnswers;
                rightAnswerResponse();
                break;
            }
            ++wrongAnswers;
            wrongAnswerResponse();
        }
    }
    const int percentage = getAnswerPercentage(correctAnswers, wrongAnswers);
    printResultInfo(percentage);
}

inline void
rightAnswerResponse()
{
    switch (randomAnswerNumber()) {
    case 0: std::cout << "Very good!\n";             break;
    case 1: std::cout << "Excellent!\n";             break;
    case 2: std::cout << "Nice work!\n";             break;
    case 3: std::cout << "Keep up the good work!\n"; break;
    default: assert(0);                              break;
    }
}

inline void
wrongAnswerResponse()
{
    switch (randomAnswerNumber()) {
    case 0: std::cout << "No. Please try again.\n"; break;
    case 1: std::cout << "Wrong. Try once more.\n"; break;
    case 2: std::cout << "Don't give up!\n";        break;
    case 3: std::cout << "No. Keep trying.\n";      break;
    default: assert(0);                             break;
    }
}

int
getAnswerPercentage(const int correctAnswers, const int wrongAnswers)
{
    assert( 0 != (wrongAnswers + correctAnswers));
    const int percentage = correctAnswers * 100 / (wrongAnswers + correctAnswers);
    return percentage;
}

inline void
printResultInfo(const int percentage)
{
    std::cout << "Your correct answer percentage is " << percentage << std::endl;
    if (percentage < 75) {
        std::cout << "Please ask your instructor for extra help." << std::endl;
    }
}

