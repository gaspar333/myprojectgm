#include <iostream>
#include <cmath>
#include <cassert>

int mystery(int a, int b);

int
main()
{
    std::cout << "Enter two integers: ";
    int x;
    int y;
    std::cin >> x >> y;
    std::cout << "The result is " << mystery(x, y) << std::endl;

    return 0;
}

inline int
mystery(int a, int b)
{
    /// supports negative x negative
    /// or positive x negative multiplication result
    if (b < 0) {
        a *= -1;
        b *= -1;
    }
    if (1 == b) {
        return a;
    }
    assert(b > 0);
    return a + mystery(a, b - 1);
}

