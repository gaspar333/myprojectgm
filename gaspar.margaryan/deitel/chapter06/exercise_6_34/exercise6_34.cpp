#include <iostream>
#include <cstdlib>
#include <ctime>

int flip();

int main()
{
    std::srand(std::time(0));
    int headCounter = 0;
    int tailCounter = 0;
    for (int i = 1; i <= 100; ++i) {
        if (flip() == 0) {
            std::cout << "Tails ";
            ++tailCounter;
        } else {
            std::cout << "Heads ";
            ++headCounter;
        }
        if (i % 10 == 0) {
            std::cout << std::endl;
        }
    }
    std::cout << "\nTotal number of tails - " << tailCounter;
    std::cout << "\nTotal number of heads - " << headCounter;

    return 0;
}

inline int
flip()
{
   return std::rand() % 2;
}

