/// Craps simulation.
#include <iostream>
#include <cstdlib> 
#include <ctime>
#include <cassert>
#include <unistd.h>

/// enumeration with constants that represent the game status 
enum Status {CONTINUE, WON, LOST}; /// all caps in constants

int rollDice();
bool crapsGame();
void chatter();
void printBankBalance(const unsigned int balance);

int
main()
{
    /// randomize random number generator using current time
    std::srand(::time(0)); 
    const bool isInteractive = ::isatty(STDIN_FILENO);

    unsigned int bankBalance = 1000;
    while (bankBalance > 0) {
        printBankBalance(bankBalance);
        if (isInteractive) {
            std::cout << "Please, make your bet: ";
        }
        unsigned int wager = 0;
        while (true) {
            std::cin >> wager;
            if (wager <= bankBalance) {
                break;
            }
            if (isInteractive) {
                std::cout << "You don't have enough money. Please, reenter valid wager." << std::endl;
            }
            printBankBalance(bankBalance);
        }
        const bool gameIsWon = crapsGame();
        if (gameIsWon) {
            bankBalance += wager;
        } else {
            bankBalance -= wager;
        }
    }
    if (0 == bankBalance) {
        printBankBalance(bankBalance);
        std::cout << "Sorry. You busted!!!" << std::endl;
    }

    return 0; /// indicates successful termination
} /// end main


inline bool
crapsGame()
{
    int myPoint = 0; /// point if no win or loss on first roll
    Status gameStatus; /// can contain CONTINUE, WON or LOST

    const int sumOfDice = rollDice(); /// first roll of the dice

    /// determine game status and point (if needed) based on first roll
    switch (sumOfDice) {
    case 7: /// win with 7 on first roll
    case 11: /// win with 11 on first roll
        gameStatus = WON;
        chatter();
        break;
    case 2: /// lose with 2 on first roll
    case 3: /// lose with 3 on first roll
    case 12: /// lose with 12 on first roll
        gameStatus = LOST;
        chatter();
        break;
    default: /// did not win or lose, so remember point
        gameStatus = CONTINUE; /// game is not over
        chatter();
        myPoint = sumOfDice; /// remember the point
        std::cout << "Point is " << myPoint << std::endl;
        break; /// optional at end of switch
    } /// end switch

    /// while game is not complete
    while (CONTINUE == gameStatus) { /// not WON or LOST
        const int nextSumOfDice = rollDice(); // roll dice again

        /// determine game status
        if (myPoint == nextSumOfDice) { /// win by making point
            gameStatus = WON;
        }
        if (7 == nextSumOfDice) { /// lose by rolling 7 before point
            gameStatus = LOST;
        }
    } /// end while
    /// display won or lost message
    if (WON == gameStatus) {
        std::cout << "Player wins!!!" << std::endl;
        return true;
    }
    std::cout << "Player loses!!!" << std::endl;
    return false;
}


/// roll dice, calculate sum and display results
inline int
rollDice()
{
    /// pick random die values
    const int die1 = 1 + rand() % 6; /// first die roll
    const int die2 = 1 + rand() % 6; /// second die roll

    const int sum = die1 + die2; // compute sum of die values
    assert(sum >= 2 && sum <= 12);
    /// display results of this roll
    std::cout << "Player rolled " << die1 << " + " << die2
         << " = " << sum << std::endl;
    return sum; /// end function rollDice
} /// end function rollDice

inline void
chatter()
{
    const int dialogue = rand() % 6;
    switch (dialogue) {
    case 0: std::cout << "Oh, you're going for broke, huh?" << std::endl;                     break;
    case 1: std::cout << "Aw cmon, take a chance!" << std::endl;                              break;
    case 2: std::cout << "You're up big. Now's the time to cash in your chips!" << std::endl; break;
    case 3: std::cout << "Your money smells mine! Sniff sniff" << std::endl;                  break;
    case 4: std::cout << "Don't give up, head up!" << std::endl;                              break;
    case 5: std::cout << "Keep rolling!" << std::endl;                                        break;
    default: assert(0);                                                                       break;
    }
}

void
printBankBalance(const unsigned int balance)
{
    std::cout << "Your current bank balance is " << balance << "$" << std::endl;
}

