#include <iostream>
#include <limits.h>
#include <cassert>
#include <unistd.h>

unsigned int getReversedNumber(unsigned int number);
unsigned int quotient(const unsigned int number1, const unsigned int number2);
unsigned int reminder(const unsigned int number1, const unsigned int number2);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Please input an integer: ";
    }
    unsigned int number;
    std::cin >> number;
    const unsigned int reversedNumber = getReversedNumber(number);
    std::cout << reversedNumber << std::endl;

    return 0;
}

inline unsigned int
getReversedNumber(unsigned int number)
{
    unsigned int reversedNumber = 0;
    unsigned int divisor = 1000000000;
    while (divisor > number) {
        divisor = quotient(divisor, 10);
    }
    while (number != 0) {
        reversedNumber += reminder(number, 10) * divisor;
        number = quotient(number, 10);
        divisor = quotient(divisor, 10);
    }
    return reversedNumber;
}

inline unsigned int
quotient(const unsigned int number1, const unsigned int number2)
{
    assert(number2 > 0);
    return number1 / number2;
}

inline unsigned int
reminder(const unsigned int number1, const unsigned int number2)
{
    assert(number2 > 0);
    return number1 % number2;
}

