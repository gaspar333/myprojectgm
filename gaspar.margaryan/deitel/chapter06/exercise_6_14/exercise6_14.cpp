#include <iostream>
#include <cmath>
#include <unistd.h>

double roundToInteger(const double x);
double roundToTenths(const double x);
double roundToHundredths(const double x);
double roundToThousandths(const double x);

int
main()
{
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input number (EOF to exit): ";
        }
        double number;
        std::cin >> number;
        if (std::cin.eof()) {
            break;
        }
        std::cout << "The original value      " << number                     << std::endl;
        std::cout << "Rounded to integer      " << roundToInteger(number)     << std::endl;
        std::cout << "Rounded to tenths       " << roundToTenths(number)      << std::endl;
        std::cout << "Rounded to hundredths   " << roundToHundredths(number)  << std::endl;
        std::cout << "Rounded to thousandths  " << roundToThousandths(number) << std::endl;
        std::cout << std::endl;
    }

    return 0;
}

inline double
roundToInteger(const double x)
{
    return std::floor(x + 0.5);
}

inline double
roundToTenths(const double x)
{
    return std::floor(x * 10 + 0.5) / 10;
}

inline double
roundToHundredths(const double x)
{
    return std::floor(x * 100 + 0.5) / 100;
}

inline double
roundToThousandths(const double x)
{
    return std::floor(x * 1000 + 0.5) / 1000;
}

