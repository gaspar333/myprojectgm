#include <iostream>

bool isPerfectNumber(const int number);
void printPerfectList();
void printDivisors(const int number);

int
main()
{
    printPerfectList();

    return 0;
}

inline bool
isPerfectNumber(const int number)
{
    const int halfNumber = number / 2;
    int divisorSum = 0;
    for (int i = 1; i <= halfNumber; ++i) {
        if (number % i == 0) {
            divisorSum += i;
        }
    }
    return divisorSum == number; 
}

inline void
printPerfectList()
{
    std::cout << "The list of perfect numbers from range 1 - 10000:" << std::endl;
    for (int i = 1; i <= 10000; ++i) {
        if (isPerfectNumber(i)) {
            std::cout << i << ": ";
            printDivisors(i);
        }
    }
}

inline void
printDivisors(const int number)
{
    const int halfNumber = number / 2;
    for (int i = 1; i <= halfNumber; ++i) {
        if (number % i == 0) {
            std::cout << i << " ";
        }
    }
    std::cout << std::endl;
}

/// checked with global integer counter for all functions
/// and all for loops it implements 25014329 times
