/// Exercise3.11: main.cpp
/// GradeBook class demonstration after separating
/// its interface from its implementation.
#include "GradeBook.hpp" /// include definition of class GradeBook
#include <iostream> /// include standard library input output stream

/// function main begins program execution
int
main()
{
/// create GradeBook object
    GradeBook gradeBook("CS101 Introduction to C++ Programming", "Prof. John Doe");

/// display values of courseName and InstructorName for GradeBook
    gradeBook.displayMessage();
    return 0; // indicate successful termination
} /// end main
