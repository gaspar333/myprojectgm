/// Exercise3.15: main.cpp
/// Date class demonstration after separating
/// its interface from its implementation.
#include "Date.hpp" /// include definition of class Date
#include <iostream> /// include standard library input output stream

/// function main begins program execution
int
main()
{
    Date myDate(1999, 12, 31);

    std::cout << "The current date is \n";
    myDate.displayMessage();

    myDate.setYear(2019);
    myDate.setMonth(5);
    myDate.setDay(31);

    std::cout << "The up date is \n";
    myDate.displayMessage();

    return 0; /// function main terminated successfully
} /// end of function main

