/// Exercise3.14: main.cpp
/// Employee class demonstration after separating
/// its interface from its implementation.
#include "Employee.hpp" /// include definition of class Employee
#include <iostream> /// include standard library input output stream

/// function main begins program execution
int
main()
{
    Employee employee1("Bruce", "Lee", 100000);
    Employee employee2("Jackie", "Chan", 650000);
/// I gave a little form of invoice to print on the screen
    std::cout << "Employee info\n";
    std::cout << "First Name\t" << "Last Name\t" << "Yearly Salary\n\n";
    std::cout << employee1.getFirstName() << "\t\t" << employee1.getLastName() << "\t\t" << employee1.getSalary() << std::endl;
    std::cout << employee2.getFirstName() << "\t\t" << employee2.getLastName() << "\t\t" << employee2.getSalary() << "\n" << std::endl;

    employee1.setSalary(employee1.getSalary() * 1.1);
    employee2.setSalary(employee2.getSalary() * 1.1);
    std::cout << "Employee updated info after salary 10% raise\n";
    std::cout << "First Name\t" << "Last Name\t" << "Yearly Salary\n\n";
    std::cout << employee1.getFirstName() << "\t\t" << employee1.getLastName() << "\t\t" << employee1.getSalary() << std::endl;
    std::cout << employee2.getFirstName() << "\t\t" << employee2.getLastName() << "\t\t" << employee2.getSalary() << std::endl;

    return 0; /// function main terminated successfully
} /// end of function main

