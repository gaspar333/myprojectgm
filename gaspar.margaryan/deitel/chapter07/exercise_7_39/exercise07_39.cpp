/// Exercsie7_39.cpp
/// A program that simulates 2 rolling dices
/// which make 36000 /or given sized/ rolls
/// keep track on the numbers of dices and store the
/// values in a vector, then prints the result in
/// table form

#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <vector>
#include <cmath>
#include <cassert>
#include <unistd.h>

size_t rollDices();
void getAndStoreRate(const std::vector<int>& vectorInput, std::vector<float>& vectorStore, const size_t rolls, const size_t size);
void countDiceRolls(std::vector<int>& vector, const size_t rolls);
void possibleUniqueRollsCounter(std::vector<int>& vector);
void printTable(const std::vector<int>& vector, const std::vector<float>& rateX, const std::vector<float>& rateY, const size_t size);
bool isInRange(const float rateX, const float rateY);

int
main()
{
    const size_t ROLLS_QUANTITY = 11;
    std::vector<int> counterVector(ROLLS_QUANTITY, 0);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input the number of rolls to generate: ";
    }
    size_t rolls;
    std::cin >> rolls;
    countDiceRolls(counterVector, rolls);

    /// get and store program evaluated rolls rates in a vector
    std::vector<float> evaluatedRollsRate(ROLLS_QUANTITY, 0);
    getAndStoreRate(counterVector, evaluatedRollsRate, rolls, ROLLS_QUANTITY);

    /// get and store the count of all possible rolls made only once
    std::vector<int> fixedRollsCount(ROLLS_QUANTITY, 0);
    possibleUniqueRollsCounter(fixedRollsCount);

    /// get and store mathematically counted all possible rolls rates in a vector
    std::vector<float> fixedRollsRate(ROLLS_QUANTITY, 0);
    const size_t MAX_ROLL_TRIES = 36;
    getAndStoreRate(fixedRollsCount, fixedRollsRate, MAX_ROLL_TRIES, ROLLS_QUANTITY);

    printTable(counterVector, evaluatedRollsRate, fixedRollsRate, ROLLS_QUANTITY);

    return 0;
}

inline size_t
rollDices()
{
    static const size_t DICE_MIN = 1;
    static const size_t DICE_MAX = 6;
    const size_t dice1 = std::rand() % DICE_MAX + DICE_MIN;
    const size_t dice2 = std::rand() % DICE_MAX + DICE_MIN;
    return dice1 + dice2;
}

inline void
countDiceRolls(std::vector<int>& vector, const size_t rolls)
{
    static const int DICES_MIN = 2;
    for (size_t i = 0; i < rolls; ++i) {
        const size_t rollSum = rollDices();
        const size_t subscript = rollSum - DICES_MIN;
        ++vector[subscript];
    }
}

inline void
getAndStoreRate(const std::vector<int>& vectorInput, std::vector<float>& vectorStore, const size_t rolls, const size_t size)
{
    static const float PERCENT = 100;
    for (size_t i = 0; i < size; ++i) {
        const float count = static_cast<float>(vectorInput[i]);
        const float rate = count / static_cast<float>(rolls) * PERCENT;
        vectorStore[i] = rate;
    }
}

inline void
possibleUniqueRollsCounter(std::vector<int>& vector)
{
    static const size_t DICE_MAX = 6; /// the maximum value of a dice
    for (size_t i = 0; i < DICE_MAX; ++i) {
        for (size_t j = 0; j < DICE_MAX; ++j) {
            ++vector[i + j];
        }
    }
}

inline void
printTable(const std::vector<int>& vector, const std::vector<float>& rateX, const std::vector<float>& rateY, const size_t size)
{
    /// table header 
    std::cout << "Rolls sum";
    /// print dice sum from 2 to 12 as header
    static const size_t DICE_MIN = 2;
    for (size_t a = DICE_MIN; a < size + DICE_MIN; ++a) {
        std::cout << "\t" << a;
    }
    std::cout << std::endl;

    /// print the count of dice sum value by given number stored in vector element
    std::cout << "Roll count";
    for (size_t b = 0; b < size; ++b) {
        std::cout << "\t" << vector[b];
    }
    std::cout << std::endl;

    /// print the list of dices rolls rates the program evaluated
    std::cout << "Roll rates";
    for (size_t c = 0; c < size; ++c) {
        std::cout << std::fixed << "\t" << std::setprecision(3)
                  << rateX[c] << "%";
    }
    std::cout << std::endl;

    /// print the list of dices rolls rates mathematically calculated
    std::cout << "Fixed rates";
    for (size_t d = 0; d < size; ++d) {
        std::cout << std::fixed << "\t" << std::setprecision(3)
                  << rateY[d] << "%";
    }
    std::cout << std::endl;

    /// print the list of dices rolls rates mathematically calculated
    std::cout << "Deviations";
    for (size_t e = 0; e < size; ++e) {
        std::cout << std::fixed << std::setprecision(3) << "\t"
                  << (rateX[e] - rateY[e]) << "%";
    }
    std::cout << std::endl;
    
    /// print 'yes' if the rates are approximately equal otherwise print no
    std::cout << "Is in range?";
    for (size_t f = 0; f < size; ++f) {
        std::cout << "\t" << (isInRange(rateX[f], rateY[f]) ? "yes" : "no");
    }
    std::cout << std::endl;
}

inline bool
isInRange(const float rateX, const float rateY)
{
    static const float EPSILON = 0.1;
    const float deviation = rateX - rateY;
    return std::fabs(deviation) <= EPSILON;
}

