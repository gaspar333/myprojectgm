/// Exercsie7_40.cpp
/// Find minimum value in vector array
#include <iostream>
#include <vector>
#include <climits>
#include <unistd.h>

int getMinimum(const std::vector<int>& vector, const int start, const int end, int& minimal);
void getArrayElements(std::vector<int>& vector, const int size);

int
main()
{
    const int VECTOR_SIZE = 20;
    std::vector<int> vector(VECTOR_SIZE);
    getArrayElements(vector, VECTOR_SIZE);
    int minimal = INT_MAX;
    getMinimum(vector, 0, VECTOR_SIZE, minimal);
    std::cout << "The minimal value in array is " << minimal << std::endl;
    return 0;
}

inline void
getArrayElements(std::vector<int>& vector, const int size)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input " << size << " integers: ";
    }
    for (int i = 0; i < size; ++i) {
        std::cin >> vector[i];
    }
}

inline int
getMinimum(const std::vector<int>& vector, const int start, const int end, int& minimal)
{
    if (end == start) {
        return minimal;
    }
    if (minimal > vector[start]) {
        minimal = vector[start];
    }
    return getMinimum(vector, start + 1, end, minimal);
}

