/// Exercsie7_15.cpp
/// A program that gets 20 integers from 10-100 inclusive
/// after each input checks whether tha same value has been
/// inputed, and if no store it in an array. 
/// Display only the unique values using the smallest possible array.

#include <iostream>
#include <unistd.h>
#include <stdlib.h>

void getStorePrintArrayElements(int array[], const size_t size);
bool isDuplicate(const int array[], const int input, const size_t subsize);

int
main()
{
    /// for 20 inputes the minimal possible array size is 19
    const size_t ARRAY_SIZE = 19; 
    int array[ARRAY_SIZE] = {}; 

    /// get values from user store in the array as unique and print
    getStorePrintArrayElements(array, ARRAY_SIZE);
}

inline void
getStorePrintArrayElements(int array[], const size_t size)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 20 integer values from range 10 - 100: ";
    }
    size_t counter = 0; /// counter for array's subscript to keep track for conditional values
    const size_t SHIFTER = 1;
    for (size_t i = 0; i < size + SHIFTER; ++i) {
        int input;
        std::cin >> input;
        if (input < 10 || input > 100) {
            std::cerr << "Error 1: Wrong number input!" << std::endl;
            ::exit(1);
        }
        if (!isDuplicate(array, input, i)) {
            std::cout << input << " ";
            /// stop storing in the array if the last value is inputed
            if (counter < size) {
                array[counter++] = input; /// post-increment to get value from zero
            }
        }
    }
    std::cout << std::endl;
}

inline bool
isDuplicate(const int array[], const int input, const size_t subsize)
{
    for (size_t i = 0; i < subsize; ++i) {
        if (input == array[i]) {
            return true;
        }
    }
    return false;
}

