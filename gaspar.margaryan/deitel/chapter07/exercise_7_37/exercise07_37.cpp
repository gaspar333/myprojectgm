/// Exercsie7_37.cpp
/// Get minimal value from array using recursion function
#include <iostream>
#include <climits>
#include <unistd.h>

int getMinimum(const int array[], const int start, const int end, int& minimal);
void getArrayElements(int array[], const int size);

int
main()
{
    const int ARRAY_SIZE = 20;
    int array[ARRAY_SIZE] = {};
    getArrayElements(array, ARRAY_SIZE);
    int minimal = INT_MAX;
    getMinimum(array, 0, ARRAY_SIZE, minimal);
    std::cout << "The minimal value in array is " << minimal << std::endl;
    return 0;
}

inline void
getArrayElements(int array[], const int size)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input " << size << " integers: ";
    }
    for (int i = 0; i < size; ++i) {
        std::cin >> array[i];
    }
}

inline int
getMinimum(const int array[], const int start, const int end, int& minimal)
{
    if (end == start) {
        return minimal;
    }
    if (minimal > array[start]) {
        minimal = array[start];
    }
    return getMinimum(array, start + 1, end, minimal);
}

