/// Exercsie7_30.cpp
/// Bucket sort
#include <iostream>
#include <unistd.h>

size_t getMaximalDigitsCount(const size_t array[], const size_t size);
void inputNumbersFromUser(size_t array[], const size_t size);
void radixSort(size_t array[], const size_t size);
void distributionPass(const size_t array[], size_t bucket[][19], const size_t columns, const size_t exponent, size_t indexArray[]);
void gatheringPass(size_t array[], const size_t bucket[][19], const size_t rows, const size_t indexArray[]);
void printArray(const size_t array[], const size_t size);

int
main()
{
    const size_t CAPACITY = 20;
    size_t array[CAPACITY] = {};
    inputNumbersFromUser(array, CAPACITY);
    radixSort(array, CAPACITY);
    printArray(array, CAPACITY);
    return 0;
}

inline void
inputNumbersFromUser(size_t array[], const size_t size)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input " << size << " positive values: ";
    }
    for (size_t i = 0; i < size; ++i) {
        std::cin >> array[i];
    }
}

/// get the maximal digits count of the maximal value
inline size_t
getMaximalDigitsCount(const size_t array[], const size_t size)
{
    size_t maximalValue = 0;
    for (size_t i = 0; i < size; ++i) {
        if (array[i] >= maximalValue) {
            maximalValue = array[i];
        }
    }
    size_t digitCount = 0;
    while (maximalValue != 0) {
        maximalValue /= 10;
        ++digitCount;
    }
    return digitCount; 
}

inline void
radixSort(size_t array[], const size_t size)
{
    static const size_t RADIX_ROWS = 10;
    static const size_t RADIX_COLUMNS = 19; /// CAPACITY - 1
    const size_t maxNumberDigit = getMaximalDigitsCount(array, size);
    size_t exponent = 1;
    for (size_t i = 0; i < maxNumberDigit; ++i) {
        /// clear items of array bucket after every loop
        size_t indexArray[RADIX_ROWS] = {}; /// the size of RADIX_ROW
        size_t bucket[RADIX_ROWS][RADIX_COLUMNS] = {};
        distributionPass(array, bucket, RADIX_COLUMNS, exponent, indexArray);
        gatheringPass(array, bucket, RADIX_ROWS, indexArray);
        exponent *= 10;
    }
}

inline void
distributionPass(const size_t array[], size_t bucket[][19], const size_t columns, const size_t exponent, size_t indexArray[])
{
    for (size_t j = 0; j < columns; ++j) {
        const size_t digit = (array[j] / exponent) % 10;
        bucket[digit][indexArray[digit]++] = array[j]; /// postincrement for getting counter's value from 0
    }
}


inline void
gatheringPass(size_t array[], const size_t bucket[][19], const size_t rows, const size_t indexArray[])
{
    size_t counter = 0;
    for (size_t i = 0; i < rows; ++i) {
        for (size_t j = 0; j < indexArray[i]; ++j) {
            /// postincrement for getting counter's value from 0
            array[counter++] = bucket[i][j];
        }
    }
}

inline void
printArray(const size_t array[], const size_t size)
{
    for (size_t i = 0; i < size; ++i) {
        std::cout << array[i] << "\n";
    }
}

