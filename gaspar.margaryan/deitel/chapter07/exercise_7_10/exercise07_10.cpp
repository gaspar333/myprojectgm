/// Exercsie7_10.cpp
/// A program that counts the number of salespeople
/// who earned salary in given ranges
/// and print the range chart with that count on a proper range row

#include <iostream>
#include <unistd.h>
#include <stdlib.h>

void executeSalaryCounterByRange(int counterArray[], const size_t persons);
void printSalaryRangeCounterChart(const int counterArray[], const size_t arraySize);

int
main()
{
    const size_t NUMBER_OF_PERSONS = 15; /// assume we have 15 persons
    const size_t SIZE_OF_ARRAY = 9; /// the number of salary range rows
    int salaryRangeCounter[SIZE_OF_ARRAY] = {};
    executeSalaryCounterByRange(salaryRangeCounter, NUMBER_OF_PERSONS);
    printSalaryRangeCounterChart(salaryRangeCounter, SIZE_OF_ARRAY);
}

/// input gross sales from user and calculate the salary for each salesperson
/// check the weekly salary range and store it in array's appropriate element
inline void
executeSalaryCounterByRange(int counterArray[], const size_t persons)
{
    for (size_t i = 0; i < persons; ++i) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input gross sale value for salesperson No " << i + 1 << ": ";
        }
        int grossSale;
        std::cin >> grossSale;
        if (grossSale < 0) {
            std::cerr << "Error 1: Sales cannot be negative!" << std::endl;
            ::exit(1);
        }
        const int salary = 200 + grossSale * 0.09;
        int counter = salary / 100 - 2; /// generate subscript for counter array
        if (counter >= 8) {
            counter = 8; /// the maximal subscript of the array
        }
        /// increment the counter for appropriate range
        ++counterArray[counter];
    }
}

/// print the range chart and the number of salespeople who earned salary in appropriate range
inline void
printSalaryRangeCounterChart(const int counterArray[], const size_t arraySize)
{
    std::cout << "SALARY RANGE\t" << "SALESPEOPLE" << std::endl;
    for (size_t i = 0; i < arraySize; ++i) {
        const int indentedValue = (i + 2) * 100; 
        if (indentedValue >= 1000) {
            std::cout << "$" << indentedValue << " and over\t" << counterArray[i] << "\n";
            break;
        }
        std::cout << "$" << indentedValue << " - " << "$" << indentedValue + 99 << "\t" << counterArray[i] << "\n";
    }
}

