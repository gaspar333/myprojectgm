/// Exercise07_19.cpp
/// Craps simulation.
/// Analize played 1000 games of craps
/// how many wins or loses on each roll starting from 1-st roll to 20-th roll and more
/// what are the rates of winning or losing and what improving we have after each roll
/// what is the average length of the game
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cassert>
#include <unistd.h>

/// enumeration with constants that represent the game status 
enum Status {CONTINUE, WON, LOST}; /// all caps in constants

size_t rollDice();
bool crapsGame(int arrayWon[], int arrayLost[], const size_t size);
bool chanceToWin (const float rate1, const float rate2);
void printAnalyzeTable(const int arrayWon[], const int arrayLost[], const float winRates[], const float loseRates[], const size_t size);
void printGameAverage(const int arrayWon[], const int arrayLost[], const size_t size, const size_t rolls);
void getAndStoreRate(const int input[], float output[], const size_t size, const size_t rolls);

int
main()
{
    static const size_t ARRAY_SIZE = 21; /// the last subscript for storing the game results higher than 20 rolls
    static int arrayWon[ARRAY_SIZE] = {};
    static int arrayLost[ARRAY_SIZE] = {};
    static const int GAME_TRIES = 1000;
    /// variables for control checking win and lose arrays elements
    int winCount = 0;
    int loseCount = 0;

    /// generate craps game parallelly counting the number of wins and loses
    for (int i = 0; i < GAME_TRIES; ++i) {
        crapsGame(arrayWon, arrayLost, ARRAY_SIZE) ? ++winCount : ++loseCount;
    }

    /// count and store win or lose rates in appropriate arrays by number of rolls
    static float winRates[ARRAY_SIZE] = {};
    getAndStoreRate(arrayWon, winRates, ARRAY_SIZE, GAME_TRIES);
    static float loseRates[ARRAY_SIZE] = {};
    getAndStoreRate(arrayLost, loseRates, ARRAY_SIZE, GAME_TRIES);

    /// print game craps analyzer table
    printAnalyzeTable(arrayWon, arrayLost, winRates, loseRates, ARRAY_SIZE);

    /// print counts of wins and loses
    std::cout << "\nCount of wins:\t" << winCount << std::endl;
    std::cout << "Count of loses:\t" << loseCount << std::endl;

    /// average rates of wins and loses
    const float rateOfWins = static_cast<float>(winCount) / static_cast<float>(GAME_TRIES);
    const float rateOfLoses = static_cast<float>(loseCount) / static_cast<float>(GAME_TRIES);
    /// print the average chance of wins and loses
    std::cout << "\nAverage win chance:\t " << std::setprecision(2) << rateOfWins << std::endl;
    std::cout << "Average lose chance:\t " << std::setprecision(2) << rateOfLoses << std::endl;

    /// calculate and print the average length of the game
    printGameAverage(arrayWon, arrayLost, ARRAY_SIZE, GAME_TRIES);

    return 0; /// indicates successful termination
} /// end main


inline bool
crapsGame(int arrayWon[], int arrayLost[], const size_t size)
{
    size_t myPoint = 0; /// point if no win or loss on first roll
    Status gameStatus; /// can contain CONTINUE, WON or LOST
    size_t rollCounter = 0;
    const size_t sumOfDice = rollDice(); /// first roll of the dice
    /// determine game status and point (if needed) based on first roll
    switch (sumOfDice) {
    case 7: /// win with 7 on first roll
    case 11: /// win with 11 on first roll
        gameStatus = WON;
        ++arrayWon[rollCounter];
        break;
    case 2: /// lose with 2 on first roll
    case 3: /// lose with 3 on first roll
    case 12: /// lose with 12 on first roll
        gameStatus = LOST;
        ++arrayLost[rollCounter];
        break;
    default: /// did not win or lose, so remember point
        gameStatus = CONTINUE; /// game is not over
        myPoint = sumOfDice; /// remember the point
        break; /// optional at end of switch
    } /// end switch
    /// while game is not complete
    while (CONTINUE == gameStatus) { /// not WON or LOST
        const size_t nextSumOfDice = rollDice(); // roll dice again
        static const size_t SHIFTER = 1;
        ++rollCounter;
        if (rollCounter >= size) {
            rollCounter = size - SHIFTER;
        } /// store after 20-th rolls results in the last array element
        /// determine game status
        if (nextSumOfDice == myPoint) { /// win by making point
            gameStatus = WON;
            ++arrayWon[rollCounter];
        }
        if (7 == nextSumOfDice) { /// lose by rolling 7 before point
            gameStatus = LOST;
            ++arrayLost[rollCounter];
        }
    } /// end while
    /// display won or lost message
    return WON == gameStatus;
}

inline bool
chanceToWin (const float rate1, const float rate2)
{
    return rate1 >= rate2;
}

/// roll dice, calculate sum and display results
inline size_t
rollDice()
{
    /// pick random die values
    static const size_t DICE_MIN = 1;
    static const size_t DICE_MAX = 6;
    const size_t dice1 = DICE_MIN + std::rand() % DICE_MAX; /// first dice roll
    const size_t dice2 = DICE_MIN + std::rand() % DICE_MAX; /// second dice roll
    const size_t sum = dice1 + dice2; // compute sum of dice values
    assert(sum >= 2 && sum <= 12);
    /// display results of this roll
    return sum; /// end function rollDice
} /// end function rollDice

inline void
printAnalyzeTable(const int arrayWon[], const int arrayLost[], const float winRates[], const float loseRates[], const size_t size)
{
    static const size_t SHIFTER = 1;
    /// roll numbers header
    std::cout << "Roll No_\t" << "Games won\t" << "Games lost\t" << "Wins rates\t" 
              << "Lose rates\t" << "Deviation\t" << "Chance to win? (improvement)\n";
    /// lost games chart
    for (size_t i = 0; i < size; ++i) {
        const float deviance = winRates[i] - loseRates[i];
        if ((i + SHIFTER) < size) {
            std::cout << i + SHIFTER << "\t\t";
        } else {
            std::cout << "21 and more" << "\t";
        }
        std::cout << arrayWon[i] << "\t\t" << arrayLost[i] << "\t\t" 
                  <<  winRates[i] << "\t\t" << loseRates[i] << "\t\t" << deviance
                  << "\t\t" << (chanceToWin(winRates[i], loseRates[i]) ? "Yes" : "No") << "\n";
    }
}

inline void
printGameAverage(const int arrayWon[], const int arrayLost[], const size_t size, const size_t rolls)
{
    size_t totalGames = 0;
    static const size_t SHIFTER = 1;
        for (size_t i = 0; i < size; ++i) {
            totalGames += (arrayWon[i] + arrayLost[i]) * (i + SHIFTER);
    }
    const float average = static_cast<float>(totalGames) / static_cast<float>(rolls);
    std::cout << "Approximate average of game 'craps':\t" 
              << std::fixed << std::setprecision(2) << average;
}


inline void
getAndStoreRate(const int input[], float output[], const size_t size, const size_t rolls)
{
    for (size_t i = 0; i < size; ++i) {
        const float count = static_cast<float>(input[i]);
        const float rate = count / static_cast<float>(rolls);
        output[i] = rate;
    }
}

