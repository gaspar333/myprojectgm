/// Exercsie7_20.cpp
/// Airline reservations system
#include <iostream>
#include <cstdlib>
#include <string>
#include <cassert>
#include <unistd.h>

enum Classes {ALL_CLASSES, FIRST_CLASS, ECONOMY_CLASS, CLASS_COUNT};
enum Seats {FIRST_SEAT, LAST_SEAT};
const bool isInteractive = ::isatty(STDIN_FILENO);

void executeReservation(bool planeSeats[], const size_t seatCount);
void classIsChanged(const char select);
void printMenu();
void printAvailableSeats(const bool planeSeats[], const size_t firstSeat, const size_t lastSeat, const size_t seatClass);
void printBoardingPass(const size_t seatClass, const size_t seatNumber);
void printChangeClassMenu(const size_t seatClass);
void printError(const size_t errorNumber, const std::string& errorMessage);
size_t getSeatClass();
std::string getClassName(const size_t seatClass);
size_t getOppositeSeatClass(const size_t seatClass);
size_t getSeatNumber(const size_t firstSeat, const size_t lastSeat);
char getSelectionFromUser();
bool reserveSeat(bool planeSeats[], const size_t seatNumber);
bool seatsAreReserved(const bool planeSeats[], const size_t firstSeat, const size_t lastSeat);

int
main()
{
    const size_t SEAT_CAPACITY = 10;
    bool planeSeats[SEAT_CAPACITY] = {};
    executeReservation(planeSeats, SEAT_CAPACITY);
    std::cout << "Next flight leaves in 3 hours." << std::endl;

    return 0;
}

inline void
executeReservation(bool planeSeats[], const size_t seatCount)
{
    static const size_t SEAT_UNITS = 2;
    const size_t halfSeats = seatCount / 2;
    static const size_t seatRange[][SEAT_UNITS] = {
        { FIRST_SEAT, seatCount }, /// the index range of all seats
        { FIRST_SEAT, halfSeats }, /// the index range of first class seats 
        { halfSeats,  seatCount }  /// the index range of econom seats
    };  

    while (!seatsAreReserved(planeSeats, seatRange[ALL_CLASSES][FIRST_SEAT], seatRange[ALL_CLASSES][LAST_SEAT])) {
        printMenu(); /// prints menu after each choice
        const size_t seatClass = getSeatClass();
        assert (FIRST_CLASS == seatClass || ECONOMY_CLASS == seatClass); /// assertion for valid seat class (aka menu number)
        /// check whether exact class seats are all reserved
        if (!seatsAreReserved(planeSeats, seatRange[seatClass][FIRST_SEAT], seatRange[seatClass][LAST_SEAT])) {
            /// prints available seat numbers for exact class
            printAvailableSeats(planeSeats, seatRange[seatClass][FIRST_SEAT], seatRange[seatClass][LAST_SEAT], seatClass);
            const size_t seatNumber = getSeatNumber(seatRange[seatClass][FIRST_SEAT], seatRange[seatClass][LAST_SEAT]);
            /// functon that checks and reserves seats
            if (reserveSeat(planeSeats, seatNumber)) {
                printBoardingPass(seatClass, seatNumber);
            }
        } else {
            /// prompts the user to change the class where all seats are reserved
            printChangeClassMenu(seatClass);
            classIsChanged(getSelectionFromUser());
        }
    }
}

inline void
classIsChanged(const char select)
{
    switch (select) {
    case 'y': case 'Y':                                                             return;
    case 'n': case 'N': std::cout << "Next flight leaves in 3 hours." << std::endl; return;
    default: printError(2, "Wrong selection input!");
    }
}

inline void
printMenu()
{
    std::cout << "Please type 1 for \"First Class\":\n"
              << "Please type 2 for \"Economy\":" << std::endl;
}

inline void
printAvailableSeats(const bool planeSeats[], const size_t firstSeat, const size_t lastSeat, const size_t seatClass)
{
    static const size_t SHIFTER = 1; /// shiftr to get exact seat number for user menu
    std::cout << getClassName(seatClass) << " available seats:\n";
    for (size_t i = firstSeat; i < lastSeat; ++i) {
        if (!planeSeats[i]) {
            std::cout << "\tSeat No " << i + SHIFTER << "\n";
        }
    }
}

inline void
printBoardingPass(const size_t seatClass, const size_t seatNumber)
{
    std::cout << "\n\"SKY AIRWAYS\" LLC\n" << "PASSENGER \t" << "_____________________________\n"
              << "\t\t\tname, surname\n" << "CLASS\t\t"
              << getClassName(seatClass) << "\nSEAT NUMBER\t\t" << "___"
              << seatNumber << "___" << "\nDESTINATION\t" << "_____________________________\n"
              << "DEPARTUERE TIME\t" << "_____\t" << "ARRIVAL TIME\t" << "_____\n"
              << "HAVE A GOOD AND SAFE FLIGHT!!!\n" << std::endl;
}

inline void
printChangeClassMenu(const size_t seatClass)
{
    /// If all seats of given class are reserved print info and ask about reservation in other class
    std::cout << "All " << getClassName(seatClass)
              << " seats are reserved.\n" << "Would you like to get a place in " 
              << getClassName(getOppositeSeatClass(seatClass)) << "? y/n: \n";
}

inline void
printError(const size_t errorNumber, const std::string& errorMessage)
{
    std::cerr << "Error " << errorNumber << ": " << errorMessage << std::endl;
    ::exit(errorNumber);
}

inline size_t
getSeatClass()
{
    size_t seatClass;
    std::cin >> seatClass;
    if (FIRST_CLASS != seatClass && ECONOMY_CLASS != seatClass) {
        printError(1, "Wrong menu number!");
    }
    return seatClass;
}

inline std::string
getClassName(const size_t seatClass)
{
    assert(FIRST_CLASS == seatClass || ECONOMY_CLASS == seatClass);
    if (FIRST_CLASS == seatClass) {
        return "\"First class\"";
    }
    return "\"Econom Class\"";
}

inline size_t
getOppositeSeatClass(const size_t seatClass)
{
    assert(FIRST_CLASS == seatClass || ECONOMY_CLASS == seatClass);
    return (FIRST_CLASS + ECONOMY_CLASS - seatClass);
}

inline size_t
getSeatNumber(const size_t firstSeat, const size_t lastSeat)
{
    if (::isInteractive) {
        std::cout << "Input seat number to reserve: ";
    }
    size_t seatNumber;
    std::cin >> seatNumber;
    /// condition to check the valid seat number input in the range of exact class
    if (seatNumber < firstSeat || seatNumber > lastSeat) {
        printError(3, "Wrong seat number!");
    }
    return seatNumber;
}

inline char
getSelectionFromUser()
{
    char select;
    std::cin >> select;
    return select;
}

inline bool
reserveSeat(bool planeSeats[], const size_t seatNumber)
{
    static const size_t SHIFTER = 1; /// subscript or value shifter
    static const bool IS_RESERVED = true;
    /// check whether the current seat is reserved. If yes - print info and skip, if not reserve
    if (planeSeats[seatNumber - SHIFTER]) {
        if (::isInteractive) {
            std::cout << "The seat is already reserved!" << std::endl;
            return false;
        }
    } else {
        /// reserve an empty seat
        planeSeats[seatNumber - SHIFTER] = IS_RESERVED;
        /// print reserved seat's boarding pass
    }
    return true;
}

inline bool
seatsAreReserved(const bool planeSeats[], const size_t firstSeat, const size_t lastSeat)
{
    for (size_t i = firstSeat; i < lastSeat; ++i) {
        if (!planeSeats[i]) {
            return false;
        }
    }
    return true;
}

