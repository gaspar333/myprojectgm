/// Exercsie7_22.cpp
/// A program that prints the tolal monthly sales in table chart
/// counting the totals for each salesman and each product type
#include <iostream>
#include <iomanip>
#include <unistd.h>

enum Sales {SALESPEOPLE_COUNT = 4, PRODUCTS_COUNT};
void printTable(const size_t array[][PRODUCTS_COUNT], const size_t rowSize, const size_t colSize);

int
main()
{
    size_t sales[SALESPEOPLE_COUNT][PRODUCTS_COUNT] = {};
    for (size_t i = 0; i < SALESPEOPLE_COUNT; ++i) {
        for (size_t j = 0; j < PRODUCTS_COUNT; ++j) {
            if (::isatty(STDIN_FILENO)) {
                std::cout << "\nInput monthly sales for Salesman No" 
                          << i << " Product No" << j << ": ";
            }
            std::cin >> sales[i][j];
        }
    }
    printTable(sales, SALESPEOPLE_COUNT, PRODUCTS_COUNT);

    return 0;
}

inline void
printTable(const size_t array[][PRODUCTS_COUNT], const size_t rowSize, const size_t colSize)
{
    static const size_t SHIFTER = 1;
    /// print header for product numbers columns and total value for salespeople
    std::cout << "\t";
    for (size_t a = 0; a < colSize; ++a) {
        std::cout << "\tProduct No" << a + SHIFTER;
    }
    std::cout << "\tTOTAL VALUE\n";
    std::cout << std::setw(110) << "BY SALESPEOPLE" << std::endl;
    /// prints the table of monthly sales by salespeople and products
    /// counts and print total value by salespeople
    for (size_t i = 0; i < rowSize; ++ i) {
        std::cout << "Salesman No" << i + SHIFTER;
        int totalBySalesman = 0; /// variable for total value calculation by salespeople
        for (size_t j = 0; j < colSize; ++j) {
            std::cout << "\t" << std::setw(9) <<  array[i][j] << " $";
            totalBySalesman += array[i][j];
        }
        std::cout << "\t" << std::setw(5) << totalBySalesman << " $\n";
    }

    /// prints total values by products selled in the last row and the total summary
    std::cout << "TOTAL VALUE \nBY PRODUCT";
    static size_t totalSummary = 0; /// variable for total summary value calculation
    for (size_t e = 0; e < colSize; ++e) {
        int totalByProduct = 0; /// variable for total value calculation by product
        for (size_t f = 0; f < rowSize; ++f) {
            totalByProduct += array[f][e];
        }
        std::cout << "\t" <<  std::setw(9) << totalByProduct << " $";
        totalSummary += totalByProduct;
    }
    std::cout << "\t" << std::setw(5) << totalSummary << " $" << std::endl;
}

