/// Exercsie7_29.cpp
/// The Sieve of Eratosthenes
/// get prime numbers using arrays subscripts
#include <iostream>

void setAllElementsTrue(bool array[], const size_t size);
void determinePrimeNumbers(bool array[], const size_t size);
void printPrimeNumbers(const bool array[],const size_t size);

int
main()
{
    const size_t CAPACITY = 1000;
    bool array[CAPACITY] = {};
    setAllElementsTrue(array, CAPACITY);
    determinePrimeNumbers(array, CAPACITY);
    printPrimeNumbers(array, CAPACITY);
    return 0;
}

inline void
setAllElementsTrue(bool array[], const size_t size)
{
    for (size_t i = 2; i < size; ++i) {
        array[i] = true;
    }
}

inline void 
printPrimeNumbers(const bool array[], const size_t size)
{
    for (size_t i = 0; i < size; ++i) {
        if (array[i]) {
            std::cout << i << std::endl;
        }
    }
}

inline void
determinePrimeNumbers(bool array[], const size_t size)
{
    for (size_t i = 2; i < size; ++i) {
        if (array[i]) {
            for (size_t j = 2 * i; j < size; j += i) {
                array[j] = false;
            }
        }
    }
}

