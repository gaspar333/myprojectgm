/// Exercsie7_23.cpp
/// Turtle graphic simulator
#include <iostream>
#include <cstdlib>
#include <limits>
#include <cassert>
#include <unistd.h>

const size_t BOARD_SIZE = 20;
const size_t SHIFTER = 1;
const bool isInteractive = ::isatty(STDIN_FILENO);
const bool DOWN_PEN = true;

enum Command {PEN_UP = 1, PEN_DOWN, TURN_RIGHT, TURN_LEFT, MOVE_FORWARD, PRINT_PATTERN, END};
enum Direction {EAST, SOUTH, WEST, NORTH};

void executeTurtleGrapics();
void printCommandMenu();
void turnRight(size_t& directionNumber);
void turnLeft(size_t& directionNumber);
void moveForward(bool board[][BOARD_SIZE], size_t& x, size_t& y, const size_t directionNumber, const bool penDown);
void goEast(bool board[][BOARD_SIZE], size_t& x, size_t& y, size_t stepLength, const bool penDown);
void goSouth(bool board[][BOARD_SIZE], size_t& x, size_t& y, size_t stepLength, const bool penDown);
void goWest(bool board[][BOARD_SIZE], size_t& x, size_t& y, size_t stepLength, const bool penDown);
void goNorth(bool board[][BOARD_SIZE], size_t& x, size_t& y, size_t stepLength, const bool penDown);
void drawPattern(const bool board[][BOARD_SIZE]);
size_t getStepLength();

Command getCommand();
Direction getDirection(const size_t directionNumber);

int
main()
{
    executeTurtleGrapics();
    return 0;
}

inline void
executeTurtleGrapics()
{
    bool board[BOARD_SIZE][BOARD_SIZE] = {};
    size_t xCoordinate = 0;
    size_t yCoordinate = 0;
    size_t directionNumber = 0; /// for generating values 0 - 3 for 4 directions, 0 represents direction EAST
    bool penDown = !DOWN_PEN; /// begin function with pen up condition
    while (true) {
        const Command command = getCommand();
        switch (command) {
        case PEN_UP:        penDown = !DOWN_PEN;                                                    break;
        case PEN_DOWN:      penDown = DOWN_PEN;                                                     break;
        case TURN_RIGHT:    turnRight(directionNumber);                                             break;
        case TURN_LEFT:     turnLeft(directionNumber);                                              break;
        case MOVE_FORWARD:  moveForward(board, xCoordinate, yCoordinate, directionNumber, penDown); break;
        case PRINT_PATTERN: drawPattern(board);                                                     break;
        case END:           penDown = !DOWN_PEN;                                                    break;
        default:            assert(0);                                                              break;
        }
        if (END == command) {
            return; /// get out from loop when end menu is entered
        }
    }
}

inline void 
printCommandMenu()
{
    if (::isInteractive) {
        static const size_t COMMAND_COUNT = 7;
        static const std::string commandMenu[COMMAND_COUNT] = {"Pen up", "Pen down", "Turn right", "Turn left",
                                                               "Move forward", "Print pattern", "End"};
        std::cout << "Command Menu\n";
        for (size_t i = 0; i < COMMAND_COUNT; ++i) {
            std::cout << "\t" << i + SHIFTER << " - " << commandMenu[i] << "\n";
        }
        std::cout << std::endl;
    }
}

inline void
turnRight(size_t& directionNumber)
{
    ++directionNumber;
}

inline void
turnLeft(size_t& directionNumber)
{
    --directionNumber;
}

inline void
moveForward(bool board[][BOARD_SIZE], size_t& x, size_t& y, const size_t directionNumber, const bool penDown)
{
    const size_t stepLength = getStepLength() - SHIFTER; /// for off-by-one issue decrease step length by 1
    const Direction direction = getDirection(directionNumber);
    switch (direction) {
    case EAST:  goEast(board, x, y, stepLength, penDown);  break;
    case SOUTH: goSouth(board, x, y, stepLength, penDown); break;
    case WEST:  goWest(board, x, y, stepLength, penDown);  break;
    case NORTH: goNorth(board, x, y, stepLength, penDown); break;
    default: assert(0); break;
    }
}

inline void 
goEast(bool board[][BOARD_SIZE], size_t& x, size_t& y, size_t stepLength, const bool penDown)
{
    if (y + stepLength >= BOARD_SIZE) {
        stepLength = BOARD_SIZE - y - SHIFTER;
    }
    if (penDown) {
        for (size_t i = 0; i <= stepLength; ++i) {
            board[x][y + i] = true;
        }
    }
    y += stepLength;
}

inline void
goSouth(bool board[][BOARD_SIZE], size_t& x, size_t& y, size_t stepLength, const bool penDown)
{
    if (x + stepLength >= BOARD_SIZE) {
        stepLength = BOARD_SIZE - x - SHIFTER;
    }
    if (penDown) {
        for (size_t i = 0; i <= stepLength; ++i) {
            board[x + i][y] = true;
        }
    }
    x += stepLength;
}

inline void
goWest(bool board[][BOARD_SIZE], size_t& x, size_t& y, size_t stepLength, const bool penDown)
{
    if (y - stepLength > BOARD_SIZE) {
        stepLength = y;
    }
    if (penDown) {
        for (size_t i = 0; i <= stepLength; ++i) {
            board[x][y - i] = true;
        }
    }
    y -= stepLength;
}

inline void
goNorth(bool board[][BOARD_SIZE], size_t& x, size_t& y, size_t stepLength, const bool penDown)
{
    if (x - stepLength > BOARD_SIZE) {
        stepLength = x;
    }
    if (penDown) {
        for (size_t i = 0; i <= stepLength; ++i) {
            board[x - i][y] = true;
        }
    }
    x -= stepLength;
}

inline void
drawPattern(const bool board[][BOARD_SIZE])
{
    for (size_t i = 0; i < BOARD_SIZE; ++i) {
        for (size_t j = 0; j < BOARD_SIZE; ++j) {
            std::cout << (board[i][j] ? 'o' : ' ') << ' ';
        }
        std::cout << std::endl;
    }
}

inline size_t
getStepLength()
{
    if (::isInteractive) {
        std::cout << "Input move forward length: ";
    }
    size_t stepLength;
    std::cin >> stepLength;
    return stepLength;
}

inline Command
getCommand()
{
    if (::isInteractive) {
        printCommandMenu();
        std::cout << "Input command number: ";
    }
    size_t commandNumber;
    std::cin >> commandNumber;
    return static_cast<Command>(commandNumber);
}

inline Direction
getDirection(const size_t number)
{
    return static_cast<Direction>(number % 4);
}

