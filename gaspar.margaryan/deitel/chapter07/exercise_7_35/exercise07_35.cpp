/// Exercsie7_35.cpp
/// Print array using recursive function with array, starting and ending
/// arguments
#include <iostream>
#include <unistd.h>

void printArray(const int array[], const int start, const int end);
void getArrayElements(int array[], const int size);

int
main()
{
    const int ARRAY_SIZE = 20;
    int array[ARRAY_SIZE] = {};
    getArrayElements(array, ARRAY_SIZE);
    printArray(array, 0, ARRAY_SIZE);

    return 0;
}

inline void
getArrayElements(int array[], const int size)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input " << size << " integers: ";
    }
    for (int i = 0; i < size; ++i) {
        std::cin >> array[i];
    }
}

inline void
printArray(const int array[], const int start, const int end)
{
    if (end == start) {
        return;
    }
    std::cout << array[start] << std::endl;
    printArray(array, start + 1, end);
}

