/// Exercsie7_32.cpp
/// Palindrome text checking program
/// ignoring all white spaces, punctuationas
#include <iostream>
#include <unistd.h>
#include <string>

bool isPalindrome(std::string& text, const size_t start, const size_t end);

int
main()
{
    std::string text;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input text :";
    }
    std::getline(std::cin, text);
    const size_t end = text.size() - 1;  /// the subscript size of string
    std::cout << std::boolalpha << isPalindrome(text, 0, end) << std::endl;
    return 0;
}

bool isPalindrome(std::string& text, size_t start, size_t end)
{
    /// increment start index if there is no letter
    while (((text[start] < 'a' || text[start] > 'z') && (text[start] < 'A' || text[start] > 'Z')) && start < end) {
        ++start;
    }
    /// decrement end index if there is no letter
    while (((text[end] < 'a' || text[end] > 'z') && (text[end] < 'A' || text[end] > 'Z')) && end > start) {
        --end;
    }
    char starting = text[start];
    char ending = text[end];
    /// make uppecase letters lowercase
    if (starting >= 'A' && starting <= 'Z') {
        starting += ' '; /// aka plus 32
    }
    if (ending >= 'A' && ending <= 'Z') {
        ending += ' '; /// aka plus 32
    }

    if (starting != ending) {
        return false;
    }
    /// base case
    if (start >= end) {
        return starting == ending;
    }
    /// recursive call till the half size of the string
    return isPalindrome(text, start + 1, end - 1);
}

