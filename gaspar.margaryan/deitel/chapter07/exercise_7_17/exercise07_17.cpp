/// Exercsie7_16.cpp
/// A program that simulates 2 rolling dices
/// which make 36000 /or given sized/ rolls
/// keep track on the numbers of dices and store the
/// values in an array, then prints the result in
/// table form

#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>
#include <cassert>
#include <unistd.h>

size_t rollDices();
void getAndStoreRate(const int arrayInput[], float arrayStore[], const size_t rolls, const size_t size);
void countDiceRolls(int array[], const size_t rolls);
void possibleUniqueRollsCounter(int array[]);
void printTable(const int array[], const float rateX[], const float rateY[], const size_t size);
bool isInRange(const float rateX, const float rateY);

int
main()
{
    const size_t ARRAY_SIZE = 11;
    int array[ARRAY_SIZE] = {};
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input the number of rolls to generate: ";
    }
    size_t rolls;
    std::cin >> rolls;
    countDiceRolls(array, rolls);

    /// get and store program evaluated rolls rates in an array
    float evaluatedRollsRate[ARRAY_SIZE] = {};
    getAndStoreRate(array, evaluatedRollsRate, rolls, ARRAY_SIZE);

    /// get and store the count of all possible rolls made only once
    int fixedRollsCount[ARRAY_SIZE] = {};
    possibleUniqueRollsCounter(fixedRollsCount);

    /// get and store mathematically counted all possible rolls rates in an array
    float fixedRollsRate[ARRAY_SIZE] = {};
    const size_t MAX_ROLL_TRIES = 36;
    getAndStoreRate(fixedRollsCount, fixedRollsRate, MAX_ROLL_TRIES, ARRAY_SIZE);

    printTable(array, evaluatedRollsRate, fixedRollsRate, ARRAY_SIZE);

    return 0;
}

inline size_t
rollDices()
{
    static const size_t DICE_MIN = 1;
    static const size_t DICE_MAX = 6;
    const size_t dice1 = std::rand() % DICE_MAX + DICE_MIN;
    const size_t dice2 = std::rand() % DICE_MAX + DICE_MIN;
    return dice1 + dice2;
}

inline void
countDiceRolls(int array[], const size_t rolls)
{
    static const int DICES_MIN = 2;
    for (size_t i = 0; i < rolls; ++i) {
        const size_t rollSum = rollDices();
        const size_t subscript = rollSum - DICES_MIN;
        ++array[subscript];
    }
}

inline void
getAndStoreRate(const int arrayInput[], float arrayStore[], const size_t rolls, const size_t size)
{
    static const float PERCENT = 100;
    for (size_t i = 0; i < size; ++i) {
        const float count = static_cast<float>(arrayInput[i]);
        const float rate = count / static_cast<float>(rolls) * PERCENT;
        arrayStore[i] = rate;
    }
}

inline void
possibleUniqueRollsCounter(int array[])
{
    static const size_t DICE_MAX = 6; /// the maximum value of a dice
    for (size_t i = 0; i < DICE_MAX; ++i) {
        for (size_t j = 0; j < DICE_MAX; ++j) {
            ++array[i + j];
        }
    }
}

inline void
printTable(const int array[], const float rateX[], const float rateY[], const size_t size)
{
    /// table header 
    std::cout << "Rolls sum";
    /// print dice sum from 2 to 12 as header
    static const size_t DICE_MIN = 2;
    for (size_t a = DICE_MIN; a < size + DICE_MIN; ++a) {
        std::cout << "\t" << a;
    }
    std::cout << std::endl;

    /// print the count of dice sum value by given number stored in array element
    std::cout << "Roll count";
    for (size_t b = 0; b < size; ++b) {
        std::cout << "\t" << array[b];
    }
    std::cout << std::endl;

    /// print the list of dices rolls rates the program evaluated
    std::cout << "Roll rates";
    for (size_t c = 0; c < size; ++c) {
        std::cout << std::fixed << "\t" << std::setprecision(3)
                  << rateX[c] << "%";
    }
    std::cout << std::endl;

    /// print the list of dices rolls rates mathematically calculated
    std::cout << "Fixed rates";
    for (size_t d = 0; d < size; ++d) {
        std::cout << std::fixed << "\t" << std::setprecision(3)
                  << rateY[d] << "%";
    }
    std::cout << std::endl;

    /// print the list of dices rolls rates mathematically calculated
    std::cout << "Deviations";
    for (size_t e = 0; e < size; ++e) {
        std::cout << std::fixed << std::setprecision(3) << "\t"
                  << (rateX[e] - rateY[e]) << "%";
    }
    std::cout << std::endl;
    
    /// print 'yes' if the rates are approximately equal otherwise print no
    std::cout << "Is in range?";
    for (size_t f = 0; f < size; ++f) {
        std::cout << "\t" << (isInRange(rateX[f], rateY[f]) ? "yes" : "no");
    }
    std::cout << std::endl;
}

inline bool
isInRange(const float rateX, const float rateY)
{
    static const float EPSILON = 0.1;
    const float deviation = rateX - rateY;
    return std::fabs(deviation) <= EPSILON;
}

