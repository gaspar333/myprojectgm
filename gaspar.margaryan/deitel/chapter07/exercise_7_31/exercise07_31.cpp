/// Exercsie7_31.cpp
/// Recursive selection sort
#include <iostream>
#include <unistd.h>

void inputNumbersFromUser(int array[], const size_t size);
void printArray(const int array[], const size_t size);
void executeSelectionSort(int array[], const size_t firstIndex, const size_t size);
size_t getMinimalValueIndex(const int array[], const size_t firstIndex, const size_t size);

int
main()
{
    const size_t CAPACITY = 20;
    int array[CAPACITY] = {};
    inputNumbersFromUser(array, CAPACITY);
    executeSelectionSort(array, 0, CAPACITY);
    printArray(array, CAPACITY);

    return 0;
}

inline void
inputNumbersFromUser(int array[], const size_t size)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input " << size << " integer numbers: ";
    }
    for (size_t i = 0; i < size; ++i) {
        std::cin >> array[i];
    }
}

inline void
printArray(const int array[], const size_t size)
{
    for (size_t i = 0; i < size; ++i) {
        std::cout << array[i] << std::endl;
    }
}

inline void
executeSelectionSort(int array[], const size_t firstIndex, const size_t size)
{
    if (size == firstIndex) {
        return;
    }
    const int minValueIndex = getMinimalValueIndex(array, firstIndex, size);
    std::swap(array[firstIndex], array[minValueIndex]);
    executeSelectionSort(array, firstIndex + 1, size);
}

inline size_t 
getMinimalValueIndex(const int array[], const size_t firstIndex, const size_t size)
{
    static const size_t SHIFTER = 1;
    size_t index = size - SHIFTER;
    for (size_t i = firstIndex; i < size; ++i) {
        if (array[i] < array[index]) {
            index = i;
        }
    }
    return index;
}

