/// Exercsie7_12.cpp
/// A program that sorts an 10 element integer array
/// by using optimized bubble sort algorithm

#include <iostream>
#include <algorithm>
#include <unistd.h>

void getArrayElementsFromUser(int array[], const size_t arraySize);
void bubbleSortArray(int array[], const size_t arraySize);
void printArrayElements(const int array[], const size_t arraySize);

int
main()
{
    const size_t SIZE_OF_ARRAY = 10;
    int arrayOfIntegers[SIZE_OF_ARRAY] = {};

    getArrayElementsFromUser(arrayOfIntegers, SIZE_OF_ARRAY);
    bubbleSortArray(arrayOfIntegers, SIZE_OF_ARRAY);
    printArrayElements(arrayOfIntegers, SIZE_OF_ARRAY);
}

inline void
getArrayElementsFromUser(int array[], const size_t arraySize)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 10 integer values for the array to store in: ";
    }
    for (size_t i = 0; i < arraySize; ++i) {
        std::cin >> array[i];
    }
}

inline void
bubbleSortArray(int array[], const size_t arraySize)
{
    for (size_t i = 0; i < arraySize; ++i) {
        for (size_t j = 1; j < arraySize - i; ++j) {
            if (array[j - 1] > array[j]) {
                std::swap(array[j - 1], array[j]);
            }
        }
    }
}

inline void
printArrayElements(const int array[], const size_t arraySize)
{
    for (size_t i = 0; i < arraySize; ++i) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
}

