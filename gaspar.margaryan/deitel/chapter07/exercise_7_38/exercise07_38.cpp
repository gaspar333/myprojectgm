/// Exercsie7_38.cpp
/// A program that counts the number of salespeople
/// who earned salary in given ranges
/// and print the range chart with that count on a proper range row
/// using vectors

#include <iostream>
#include <vector>
#include <unistd.h>
#include <stdlib.h>

void executeSalaryCounterByRange(std::vector<int>& rangeCounter, const size_t persons);
void printSalaryRangeCounterChart(const std::vector<int>& rangeCounter, const size_t rangeSize);

int
main()
{
    const size_t NUMBER_OF_PERSONS = 15; /// assume we have 15 persons
    const size_t RANGE_SIZE = 9; /// the number of salary range rows
    std::vector<int> salaryRangeCounter(RANGE_SIZE, 0);
    executeSalaryCounterByRange(salaryRangeCounter, NUMBER_OF_PERSONS);
    printSalaryRangeCounterChart(salaryRangeCounter, RANGE_SIZE);
}

/// input gross sales from user and calculate the salary for each salesperson
/// check the weekly salary range and store it in vector's appropriate element
inline void
executeSalaryCounterByRange(std::vector<int>& rangeCounter, const size_t persons)
{
    const size_t SHIFTER = 1;
    for (size_t i = 0; i < persons; ++i) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input gross sale value for salesperson No " << i + SHIFTER << ": ";
        }
        int grossSale;
        std::cin >> grossSale;
        if (grossSale < 0) {
            std::cerr << "Error 1: Sales cannot be negative!" << std::endl;
            ::exit(1);
        }
        const int salary = 200 + grossSale * 0.09;
        int counter = salary / 100 - 2; /// generate subscript for counter array
        if (counter >= 8) {
            counter = 8; /// the maximal subscript of the array
        }
        /// increment the counter for appropriate range
        ++rangeCounter[counter];
    }
}

/// print the range chart and the number of salespeople who earned salary in appropriate range
inline void
printSalaryRangeCounterChart(const std::vector<int>& rangeCounter, const size_t rangeSize)
{
    std::cout << "SALARY RANGE\t" << "SALESPEOPLE" << std::endl;
    for (size_t i = 0; i < rangeSize; ++i) {
        const int indentedValue = (i + 2) * 100; 
        if (indentedValue >= 1000) {
            std::cout << "$" << indentedValue << " and over\t" << rangeCounter[i] << "\n";
            break;
        }
        std::cout << "$" << indentedValue << " - " << "$" << indentedValue + 99 << "\t" << rangeCounter[i] << "\n";
    }
}

