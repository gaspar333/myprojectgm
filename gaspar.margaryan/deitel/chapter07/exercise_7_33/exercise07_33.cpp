/// Exercsie7_33.cpp
/// Linear search of an array reursively
#include <iostream>
#include <unistd.h>

int linearSearch(const int array[], const size_t size, const int searchKey);

int
main()
{
    const size_t ARRAY_SIZE = 100;
    int array[ARRAY_SIZE] = {};
    for (size_t i = 0; i < ARRAY_SIZE; ++i) {
        array[i] = 2 * i;
    }
    int searchKey;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter integer search key: ";
    }
    std::cin >> searchKey;
    const int element = linearSearch(array, ARRAY_SIZE, searchKey);
    if (-1 != element) {
        std::cout << "Found value in " << element << std::endl;
    } else {
        std::cout << "Value not found!" << std::endl;
    }

    return 0;
}

inline int
linearSearch(const int array[], const size_t size, const int searchKey)
{
    if (0 == size) {
        return -1;
    }
    if (searchKey == array[size - 1]) {
        return size;
    }
    return linearSearch(array, size - 1, searchKey);
}

