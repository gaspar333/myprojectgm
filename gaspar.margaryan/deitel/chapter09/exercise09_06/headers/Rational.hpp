/// Complex.hpp
/// header file for class Rational

#ifndef __RATIONAL_HPP__
#define __RATIONAL_HPP__

class Rational
{
public:
    Rational (const int numerator = 1, const int denominator = 1);
    
    Rational add(const Rational& r1) const;
    Rational subtract(const Rational& r1) const;
    Rational multiply(const Rational& r1) const;
    Rational divide(const Rational& r1) const;
    void setNumerator(const int numerator);
    void setDenominator(const int denominator);
    void normalize();
    void printRationalNumber();
    int getNumerator() const;
    int getDenominator() const;
    
private:
    int getGCD() const;
    
private:
    int numerator_;
    int denominator_;
};
    
#endif /// __RATIONAL_HPP__

