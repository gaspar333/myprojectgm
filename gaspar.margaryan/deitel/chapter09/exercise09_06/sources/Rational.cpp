/// Exercise_09_06.cpp
/// class Rational implementation
/// make calculation with rational numbers and
/// print the result
#include "headers/Rational.hpp"

#include <iostream>
#include <iomanip>
#include <cassert>
#include <cmath>

Rational::Rational(const int numerator, const int denominator)
{
    assert (0 != denominator);
    setNumerator(numerator);
    setDenominator(denominator);
}

Rational
Rational::add(const Rational& r1) const
{
    const Rational sum(getNumerator() * r1.getDenominator() + r1.getNumerator() * getDenominator(), getDenominator() * r1.getDenominator());
    return sum;
}

Rational
Rational::subtract(const Rational& r1) const
{
    const Rational subtract(getNumerator()  * r1.getDenominator() - r1.getNumerator() * getDenominator(),getDenominator() * r1.getDenominator());
    return subtract;
}

Rational
Rational::multiply(const Rational& r1) const
{
    const Rational multiple(getNumerator() * r1.getNumerator(), getDenominator() * r1.getDenominator());
    return multiple;
}

Rational
Rational::divide(const Rational& r1) const
{
    const Rational division(getNumerator() * r1.getDenominator(), getDenominator() * r1.getNumerator());
    return division;
}

int
Rational::getNumerator() const
{
    return numerator_;
}

int
Rational::getDenominator() const
{
    return denominator_;
}

void
Rational::normalize()
{
    if (getNumerator() < 0 && getDenominator() < 0) {
        setNumerator(std::abs(getNumerator()));
        setDenominator(std::abs(getDenominator()));
    }
    if (getNumerator() > 0 && getDenominator() < 0) {
        setNumerator(-1 * getNumerator());
        setDenominator(std::abs(getDenominator()));
    }
    const int greatestCommonDivisor = getGCD();
    assert (0 != greatestCommonDivisor);
    setNumerator(getNumerator() / greatestCommonDivisor);
    setDenominator(getDenominator() / greatestCommonDivisor);
}

void
Rational::setNumerator(int numerator)
{
    numerator_ = numerator;
}

void
Rational::setDenominator(int denominator)
{
    denominator_ = denominator;
}

void
Rational::printRationalNumber()
{
    std::cout << getNumerator() << "/" << getDenominator();
}

int
Rational::getGCD() const
{
    int numerator = std::abs(getNumerator());
    int denominator = std::abs(getDenominator());
    assert (0 != denominator);
    while (numerator != denominator) {
        if (0 == numerator) {
            return denominator;
        }
        if (0 == denominator) {
            return numerator;
        }
        if (numerator > denominator) {
            numerator %= denominator;
        } else {
            denominator %= numerator;
        }
    }
    return numerator;
}

