/// Exercise_09_06 main.cpp
/// A program that make rational numbers calculations
/// and prints the result
#include "headers/Rational.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input numerator and denominator: ";
    }
    int numerator1;
    int denominator1;
    std::cin >> numerator1 >> denominator1;
    if (0 == denominator1) {
        std::cerr << "Error 1: Wrong input number!" << std::endl;
        return 1;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input next numerator and denominator: ";
    }
    int numerator2;
    int denominator2;
    std::cin >> numerator2 >> denominator2;
    if (0 == numerator2 || 0 == denominator2) {
        std::cerr << "Error 1: Wrong input number!" << std::endl;
        return 1;
    }
    Rational r1(numerator1, denominator1);
    Rational r2(numerator2, denominator2);
    r1.normalize();
    r2.normalize();
    std::cout << "Here is the rational number1: ";
    r1.printRationalNumber();
    std::cout << std::endl;
    std::cout << "Here is the rational number2: ";
    r2.printRationalNumber();
    std::cout << std::endl;
    /// starting math calculations
    Rational r3 = r1.add(r2);
    r3.normalize();
    std::cout << "The sum of both rational numbers: ";
    r3.printRationalNumber();
    std::cout << std::endl;
    Rational r4 = r1.subtract(r2);
    r4.normalize();
    std::cout << "The subtraction of both rational numbers: ";
    r4.printRationalNumber();
    std::cout << std::endl;
    Rational r5 = r1.multiply(r2);
    r5.normalize();
    std::cout << "The multiple of both rational numbers: ";
    r5.printRationalNumber();
    std::cout << std::endl;
    Rational r6 = r1.divide(r2);
    r6.normalize();
    std::cout << "The division of both rational numbers: ";
    r6.printRationalNumber();
    std::cout << std::endl;

    return 0;
}

