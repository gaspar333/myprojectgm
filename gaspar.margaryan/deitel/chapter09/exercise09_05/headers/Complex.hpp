/// Complex.hpp
/// header file for class Complex

#ifndef __COMPLEX_HPP__
#define __COMPLEX_HPP__

class Complex
{
public:
    Complex(const double realNumber = 0.0, const double imaginaryNumber = 0.0);
    
    void setRealNumber(const double number);
    void setImaginaryNumber(const double number);

    Complex addNumbers(const Complex& c1);

    Complex subtractNumbers(const Complex& c1);

    
    double getRealNumber() const;
    double getImaginaryNumber() const;
    
    void printComplexNumber();
    
private:
    double realNumber_;
    double imaginaryNumber_;
};
    
#endif /// __COMPLEX_HPP__ 
