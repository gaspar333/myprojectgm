/// main.cpp
/// A program that using add or subtract complex numbers
/// and prints the result
#include <iostream>
#include <unistd.h>
#include "headers/Complex.hpp"

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input real number and imaginary number: ";
    }
    double realNumber;
    double imaginaryNumber;
    std::cin >> realNumber >> imaginaryNumber;
    Complex c1(realNumber, imaginaryNumber);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input next real number and imaginary number: ";
    }
    std::cin >> realNumber >> imaginaryNumber;
    Complex c2(realNumber, imaginaryNumber);
    std::cout << "Here is the complex number1: ";
    c1.printComplexNumber();
    std::cout << std::endl;
    std::cout << "Here is the complex number2: ";
    c2.printComplexNumber();
    std::cout << std::endl;

    Complex c3 = c1.addNumbers(c2);
    std::cout << "The sum of both complex numbers: ";
    c3.printComplexNumber();
    std::cout << std::endl;
    Complex c4 = c1.subtractNumbers(c2);
    std::cout << "The subtraction of both complex numbers: ";
    c4.printComplexNumber();
    std::cout << std::endl;

    return 0;
}

