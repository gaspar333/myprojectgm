/// Exercise_09_05.cpp
/// class complex implementation

#include <iostream>
#include <iomanip>

#include "headers/Complex.hpp"


Complex::Complex(const double realNumber, const double imaginaryNumber)
{
    setRealNumber(realNumber);
    setImaginaryNumber(imaginaryNumber);
}

void
Complex::setRealNumber(const double number)
{
    realNumber_ = number;
}

void
Complex::setImaginaryNumber(const double number)
{
    imaginaryNumber_ = number;
}

double
Complex::getRealNumber() const
{
    return realNumber_;
}

double
Complex::getImaginaryNumber() const
{
    return imaginaryNumber_;
}

Complex
Complex::addNumbers(const Complex& c1)
{
    Complex sum;
    sum.setRealNumber(realNumber_ + c1.getRealNumber());
    sum.setImaginaryNumber(imaginaryNumber_ + c1.getImaginaryNumber());
    return sum;
}


Complex
Complex::subtractNumbers(const Complex& c1)
{
    Complex substract;
    substract.setRealNumber(realNumber_ - c1.getRealNumber());
    substract.setImaginaryNumber(imaginaryNumber_ - c1.getImaginaryNumber());
    return substract;
}

void
Complex::printComplexNumber()
{
    std::cout << "(" << realNumber_ << " + " << imaginaryNumber_ << "i)";
}

