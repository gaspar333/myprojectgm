/// Exercise_09_07
/// Time.hpp
/// header file for class Time
#ifndef __TIME_HPP__
#define __TIME_HPP__

#include <cstddef>

class Time
{
public:
    Time(const size_t hour = 0, const size_t minute = 0, const size_t second = 0);
    
    void tick(const size_t tickCount);
    void setCurrentTime();
    void setHours(const size_t time);
    void setMinutes(const size_t time);
    void setSeconds(const size_t time);
    void nextHour();
    void nextMinute();
    void nextSecond();
    size_t getHours() const;
    size_t getMinutes() const;
    size_t getSeconds() const;
    void printUniversalTime();
    void printStandardTime();
    
private:
    size_t hours_;
    size_t minutes_;
    size_t seconds_;
};
    
#endif /// __TIME_HPP__ 
