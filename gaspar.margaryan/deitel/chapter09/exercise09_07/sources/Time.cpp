/// Exercise_09_07
/// Time.cpp
/// class time implementation
/// 86400 = 24 hours * 60 minutes * 60 seconds
/// representing a 24hour day in seconds
/// 3600 is the hour represented in seconds
#include "headers/Time.hpp"

#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <ctime>

Time::Time(const size_t hours, const size_t minutes, const size_t seconds)
{
    if (0 == hours && 0 == minutes && 0 == seconds) {
        setCurrentTime();
        return;
    }
    setHours(hours);
    setMinutes(minutes);
    setSeconds(seconds);
}

void
Time::tick(const size_t tickCount)
{
    /// get the time inputed by the user in total seconds
    const size_t totalSeconds = getHours() * 3600 + getMinutes() * 60 + getSeconds();
    for (size_t sec = totalSeconds; sec <= totalSeconds + tickCount; ++sec) {
        nextSecond();
    }
}

void
Time::setCurrentTime()
{
    const size_t time = static_cast<size_t>(std::time(0));
    setHours((time % 86400) / 3600 + 4); /// GMT +4
    setMinutes((time % 3600) / 60);
    setSeconds(time % 60);
}

void
Time::setHours(const size_t hours)
{
    hours_ = hours % 24;
}

void
Time::setMinutes(const size_t minutes)
{
    minutes_ = minutes % 60;
}

void
Time::setSeconds(const size_t seconds)
{
    seconds_ = seconds % 60;
}

void
Time::nextHour()
{
    const size_t nextHour = getHours() + 1;
    setHours(nextHour);
}

void
Time::nextMinute()
{
    const size_t nextMinute = getMinutes() + 1;
    setMinutes(nextMinute);
    if (nextMinute > 59) {
        nextHour();
    }
}

void
Time::nextSecond()
{
    const size_t nextSecond = getSeconds() + 1;
    setSeconds(nextSecond);
    if (nextSecond > 59) {
        nextMinute();
    }
}

size_t
Time::getHours() const
{
    return hours_;
}

size_t
Time::getMinutes() const
{
    return minutes_;
}

size_t
Time::getSeconds() const
{
    return seconds_;
}

void
Time::printUniversalTime()
{
    std::cout << std::setfill('0') << std::setw(2) << getHours() << ":"
              << std::setw(2) << getMinutes() << ":" << std::setw(2) << getSeconds()
              << std::endl;
}

void
Time::printStandardTime()
{
    std::cout << std::setfill('0') << std::setw(2)
              << ((0 == getHours() || 12 == getHours()) ? 12 : getHours() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinutes()
              << ":" << std::setw(2) << getSeconds() << (getHours() < 12 ? "AM" : "PM")
              << std::endl;
}

