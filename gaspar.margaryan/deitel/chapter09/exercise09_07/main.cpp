/// Exercise_09_07
/// main.cpp
/// A program that prints the ticking time
#include "headers/Time.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
/// Time currentTime;
/// if (::isatty(STDIN_FILENO)) {
///     std::cout << "The current time in Yerevan: ";
/// }
/// currentTime.printStandardTime();
    Time time(23, 59, 30);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input tick count: ";
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Inputed time:\n";
    }
    time.printStandardTime();
    time_t tickCount;
    std::cin >> tickCount;
    time.tick(tickCount);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Inputed time after " << tickCount << " seconds ticking:\n";
    }
    time.printStandardTime();
    time.nextSecond();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The time after next second:\n";
    }
    time.printStandardTime();
    time.nextMinute();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The time after next minute:\n";
    }
    time.printStandardTime();
    time.nextHour();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The time after next hour:\n";
    }
    time.printStandardTime();
    
    return 0;
}

