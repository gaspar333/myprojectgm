/// Exercise_09_13
/// Rectangle.cpp
/// class rectangle implementation

#include "headers/Rectangle.hpp"

#include <iostream>
#include <cmath>
#include <cassert>
#include <unistd.h>

namespace
{
inline bool
areEqual(const float side1, const float side2)
{
    const float epsilon = 0.0001;
    return (side1 - side2 < epsilon) && (side1 - side2 > -epsilon);
}

inline bool
isVertical(const float* point1, const float* point2)
{
    const float deltaX = std::fabs(point1[0] - point2[0]);
    const float deltaY = std::fabs(point1[1] - point2[1]);
    return deltaX < deltaY;
}

inline float
round(const float number)
{
    const float epsilon = 0.0001;
    return std::floor(number + 0.5 + epsilon);
}

static const float PI = 3.14159;
static const int BOX_SIZE = 25;
}

Rectangle::Rectangle(const float x1, const float y1, const float x2, const float y2, const float x3, const float y3, const float x4, const float y4)
{
    /// assuming the initial coordinates of the four corner points of rectangle are in range 0 - 25
    assert(x1 >= 0 && x1 <= BOX_SIZE && x2 >= 0 && x2 <= BOX_SIZE && x3 >= 0 && x3 <= BOX_SIZE && x4 >= 0 && x4 <= BOX_SIZE);
    assert(y1 >= 0 && y1 <= BOX_SIZE && y2 >= 0 && y2 <= BOX_SIZE && y3 >= 0 && y3 <= BOX_SIZE && y4 >= 0 && y4 <= BOX_SIZE);
    setCoordinates(x1, y1, x2, y2, x3, y3, x4, y4);
    assert(isRectangle() || isSquare());
}

void
Rectangle::setCoordinates(const float x1, const float y1, const float x2, const float y2, const float x3, const float y3, const float x4, const float y4)
{
    /// assuming that the corners can get out of the range 0 - 25 after rotating, moving or scaling
    pointA_[0] = x1;
    pointA_[1] = y1;
    pointB_[0] = x2;
    pointB_[1] = y2;
    pointC_[0] = x3;
    pointC_[1] = y3;
    pointD_[0] = x4;
    pointD_[1] = y4;
}

void
Rectangle::setFillCharacter(const char symbol)
{
    for (int i = BOX_SIZE; i >= 0; --i) {
        for (int j = 0; j <= BOX_SIZE; ++j) {
            box_[i][j] = symbol;
        }
    }
}

void
Rectangle::setPerimeterCharacter(const char symbol)
{
    drawLine(pointA_, pointB_, symbol);
    drawLine(pointB_, pointC_, symbol);
    drawLine(pointC_, pointD_, symbol);
    drawLine(pointD_, pointA_, symbol);
}

float
Rectangle::getWidth() const
{
    const float sideAB = getSideLength(pointA_, pointB_);
    const float sideBC = getSideLength(pointB_, pointC_);
    if (sideAB <= sideBC) {
        return sideAB;
    }
    return sideBC;
}

float
Rectangle::getLength() const
{
    const float sideAB = getSideLength(pointA_, pointB_);
    const float sideBC = getSideLength(pointB_, pointC_);
    if (sideAB >= sideBC) {
        return sideAB;
    }
    return sideBC;
}

float
Rectangle::getPerimeter() const
{
    return 2 * (getWidth() + getLength());
}

float
Rectangle::getArea() const
{
    return getWidth() * getLength();
}

float
Rectangle::getSideLength(const float* point1, const float* point2) const
{
    const float x1 = point1[0];
    const float x2 = point2[0];
    const float y1 = point1[1];
    const float y2 = point2[1];
    const float xLength = x1 - x2;
    const float yLength = y1 - y2;
    
    return std::sqrt(xLength * xLength + yLength * yLength);
}

float
Rectangle::getDegree() const
{
    float degree;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input degree of rotation: ";
    }
    std::cin >> degree;
    return degree;
}

bool
Rectangle::isRectangle() const
{
    const float sideAB = getSideLength(pointA_, pointB_);
    const float sideBC = getSideLength(pointB_, pointC_);
    const float sideCD = getSideLength(pointC_, pointD_);
    const float sideDA = getSideLength(pointD_, pointA_);
    const float diagonalAC = getSideLength(pointA_, pointC_);
    const float diagonalBD = getSideLength(pointB_, pointD_);
    const bool areEqualABCD = areEqual(sideAB, sideCD);
    const bool areEqualBCDA = areEqual(sideBC, sideDA);
    const bool areEqualACBD = areEqual(diagonalAC, diagonalBD);
    const bool areEqualABBC = areEqual(sideAB, sideBC);

    return  areEqualABCD && areEqualBCDA && areEqualACBD && !areEqualABBC;
}

bool
Rectangle::isSquare() const
{
    const float sideAB = getSideLength(pointA_, pointB_);
    const float sideBC = getSideLength(pointB_, pointC_);
    const float sideCD = getSideLength(pointC_, pointD_);
    const float sideDA = getSideLength(pointD_, pointA_);
    const float diagonalAC = getSideLength(pointA_, pointC_);
    const float diagonalBD = getSideLength(pointB_, pointD_);
    const bool areEqualABCD = areEqual(sideAB, sideCD);
    const bool areEqualBCDA = areEqual(sideBC, sideDA);
    const bool areEqualACBD = areEqual(diagonalAC, diagonalBD);
    const bool areEqualABBC = areEqual(sideAB, sideBC);

    return  areEqualABCD && areEqualBCDA && areEqualACBD && areEqualABBC;
}

void
Rectangle::drawRectangle(const char symbol1, const char symbol2)
{
    setFillCharacter(symbol1);
    setPerimeterCharacter(symbol2);
    for (int i = BOX_SIZE; i >= 0; --i) {
        for (int j = 0; j <= BOX_SIZE; ++j) {
            std::cout << box_[i][j] << ' ';
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void
Rectangle::rotateRectangle()
{
    float center[2] = {};
    getCenterCoordinates(center);
    const float degree = getDegree();
    float rotatedA[2];
    rotateThePoint(pointA_, center, degree, rotatedA);
    float rotatedB[2];
    rotateThePoint(pointB_, center, degree, rotatedB);
    float rotatedC[2];
    rotateThePoint(pointC_, center, degree, rotatedC);
    float rotatedD[2];
    rotateThePoint(pointD_, center, degree, rotatedD);
    setCoordinates(rotatedA[0], rotatedA[1], rotatedB[0], rotatedB[1], rotatedC[0], rotatedC[1], rotatedD[0], rotatedD[1]);
}

void
Rectangle::scaleRectangle(const float rate)
{
    float center[2] = {};
    getCenterCoordinates(center);
    /// as the radius of a circle
    const float halfDiagonal = getSideLength(pointA_, pointC_) / 2;
    /// as the scaled new radius of a circle
    const float newHalfDiagonal = halfDiagonal * rate;
    /// formula x" = centerX + radius * sin(theta) and y" = centerR + radius * cos(theta)
    const float radiansX1 = getRadians(pointA_[0], center[0], halfDiagonal);
    const float radiansY1 = getRadians(pointA_[1], center[1], halfDiagonal);
    const float radiansX2 = getRadians(pointB_[0], center[0], halfDiagonal);
    const float radiansY2 = getRadians(pointB_[1], center[1], halfDiagonal);
    const float radiansX3 = getRadians(pointC_[0], center[0], halfDiagonal);
    const float radiansY3 = getRadians(pointC_[1], center[1], halfDiagonal);
    const float radiansX4 = getRadians(pointD_[0], center[0], halfDiagonal);
    const float radiansY4 = getRadians(pointD_[1], center[1], halfDiagonal);
    const float aX1 = getScaledCoordinate(radiansX1, newHalfDiagonal, center[0]);
    const float aY1 = getScaledCoordinate(radiansY1, newHalfDiagonal, center[1]);
    const float bX2 = getScaledCoordinate(radiansX2, newHalfDiagonal, center[0]);
    const float bY2 = getScaledCoordinate(radiansY2, newHalfDiagonal, center[1]);
    const float cX3 = getScaledCoordinate(radiansX3, newHalfDiagonal, center[0]);
    const float cY3 = getScaledCoordinate(radiansY3, newHalfDiagonal, center[1]);
    const float dX4 = getScaledCoordinate(radiansX4, newHalfDiagonal, center[0]);
    const float dY4 = getScaledCoordinate(radiansY4, newHalfDiagonal, center[1]);
    setCoordinates(aX1, aY1, bX2, bY2, cX3, cY3, dX4, dY4);
}

void
Rectangle::moveRectangle(const float x, const float y)
{
    setCoordinates(pointA_[0] + x, pointA_[1] + y, pointB_[0] + x, pointB_[1] + y, pointC_[0] + x, pointC_[1] + y, pointD_[0] + x, pointD_[1] + y);
}

void 
Rectangle::drawLine(const float* point1, const float* point2 ,const char symbol)
{
    if (isVertical(point1, point2)) {
        drawLineVertically(point1, point2, symbol);
        return;
    }
    drawLineHorizontally(point1, point2, symbol);
}

void
Rectangle::drawLineVertically(const float* point1, const float* point2 ,const char symbol)
{
    const int minY = std::max(0, std::min(static_cast<int>(round(point1[1])), static_cast<int>(round(point2[1]))));
    const int maxY = std::min(std::max(static_cast<int>(round(point1[1])), static_cast<int>(round(point2[1]))), BOX_SIZE);
    const float deltaX = point1[0] - point2[0];
    const float deltaY = point1[1] - point2[1];
    assert(0 != deltaY);
    for (int y = minY; y <= maxY; ++y) {
        /// from formula "y = m * x + a" where "m = deltaY / deltaX" and "a = y - m * x"
        const int x = static_cast<int>(round(y * deltaX / deltaY - point1[1] * deltaX / deltaY + point1[0]));
        box_[y][x] = symbol;
    }
}

void
Rectangle::drawLineHorizontally(const float* point1, const float* point2 ,const char symbol)
{
    const int minX = std::max(0, std::min(static_cast<int>(round(point1[0])), static_cast<int>(round(point2[0]))));
    const int maxX = std::min(std::max(static_cast<int>(round(point1[0])), static_cast<int>(round(point2[0]))), BOX_SIZE);
    const float deltaX = point1[0] - point2[0];
    const float deltaY = point1[1] - point2[1];
    assert(0 != deltaX);
    for (int x = minX; x <= maxX; ++x) {
        /// from formula "y = m * x + a" where "m = deltaY / deltaX" and "a = y - m * x"
        const int y = static_cast<int>(round(deltaY / deltaX * x + point1[1] - deltaY / deltaX * point1[0]));
        box_[y][x] = symbol;
    }
}

void
Rectangle::getCenterCoordinates(float* center) const
{
    center[0] = pointA_[0] - (pointA_[0] - pointC_[0]) / 2;
    center[1] = pointA_[1] - (pointA_[1] - pointC_[1]) / 2;
}

void
Rectangle::rotateThePoint(const float* point, const float* center, const float degree, float* rotatedPoints)
{
    const float x = point[0];
    const float y = point[1];
    const float cx = center[0];
    const float cy = center[1];
    const float tempX = x - cx;
    const float tempY = y - cy;
    const float radians = degree * PI / 180;
    const float rotateX = tempX * std::cos(radians) - tempY * std::sin(radians) + cx;
    const float rotateY = tempX * std::sin(radians) + tempY * std::cos(radians) + cy;
    rotatedPoints[0] = rotateX;
    rotatedPoints[1] = rotateY;
}

float
Rectangle::getRadians(const float point, const float center, const float radius) const
{
    return (point - center) / radius;
}

float
Rectangle::getScaledCoordinate(const float radians, const float radius, const float center) const
{
    return (radians * radius) + center;
}

