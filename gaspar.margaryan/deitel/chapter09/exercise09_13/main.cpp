/// Exercise_09_12
/// main.cpp
/// A program that inputs 4 set of coordinates
/// specifies rectangle (or square), calculates its
/// width, length, perimeter and area
/// rotate, move, scale rectangle and print it on 25 X 25 sized box
/// using <Rectangle> class
#include "headers/Rectangle.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 4 coordinates clockwise direction order to specify rectangle: ";
    }
    float x1, y1, x2, y2, x3, y3, x4, y4;
    std::cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3 >> x4 >> y4;
    Rectangle rectangle(x1, y1, x2, y2, x3, y3, x4, y4);
    if (rectangle.isRectangle()) {
        std::cout << "Inputed coordinates specify a rectangle." << std::endl;
        std::cout << "The width of rectangle is " << rectangle.getWidth() << std::endl;
        std::cout << "The length of rectangle is " << rectangle.getLength() << std::endl;
        std::cout << "The perimeter of rectangle is " << rectangle.getPerimeter() << std::endl;
        std::cout << "The area of rectangle is " << rectangle.getArea() << std::endl;
    }
    if (rectangle.isSquare()) {
        std::cout << "Inputed coordinates specify a square." << std::endl;
        std::cout << "The side of square is " << rectangle.getWidth() << std::endl;
        std::cout << "The perimeter of square is " << rectangle.getPerimeter() << std::endl;
        std::cout << "The area of square is " << rectangle.getArea() << std::endl;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input a symbol for blank graph: ";
    }
    char symbol1;
    std::cin >> symbol1;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input a symbol for rectangle graph: ";
    }
    char symbol2;
    std::cin >> symbol2;
    rectangle.drawRectangle(symbol1, symbol2);

    rectangle.rotateRectangle();
    rectangle.drawRectangle(symbol1, symbol2);

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input \"x\" and \"y\" value to move the rectangle: ";
    }
    float x, y;
    std::cin >> x >> y;
    rectangle.moveRectangle(x, y);
    rectangle.drawRectangle(symbol1, symbol2);

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input a rate to scale the rectangle: ";
    }
    float scaleRate;
    std::cin >> scaleRate;
    rectangle.scaleRectangle(scaleRate);
    rectangle.drawRectangle(symbol1, symbol2);

    return 0;
}

