/// Rectangle.hpp
/// header file for class Rectangle

#ifndef __RECTANGLE_HPP__
#define __RECTANGLE_HPP__

class Rectangle
{
public:
    Rectangle(const float x1 = 0, const float y1 = 0, const float x2 = 0, const float y2 = 0, const float x3 = 0, const float y3 = 0, const float x4 = 0, const float y4 = 0);
    
    void setCoordinates(const float x1, const float y1, const float x2, const float y2, const float x3, const float y3, const float x4, const float y4);
    void setFillCharacter(const char symbol);
    void setPerimeterCharacter(const char symbol);
    float getWidth() const;
    float getLength() const;
    float getPerimeter() const;
    float getArea() const;
    float getSideLength(const float* point1, const float* point2) const;
    float getDegree() const;
    bool isRectangle() const;
    bool isSquare() const;
    void drawRectangle(const char symbol1, const char symbol2);
    void rotateRectangle();
    void scaleRectangle(const float rate);
    void moveRectangle(const float x, const float y);
    
private:
    void drawLine(const float* point1, const float* point2, const char symbol);
    void drawLineVertically(const float* point1, const float* point2, const char symbol);
    void drawLineHorizontally(const float* point1, const float* point2, const char symbol);
    void getCenterCoordinates(float* center) const;
    void rotateThePoint(const float* point, const float* center, const float degree, float* rotatedPoints);
    float getRadians(const float point, const float center, const float radius) const;
    float getScaledCoordinate(const float radians, const float radius, const float center) const;

private:
    float pointA_[2];
    float pointB_[2];
    float pointC_[2];
    float pointD_[2];
    char box_[26][26];
};
    
#endif /// __RECTANGLE_HPP__

