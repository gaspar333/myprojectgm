/// main.cpp
/// A program that using library <ctime>
/// prints the current time
#include <iostream>
#include "headers/Time.hpp"

int
main()
{
    Time currentTime;
    std::cout << "The current time in Yerevan in universal format: ";
    currentTime.printUniversalTime();
    std::cout << "The current time in Yerevan in standard format:  ";
    currentTime.printStandardTime();
    Time t1(static_cast<time_t>(17));
    std::cout << "Time t1 in universal format: ";
    t1.printUniversalTime();
    std::cout << "Time t1 in standard format:  ";
    t1.printStandardTime();
    Time t2(static_cast<time_t>(17), static_cast<time_t>(30));
    std::cout << "Time t2 in universal format: ";
    t2.printUniversalTime();
    std::cout << "Time t2 in standard format:  ";
    t2.printStandardTime();
    Time t3(static_cast<time_t>(17), static_cast<time_t>(30), static_cast<time_t>(46));
    std::cout << "Time t3 in universal format: ";
    t3.printUniversalTime();
    std::cout << "Time t3 in standard format:  ";
    t3.printStandardTime();

    return 0;
}

