/// Time.hpp
/// header file for class Time

#ifndef __TIME_HPP__
#define __TIME_HPP__

class Time
{
public:
    Time(const time_t hours = 0, const time_t minutes = 0, const time_t seconds = 0);
    
    void setHours(const time_t hours);
    void setMinutes(const time_t minutes);
    void setSeconds(const time_t seconds);
    
    time_t getHours();
    time_t getMinutes();
    time_t getSeconds();
    
    void printUniversalTime();
    void printStandardTime();
    
private:
    time_t hours_;
    time_t minutes_;
    time_t seconds_;
};
    
#endif /// __TIME_HPP__ 
