/// Exercise_09_04
/// Time.cpp
/// class time implementation

#include <iostream>
#include <iomanip>
#include <ctime>

#include "headers/Time.hpp"

Time::Time(const time_t hours, const time_t minutes, const time_t seconds)
{
    if (0 == hours && 0 == minutes && 0 == seconds) { 
        const time_t time = std::time(0);
        setHours((time % 86400) / 3600 + 4);  /// GMT +4
        setMinutes((time % 3600) / 60);
        setSeconds(time % 60);
        return;
    }
    setHours(hours);
    setMinutes(minutes);
    setSeconds(seconds);
}

void
Time::setHours(const time_t hours)
{
    hours_ = hours % 24;
}

void
Time::setMinutes(const time_t minutes)
{
    minutes_ = minutes % 60;
}

void
Time::setSeconds(const time_t seconds)
{
    seconds_ = seconds % 60;
}

time_t
Time::getHours()
{
    return hours_;
}

time_t
Time::getMinutes()
{
    return minutes_;
}

time_t
Time::getSeconds()
{
    return seconds_;
}

void
Time::printUniversalTime()
{
    std::cout << std::setfill('0') << std::setw(2) << getHours() << ":"
              << std::setw(2) << getMinutes() << ":" << std::setw(2) << getSeconds()
              << std::endl;
}

void
Time::printStandardTime()
{
    std::cout << std::setfill('0') << std::setw(2)
              << ((0 == getHours() || 12 == getHours()) ? 12 : getHours() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinutes()
              << ":" << std::setw(2) << getSeconds() << (getHours() < 12 ? " AM" : " PM")
              << std::endl;
}

