/// Exercise_09_08
/// main.cpp
/// A program that prints the date
#include "headers/Date.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    Date date(2020, 1, 22);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The inputed date is:\n";
    }
    date.printDate();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input days to iterate the date: ";
    }
    size_t days;
    std::cin >> days;
    date.iterateDate(days);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The date after " << days << " days:\n";
    }
    date.printDate();
        if (::isatty(STDIN_FILENO)) {
        std::cout << "The date after a day:\n";
    }
    date.nextDay();
    date.printDate();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The date after a month:\n";
    }
    date.nextMonth();
    date.printDate();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The date after a year:\n";
    }
    date.nextYear();
    date.printDate();

    return 0;
}

