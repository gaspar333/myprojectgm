/// Exercise_09_08
/// Date.cpp
/// class Date implementation
#include "headers/Date.hpp"

#include <iostream>
#include <iomanip>
#include <unistd.h>

static const size_t monthDays[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

Date::Date(const size_t year, const size_t month, const size_t day)
{
    setYear(year);
    setMonth(month);
    setDay(day);
}

void
Date::setYear(const size_t year)
{
    if (year < 1900 || year > 9999) {
        year_ = 1900; /// initialize to default
        return;
    }

    year_ = year;
}

void
Date::setMonth(const size_t month)
{
    if (month < 1 || month > 12) {
        month_ = 1; /// initialize to default
        return;
    }
    month_ = month;
}

void
Date::setDay(const size_t day)
{
    /// ternary operator for february 29
    if (day < 1 || day > getMonthDaysCount()) {
        day_ = 1; /// initialize to default
        return;
    }
    day_ = day;
}

size_t
Date::getYear() const
{
    return year_;
}

size_t
Date::getMonth() const
{
    return month_;
}

size_t
Date::getDay() const
{
    return day_;
}

void
Date::iterateDate(const size_t days)
{
    for (size_t day = 0; day <= days; ++day) {
        nextDay();
    }
}

void
Date::nextYear()
{
    setYear(getYear() + 1);
}

void
Date::nextMonth()
{
    const size_t nextMonth = getMonth() + 1;
    setMonth(nextMonth);
    if (nextMonth > 12) {
        nextYear();
    }
}

void
Date::nextDay()
{
    const size_t nextDay = getDay() + 1;
    const size_t monthDaysCount = getMonthDaysCount();
    if (nextDay > monthDaysCount) {
        nextMonth();
    }
    setDay(nextDay);
}

void
Date::printDate()
{
    std::cout << std::setfill('0') << std::setw(2) << getDay() << "-"
              << std::setw(2) << getMonth() << "-" << std::setw(4)
              << getYear() << std::endl;
}

/// setting leap year (aka february 29) for every 4 years, 100 years and 400 years
bool
Date::isLeapYear() const
{
    const size_t year = getYear();
    return (0 == year % 4 && 0 != year % 100) || (0 == year % 400);
}

size_t
Date::getMonthDaysCount() const
{
    return (isLeapYear() && 2 == getMonth()) ? monthDays[getMonth()] + 1 : monthDays[getMonth()];
}

