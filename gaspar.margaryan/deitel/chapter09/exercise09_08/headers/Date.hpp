/// Exercise_09_08
/// Date.hpp
/// header file for class Date

#ifndef __DATE_HPP__
#define __DATE_HPP__

#include <cstddef>

class Date
{
public:
    Date (const size_t year = 1900, const size_t month = 1, const size_t day = 1);

    void setYear(const size_t year);
    void setMonth(const size_t month);
    void setDay(const size_t day);
    size_t getYear() const;
    size_t getMonth() const;
    size_t getDay() const;
    void nextYear();
    void nextMonth();
    void nextDay();
    void iterateDate(const size_t days);
    void printDate();
    bool isLeapYear() const;
private:
    size_t getMonthDaysCount() const;
private:
    size_t day_;
    size_t month_;
    size_t year_;
};

#endif /// __DATE_HPP__

