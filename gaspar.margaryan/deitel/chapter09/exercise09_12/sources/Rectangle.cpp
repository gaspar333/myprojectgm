/// Exercise_09_12
/// Rectangle.cpp
/// class rectangle implementation

#include "headers/Rectangle.hpp"

#include <cmath>
#include <cassert>

namespace
{
inline bool
areEqual(const float side1, const float side2)
{
    const float epsilon = 0.0001;
    return (side1 - side2 < epsilon) && (side1 - side2 > -epsilon);
}
}

Rectangle::Rectangle(const float x1, const float y1, const float x2, const float y2, const float x3, const float y3, const float x4, const float y4)
{
    assert(x1 >= 0 && x1 <= 20 && x2 >= 0 && x2 <= 20 && x3 >= 0 && x3 <= 20 && x4 >= 0 && x4 <= 20);
    assert(y1 >= 0 && y1 <= 20 && y2 >= 0 && y2 <= 20 && y3 >= 0 && y3 <= 20 && y4 >= 0 && y4 <= 20);
    setCoordinates(x1, y1, x2, y2, x3, y3, x4, y4);
    assert(isRectangle() || isSquare());
}

void
Rectangle::setCoordinates(const float x1, const float y1, const float x2, const float y2, const float x3, const float y3, const float x4, const float y4)
{
    assert(x1 >= 0 && x1 <= 20 && x2 >= 0 && x2 <= 20 && x3 >= 0 && x3 <= 20 && x4 >= 0 && x4 <= 20);
    assert(y1 >= 0 && y1 <= 20 && y2 >= 0 && y2 <= 20 && y3 >= 0 && y3 <= 20 && y4 >= 0 && y4 <= 20);
    assert(x1 < x4 && x2 < x3);
    assert(y1 < y2 && y4 < x3);
    pointA_[0] = x1;
    pointA_[1] = y1;
    pointB_[0] = x2;
    pointB_[1] = y2;
    pointC_[0] = x3;
    pointC_[1] = y3;
    pointD_[0] = x4;
    pointD_[1] = y4;
}

float
Rectangle::getWidth() const
{
    const float sideAB = getSideLength(pointA_, pointB_);
    const float sideBC = getSideLength(pointB_, pointC_);
    if (sideAB <= sideBC) {
        return sideAB;
    }
    return sideBC;
}

float
Rectangle::getLength() const
{
    const float sideAB = getSideLength(pointA_, pointB_);
    const float sideBC = getSideLength(pointB_, pointC_);
    if (sideAB >= sideBC) {
        return sideAB;
    }
    return sideBC;
}

float
Rectangle::getPerimeter() const
{
    return 2 * (getWidth() + getLength());
}

float
Rectangle::getArea() const
{
    return getWidth() * getLength();
}

float
Rectangle::getSideLength(const float coordinate1[], const float coordinate2[]) const
{
    const float x1 = coordinate1[0];
    const float x2 = coordinate2[0];
    const float y1 = coordinate1[1];
    const float y2 = coordinate2[1];
    const float xLength = x1 - x2;
    const float yLength = y1 - y2;
    
    return std::sqrt(xLength * xLength + yLength * yLength);
}

bool
Rectangle::isRectangle() const
{
    const float sideAB = getSideLength(pointA_, pointB_);
    const float sideBC = getSideLength(pointB_, pointC_);
    const float sideCD = getSideLength(pointC_, pointD_);
    const float sideDA = getSideLength(pointD_, pointA_);
    const float diagonalAC = getSideLength(pointA_, pointC_);
    const float diagonalBD = getSideLength(pointB_, pointD_);
    const bool areEqualABCD = areEqual(sideAB, sideCD);
    const bool areEqualBCDA = areEqual(sideBC, sideDA);
    const bool areEqualACBD = areEqual(diagonalAC, diagonalBD);
    const bool areEqualABBC = areEqual(sideAB, sideBC);

    return  areEqualABCD && areEqualBCDA && areEqualACBD && !areEqualABBC;
}

bool
Rectangle::isSquare() const
{
    const float sideAB = getSideLength(pointA_, pointB_);
    const float sideBC = getSideLength(pointB_, pointC_);
    const float sideCD = getSideLength(pointC_, pointD_);
    const float sideDA = getSideLength(pointD_, pointA_);
    const float diagonalAC = getSideLength(pointA_, pointC_);
    const float diagonalBD = getSideLength(pointB_, pointD_);
    const bool areEqualABCD = areEqual(sideAB, sideCD);
    const bool areEqualBCDA = areEqual(sideBC, sideDA);
    const bool areEqualACBD = areEqual(diagonalAC, diagonalBD);
    const bool areEqualABBC = areEqual(sideAB, sideBC);

    return  areEqualABCD && areEqualBCDA && areEqualACBD && areEqualABBC;
}


