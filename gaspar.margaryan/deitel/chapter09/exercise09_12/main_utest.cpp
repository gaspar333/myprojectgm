#include "headers/Rectangle.hpp"

#include <gtest/gtest.h>

TEST(Rectangle, width)
{
    const Rectangle rect(0, 6, 3, 10, 11, 4, 8, 0);
    EXPECT_EQ(rect.getWidth(), 5);
}

TEST(Rectangle, length)
{
    const Rectangle rect(0, 6, 3, 10, 11, 4, 8, 0);
    EXPECT_EQ(rect.getLength(), 10);
}

TEST(Rectangle, perimeter)
{
    const Rectangle rect(0, 6, 3, 10, 11, 4, 8, 0);
    EXPECT_EQ(rect.getPerimeter(), 30);
}

TEST(Rectangle, area)
{
    const Rectangle rect(0, 6, 3, 10, 11, 4, 8, 0);
    EXPECT_EQ(rect.getArea(), 50);
}

TEST(Rectangle, isRectangle)
{
    const Rectangle rect(0, 6, 3, 10, 11, 4, 8, 0);
    EXPECT_EQ(rect.isRectangle(), true);
    EXPECT_EQ(rect.isSquare(), false);
}

TEST(Rectangle, isSquare)
{
    const Rectangle rect(1, 1, 1, 4, 4, 4, 4, 1);
    EXPECT_EQ(rect.isSquare(), true);
    EXPECT_EQ(rect.isRectangle(), false);
}

int
main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

