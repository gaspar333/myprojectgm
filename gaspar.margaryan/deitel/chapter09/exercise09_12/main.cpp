/// Exercise_09_12
/// main.cpp
/// A program that inputs 4 set of coordinates
/// specifies rectangle (or square), calculates its
/// width, length, perimeter and area
/// using <Rectangle> class
#include "headers/Rectangle.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 4 coordinates clockwise direction order to specify rectangle: ";
    }
    float x1, y1, x2, y2, x3, y3, x4, y4;
    std::cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3 >> x4 >> y4;
    Rectangle rectangle(x1, y1, x2, y2, x3, y3, x4, y4);
    if (rectangle.isRectangle()) {
        std::cout << "Inputed coordinates specify a rectangle." << std::endl;
        std::cout << "The width of rectangle is " << rectangle.getWidth() << std::endl;
        std::cout << "The length of rectangle is " << rectangle.getLength() << std::endl;
        std::cout << "The perimeter of rectangle is " << rectangle.getPerimeter() << std::endl;
        std::cout << "The area of rectangle is " << rectangle.getArea() << std::endl;
    }
    if (rectangle.isSquare()) {
        std::cout << "Inputed coordinates specify a square." << std::endl;
        std::cout << "The side of square is " << rectangle.getWidth() << std::endl;
        std::cout << "The perimeter of square is " << rectangle.getPerimeter() << std::endl;
        std::cout << "The area of square is " << rectangle.getArea() << std::endl;
    }
    
    return 0;
}

