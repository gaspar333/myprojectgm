/// Rectangle.hpp
/// header file for class Rectangle

#ifndef __RECTANGLE_HPP__
#define __RECTANGLE_HPP__

class Rectangle
{
public:
    Rectangle(const float x1 = 0, const float y1 = 0, const float x2 = 0, const float y2 = 0, const float x3 = 0, const float y3 = 0, const float x4 = 0, const float y4 = 0);
    
    void setCoordinates(const float x1, const float y1, const float x2, const float y2, const float x3, const float y3, const float x4, const float y4);
    float getWidth() const;
    float getLength() const;
    float getPerimeter() const;
    float getArea() const;
    float getSideLength(const float coordinate1[], const float coordinate2[]) const;
    bool isRectangle() const;
    bool isSquare() const;
    
private:
    float pointA_[2];
    float pointB_[2];
    float pointC_[2];
    float pointD_[2];
};
    
#endif /// __RECTANGLE_HPP__ 
