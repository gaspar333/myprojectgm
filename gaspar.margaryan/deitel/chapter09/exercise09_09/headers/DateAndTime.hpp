/// Exercise_09_08
/// Date.hpp
/// header file for class Date

#ifndef __DATE_AND_TIME_HPP__
#define __DATE_AND_TIME_HPP__

#include <cstddef>

class DateAndTime
{
public:
    DateAndTime(const size_t year = 1900, const size_t month = 1, const size_t day = 1, const size_t hour = 0, const size_t minute = 0, const size_t second = 0);
    
    void setYear(const size_t year);
    void setMonth(const size_t month);
    void setDay(const size_t day);
    void setCurrentTime();
    void setHours(const size_t time);
    void setMinutes(const size_t time);
    void setSeconds(const size_t time);
    size_t getYear() const;
    size_t getMonth() const;
    size_t getDay() const;
    size_t getHours() const;
    size_t getMinutes() const;
    size_t getSeconds() const;
    void tick(const size_t tickCount);
    void nextYear();
    void nextMonth();
    void nextDay();
    void nextHour();
    void nextMinute();
    void nextSecond();
    void iterateDate(const size_t days);
    void printUniversal();
    void printStandard();
    bool isLeapYear() const;

private:
    size_t getMonthDaysCount() const;
    
private:
    size_t year_;
    size_t month_;
    size_t day_;
    size_t hours_;
    size_t minutes_;
    size_t seconds_;
};

#endif /// __DATE_AND_TIME_HPP__ 
