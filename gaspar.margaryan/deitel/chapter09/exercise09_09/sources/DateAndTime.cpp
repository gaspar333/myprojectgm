/// Exercise_09_08
/// Date.cpp
/// class date implementation
#include "headers/DateAndTime.hpp"

#include <iostream>
#include <iomanip>
#include <ctime>

static const size_t monthDays[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

DateAndTime::DateAndTime(const size_t year, const size_t month, const size_t day, const size_t hours, const size_t minutes, const size_t seconds)
{
    if (0 == hours && 0 == minutes && 0 == seconds) {
        setCurrentTime();
        return;
    }
    setYear(year);
    setMonth(month);
    setDay(day);
    setHours(hours);
    setMinutes(minutes);
    setSeconds(seconds);
}

void
DateAndTime::setYear(const size_t year)
{
    if (year < 1900 || year > 9999) {
        year_ = 1900; /// initialize to default
        return;
    }
    year_ = year;
}

void
DateAndTime::setMonth(const size_t month)
{
    if (month < 1 || month > 12) {
        month_ = 1; /// initialize to default
        return;
    }
    month_ = month;
}

void
DateAndTime::setDay(const size_t day)
{
    /// ternary operator for february 29
    if (day < 1 || day > getMonthDaysCount()) {
        day_ = 1; /// initialize to default
        return;
    }
    day_ = day;
}

void
DateAndTime::setCurrentTime()
{
    const size_t time = static_cast<size_t>(std::time(0));
    setHours((time % 86400) / 3600 + 4); /// GMT +4
    setMinutes((time % 3600) / 60);
    setSeconds(time % 60);
}

void
DateAndTime::setHours(const size_t hours)
{
    hours_ = hours % 24;
}

void
DateAndTime::setMinutes(const size_t minutes)
{
    minutes_ = minutes % 60;
}

void
DateAndTime::setSeconds(const size_t seconds)
{
    seconds_ = seconds % 60;
}

size_t
DateAndTime::getYear() const
{
    return year_;
}

size_t
DateAndTime::getMonth() const
{
    return month_;
}

size_t
DateAndTime::getDay() const
{
    return day_;
}

size_t
DateAndTime::getHours() const
{
    return hours_;
}

size_t
DateAndTime::getMinutes() const
{
    return minutes_;
}

size_t
DateAndTime::getSeconds() const
{
    return seconds_;
}

void
DateAndTime::printUniversal()
{
    std::cout << std::setfill('0') << std::setw(2) << getHours() << ":"
              << std::setw(2) << getMinutes() << ":" << std::setw(2) << getSeconds() << " "
              << std::setfill('0') << std::setw(2) << getDay() << "-"
              << std::setw(2) << getMonth() << "-" << std::setw(4)
              << getYear() << std::endl;
}

void
DateAndTime::printStandard()
{
    std::cout << std::setfill('0') << std::setw(2)
              << ((0 == getHours() || 12 == getHours()) ? 12 : getHours() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinutes()
              << ":" << std::setw(2) << getSeconds() << (getHours() < 12 ? "AM " : "PM ")
              << std::setfill('0') << std::setw(2) << getDay() << "-"
              << std::setw(2) << getMonth() << "-" << std::setw(4)
              << getYear() << std::endl;
}

void
DateAndTime::tick(const size_t tickCount)
{
    /// get the time inputed by the user in total seconds
    const size_t totalSeconds = getHours() * 3600 + getMinutes() * 60 + getSeconds();
    for (size_t sec = totalSeconds; sec <= totalSeconds + tickCount; ++sec) {
        nextSecond();
    }
}

void
DateAndTime::nextYear()
{
    setYear(getYear() + 1);
}

void
DateAndTime::nextMonth()
{
    const size_t nextMonth = getMonth() + 1;
    setMonth(nextMonth);
    if (nextMonth > 12) {
        nextYear();
    }
}

void
DateAndTime::nextDay()
{
    const size_t nextDay = getDay() + 1;
    const size_t monthDaysCount = getMonthDaysCount();
    if (nextDay > monthDaysCount) {
        nextMonth();
    }
    setDay(nextDay);
}

void
DateAndTime::nextHour()
{
    const size_t nextHour = getHours() + 1;
    setHours(nextHour);
    if (nextHour > 23) {
        nextDay();
    }
}

void
DateAndTime::nextMinute()
{
    const size_t nextMinute = getMinutes() + 1;
    setMinutes(nextMinute);
    if (nextMinute > 59) {
        nextHour();
    }
}

void
DateAndTime::nextSecond()
{
    const size_t nextSecond = getSeconds() + 1;
    setSeconds(nextSecond);
    if (nextSecond > 59) {
        nextMinute();
    }
}

void
DateAndTime::iterateDate(const size_t days)
{
    for (size_t day = 0; day <= days; ++day) {
        nextDay();
    }
}

/// setting leap year (aka february 29) for every 4 years, 100 years and 400 years
bool
DateAndTime::isLeapYear() const
{
    const size_t year = getYear();
    return ((0 == year % 4 && 0 != year % 100) || (0 == year % 400)) ? true : false;
}

size_t
DateAndTime::getMonthDaysCount() const
{
    return (isLeapYear() && 2 == getMonth()) ? monthDays[getMonth()] + 1 : monthDays[getMonth()];
}

