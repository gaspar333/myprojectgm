/// Exercise_09_09
/// main.cpp
/// A program that prints the date and time
#include "headers/DateAndTime.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    DateAndTime dateAndTime(2020, 12, 31, 23, 59, 30);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Inputed date and time is:\n";
    }    
    dateAndTime.printStandard();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input seconds amount to tick the time: ";
    }
    size_t seconds;
    std::cin >> seconds;
    dateAndTime.tick(seconds);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The date and time after " << seconds << " seconds tick:\n";
    }
    dateAndTime.printStandard();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The date and time after a seconds:\n";
    }
    dateAndTime.nextSecond();
    dateAndTime.printStandard();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The date and time after a minute:\n";
    }
    dateAndTime.nextMinute();
    dateAndTime.printStandard();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The date and time after an hour:\n";
    }
    dateAndTime.nextHour();
    dateAndTime.printStandard();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input days amount to iterate the date:\n";
    }
    size_t days;
    std::cin >> days;
    dateAndTime.iterateDate(days);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The date after " << days << " days:\n";
    }
    dateAndTime.printStandard();
    dateAndTime.nextDay();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The date after a day:\n";
    }
    dateAndTime.printStandard();
    dateAndTime.nextMonth();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The date after a month:\n";
    }
    dateAndTime.printStandard();
    dateAndTime.nextYear();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "The date after a year:\n";
    }
    dateAndTime.printStandard();

    return 0;
}

