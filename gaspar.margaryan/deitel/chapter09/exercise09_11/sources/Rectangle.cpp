/// Exercise_09_11
/// Rectangle.cpp
/// class rectangle implementation

#include "headers/Rectangle.hpp"

Rectangle::Rectangle(const float length, const float width)
{
    setLength(length);
    setWidth(width);
}

void
Rectangle::setWidth(const float width)
{
    if (width <= 0 || width >= 20) {
        width_ = 1; /// set to minimal default
        return;
    }
    width_ = width;
}

void
Rectangle::setLength(const float length)
{    
    if (length <= 0 || length >= 20) {
        length_ = 1; /// set to minimal default
        return;
    }
    length_ = length;
}

float
Rectangle::getWidth() const
{
    return width_;
}

float
Rectangle::getLength() const
{
    return length_;
}

float
Rectangle::getArea() const
{
    return getWidth() * getLength();
}

float
Rectangle::getPerimeter() const
{
    return 2 * (getWidth() + getLength());
}

