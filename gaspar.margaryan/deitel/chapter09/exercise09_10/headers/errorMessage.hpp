/// Exercise_09_08
/// Date.hpp
/// header file for class Date

#ifndef __ERROR_MESSAGE_HPP__
#define __ERROR_MESSAGE_HPP__

typedef unsigned int uint;

void printErrorMessage(const uint errorNumber);

#endif /// __ERROR_MESSAGE_HPP__
