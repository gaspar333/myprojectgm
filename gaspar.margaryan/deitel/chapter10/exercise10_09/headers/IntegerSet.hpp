/// IntegerSet.hpp
/// header file for class IntegerSet

#ifndef __INTEGER_SET_HPP__
#define __INTEGER_SET_HPP__

typedef unsigned int uint;

class IntegerSet
{
public:
    static const uint SIZE_ = 100;

public:
    IntegerSet();
    IntegerSet(const uint validInteger);
    IntegerSet(const IntegerSet& intSet);
    IntegerSet(const uint* array, const uint size);
    ~IntegerSet();
    IntegerSet unionOfSets(const IntegerSet& rhv) const;
    IntegerSet intersectionOfSets(const IntegerSet& rhv) const;
    void insertElement(const uint index);
    void deleteElement(const uint index);
    void printSet() const;
    bool getElement(const uint index) const;
    bool isEqualTo(const IntegerSet& rhv) const;

private:
    void emptyTheSet();

private:
    bool arraySet_[SIZE_];
};
    
#endif /// __INTEGER_SET_HPP__ 
