#include "headers/IntegerSet.hpp"

#include <gtest/gtest.h>

TEST(IntegerSet, DefaultConstructor)
{
    IntegerSet intSet;
    for (uint i = 0; i < IntegerSet::SIZE_; ++i) {
        EXPECT_FALSE(intSet.getElement(i));
    }
}

TEST(IntegerSet, ParameterizedConstructor)
{
    IntegerSet intSet(50);
    for (uint i = 0; i < IntegerSet::SIZE_; ++i) {
        if (50 == i) {
            EXPECT_TRUE(intSet.getElement(i));
            continue;
        }
        EXPECT_FALSE(intSet.getElement(i));
    }
}

TEST(IntegerSet, ArrayedConstructor)
{
    const uint array[] = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90};
    const uint arraySize = sizeof(array) / sizeof(uint);
    IntegerSet intSet(array, arraySize);
    for (uint i = 0; i < IntegerSet::SIZE_; ++i) {
        if (0 == i % 10) {
            EXPECT_TRUE(intSet.getElement(i));
            continue;
        }
        EXPECT_FALSE(intSet.getElement(i));
    }
}


TEST(IntegerSet, copyConstructor)
{
    IntegerSet intSet1(20);
    IntegerSet intSet2(intSet1);
    for (uint i = 0; i < IntegerSet::SIZE_; ++i) {
        if (20 == i) {
            EXPECT_TRUE(intSet2.getElement(i));
            continue;
        }
        EXPECT_FALSE(intSet2.getElement(i));
    }
}

TEST(IntegerSet, UnionOfIntegerSets)
{
    const IntegerSet intSet1(20);
    const IntegerSet intSet2(10);
    IntegerSet intSet3 =  intSet2.unionOfSets(intSet1);
        for (uint i = 0; i < IntegerSet::SIZE_; ++i) {
        if (10 == i || 20 == i) {
            EXPECT_TRUE(intSet3.getElement(i));
            continue;
        }
        EXPECT_FALSE(intSet3.getElement(i));
    }
}

TEST(IntegerSet, IntersectionOfIntegerSets)
{
    IntegerSet intSet1(15);
    IntegerSet intSet2(15);
    intSet1.insertElement(10);
    intSet2.insertElement(30);
    IntegerSet intSet3 =  intSet2.intersectionOfSets(intSet1);
        for (uint i = 0; i < IntegerSet::SIZE_; ++i) {
        if (15 == i) {
            EXPECT_TRUE(intSet3.getElement(i));
            continue;
        }
        EXPECT_FALSE(intSet3.getElement(i));
    }
}

TEST(IntegerSet, EquityOfIntegerSets)
{
    IntegerSet intSet1(35);
    IntegerSet intSet2(35);
    intSet1.insertElement(40);
    intSet2.insertElement(40);
    EXPECT_TRUE(intSet2.isEqualTo(intSet1));
}

TEST(IntegerSet, GetElementOfIntegerSets)
{
    const IntegerSet intSet1(22);
    EXPECT_TRUE(intSet1.getElement(22));
}

int
main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

