/// Exercise_10_09
/// IntegerSet.cpp
/// class IntegerSet implementation

#include "headers/IntegerSet.hpp"

#include <iostream>
#include <cassert>
#include <iomanip>

IntegerSet::IntegerSet()
{
    emptyTheSet();
}

IntegerSet::IntegerSet(const IntegerSet& rhv)
{
    for (uint i = 0; i < SIZE_; ++i) {
        arraySet_[i] = rhv.arraySet_[i];
    }
}

IntegerSet::IntegerSet(const uint validInt)
{
    assert(validInt < SIZE_);
    emptyTheSet();
    insertElement(validInt);
}

IntegerSet::IntegerSet(const uint* array, const uint size)
{
    assert(size < SIZE_);
    for (uint i = 0; i < size; ++i) {
        assert(array[i] < SIZE_);
    }
    emptyTheSet();
    for (uint i = 0; i < size; ++i) {
        insertElement(array[i]);
    }
}

IntegerSet::~IntegerSet()
{
}


IntegerSet
IntegerSet::unionOfSets(const IntegerSet& rhv) const
{
    IntegerSet result;
    for (uint i = 0; i < SIZE_; ++i) {
        result.arraySet_[i] = rhv.arraySet_[i] || arraySet_[i];
    }
    return result;
}

IntegerSet
IntegerSet::intersectionOfSets(const IntegerSet& rhv) const
{
    IntegerSet result;
    for (uint i = 0; i < SIZE_; ++i) {
         result.arraySet_[i] = rhv.arraySet_[i] && arraySet_[i];
    }
    return result;
}

void
IntegerSet::insertElement(const uint index)
{
    assert(index < SIZE_);
    arraySet_[index] = true;
}

void
IntegerSet::deleteElement(const uint index)
{
    assert(index < SIZE_);
    arraySet_[index] = false;
}

void
IntegerSet::printSet() const
{
    for (uint i = 0; i < SIZE_; ++i) {
        if (arraySet_[i]) {
            std::cout << std::setw(3) << 1 << " ";
        } else {
            std::cout << "---" << " ";
        }
        if (0 == (i + 1) % 25) {
            std::cout << std::endl;
        }
    }
    std::cout << std::endl;
}

bool
IntegerSet::getElement(const uint index) const
{
    assert(index < SIZE_);
    return arraySet_[index];
}

bool
IntegerSet::isEqualTo(const IntegerSet& rhv) const
{
    for (uint i = 0; i < SIZE_; ++i) {
        if (rhv.arraySet_[i] != arraySet_[i]) {
            return false;
        }
    }
    return true;
}

void
IntegerSet::emptyTheSet()
{
    for (uint i = 0; i < SIZE_; ++i) {
        arraySet_[i] = false;
    }
}

