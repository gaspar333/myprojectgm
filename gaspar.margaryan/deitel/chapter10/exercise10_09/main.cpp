/// Exercise_10_09
/// main.cpp
#include "headers/IntegerSet.hpp"

#include <iostream>

int
main()
{
    IntegerSet integerSet1;
    std::cout << "IntegerSet1 after default initialization" << std::endl;
    integerSet1.printSet();
    for (uint i = 0; i < IntegerSet::SIZE_; ++i) {
        integerSet1.insertElement(i);
    }
    std::cout << "IntegerSet1 after setting all alements" << std::endl;
    integerSet1.printSet();
    for (uint i = 0; i < IntegerSet::SIZE_; i += 2) {
        integerSet1.deleteElement(i);
    }
    std::cout << "IntegerSet1 after deleting each even element" << std::endl;
    integerSet1.printSet();
    IntegerSet integerSet2(50);
    std::cout << "IntegerSet2" << std::endl;
    integerSet2.printSet();
    IntegerSet integerSet3 = integerSet1.unionOfSets(integerSet2);
    std::cout << "IntegerSet3 after IntegerSet1 and IntegerSet2 union" << std::endl;
    integerSet3.printSet();
    IntegerSet integerSet4 = integerSet3.intersectionOfSets(integerSet2);
    std::cout << "IntegerSet4 after IntegerSet3 and IntegerSet2 intersection" << std::endl;
    integerSet4.printSet();
    IntegerSet integerSet5(integerSet2);
    std::cout << "IntegerSet5 after copy of IntegerSet2" << std::endl;
    integerSet5.printSet();
    IntegerSet integerSet6;
    std::cout << "IntegerSet6 after default initialization" << std::endl;
    integerSet6.printSet();
    std::cout << "IntegerSet5 is equal to IntegerSet6: " << std::boolalpha << integerSet5.isEqualTo(integerSet6) << std::endl;
    integerSet6.insertElement(50);
    std::cout << "IntegerSet5" << std::endl;
    integerSet5.printSet();
    std::cout << "IntegerSet6" << std::endl;
    integerSet6.printSet();
    std::cout << "IntegerSet5 is equal to IntegerSet6: " << integerSet5.isEqualTo(integerSet6) << std::endl;
    std::cout << "IntegerSet6" << std::endl;
    const uint array[] = {1, 2, 3, 4, 5, 6};
    const uint arraySize = sizeof(array) / sizeof(uint);
    IntegerSet integerSet7(array, arraySize);
    integerSet7.printSet();

    return 0;
}

