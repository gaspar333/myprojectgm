/// Exercise_10_08
/// SavingsAccount.cpp
/// class SavingsAccount implementation

#include "headers/SavingsAccount.hpp"

#include <iostream>
#include <iomanip>
#include <cassert>
#include <unistd.h>
#include <string>

float SavingsAccount::annualInterestRate_ = 0;

float
SavingsAccount::getInterestRate()
{
    return annualInterestRate_;
}

void
SavingsAccount::modifyInterestRate(const float rate)
{
    assert(rate >= 0);
    annualInterestRate_ = rate;
}

SavingsAccount::SavingsAccount(const std::string& name, const float balance) 
    : name_(name)
    , savingsBalance_(balance)
{
    assert(balance >= 0);
}

SavingsAccount::~SavingsAccount()
{
}

void
SavingsAccount::setBalance(const float newBalance)
{
    assert(newBalance >= 0);
    savingsBalance_ = newBalance;
}

float
SavingsAccount::getBalance() const
{
    return savingsBalance_;
}

void
SavingsAccount::calculateMonthlyInterest()
{
    const float currentBalance = savingsBalance_ + savingsBalance_ * getInterestRate() / 100 / 12;
    setBalance(currentBalance);
}

void
SavingsAccount::printBalance() const
{
    std::cout << "The current balance of " << name_ << " is: " << std::fixed << std::setprecision(2) << getBalance() << "$" << std::endl;
}

void
SavingsAccount::setName(const std::string& name)
{
    name_ = name;
}

std::string
SavingsAccount::getName() const
{
    return name_;
}

