#include "headers/SavingsAccount.hpp"

#include <gtest/gtest.h>

TEST(SavingsAccount, DefaultBalance)
{
    const SavingsAccount saver;
    EXPECT_EQ(saver.getBalance(), 0);
}

TEST(SavingsAccount, GetBalance)
{
    const SavingsAccount saver("Saver1", 5000);
    EXPECT_EQ(saver.getBalance(), 5000);
}

TEST(SavingsAccount, DefaultName)
{
    const SavingsAccount saver;
    EXPECT_EQ(saver.getName().length(), 0);
}

TEST(SavingsAccount, GetName)
{
    const SavingsAccount saver("Saver", 5000);
    const char* name = saver.getName().c_str();
    EXPECT_STREQ(name, "Saver");
}

TEST(SavingsAccount, SetName)
{
    SavingsAccount saver;
    saver.setName("Saver1");
    const char* name = saver.getName().c_str();
    EXPECT_STREQ(name, "Saver1");
}

TEST(SavingsAccount, SetBalance)
{
    SavingsAccount saver;
    saver.setBalance(2000);
    EXPECT_EQ(saver.getBalance(), 2000);
}

TEST(SavingsAccount, GetDefaultInterestRate)
{
    const SavingsAccount saver;
    EXPECT_EQ(saver.getInterestRate(), 0);
}

TEST(SavingsAccount, GetModifiedInterestRate)
{
    const SavingsAccount saver;
    SavingsAccount::modifyInterestRate(5);
    EXPECT_EQ(saver.getInterestRate(), 5);
}

TEST(SavingsAccount, CalculateMonthlyInterest)
{
    const float balance = 1000;
    const float rate = 5;
    const float newBalance = balance + balance * rate / 100 / 12;
    SavingsAccount saver("Saver2", balance);
    saver.modifyInterestRate(rate);
    saver.calculateMonthlyInterest();
    EXPECT_EQ(saver.getBalance(), newBalance);
}

int
main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

