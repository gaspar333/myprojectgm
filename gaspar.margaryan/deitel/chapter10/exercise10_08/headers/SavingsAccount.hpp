/// SavingsAccount.hpp
/// header file for class Rectangle

#ifndef __SAVINGS_ACCOUNT_HPP__
#define __SAVINGS_ACCOUNT_HPP__

#include <string>

class SavingsAccount
{
private:
    static float annualInterestRate_;

public:
    static float getInterestRate();
    static void modifyInterestRate(const float rate);
    
public:
    SavingsAccount(const std::string& name = "", const float balance = 0);
    ~SavingsAccount();
    void setBalance(const float newBalance);
    float getBalance() const;
    void calculateMonthlyInterest();
    void printBalance() const;
    void setName(const std::string& name);
    std::string getName() const;

private:
    std::string name_;
    float savingsBalance_;
};
    
#endif /// __SAVINGS_ACCOUNT_HPP__ 
