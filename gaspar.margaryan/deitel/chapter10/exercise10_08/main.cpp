/// Exercise_10_08
/// main.cpp
#include "headers/SavingsAccount.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input annual rate: ";
    }
    float rate;
    std::cin >> rate;
    if (rate < 0) {
        std::cerr << "Error 1: Wrong data input!" << std::endl;
        return 1;
    }
    SavingsAccount::modifyInterestRate(rate);
    SavingsAccount saver1("Saver1", 2000);
    SavingsAccount saver2("Saver2", 3000);
    saver1.calculateMonthlyInterest();
    saver2.calculateMonthlyInterest();
    saver1.printBalance();
    saver2.printBalance();
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input modified rate: ";
    }
    std::cin >> rate;
    if (rate < 0) {
        std::cerr << "Error 1: Wrong data input!" << std::endl;
        return 1;
    }
    SavingsAccount::modifyInterestRate(rate);
    saver1.calculateMonthlyInterest();
    saver2.calculateMonthlyInterest();
    saver1.printBalance();
    saver2.printBalance();

    return 0;
}

