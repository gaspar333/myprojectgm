/// Exercise_11_13
/// Complex.cpp

#include "headers/Complex.hpp"

#include <iostream>
#include <cassert>
#include <iomanip>

Complex::Complex(double realPart, double imaginaryPart)
    : real_(realPart)
    , imaginary_(imaginaryPart)
{
}

Complex::~Complex()
{
}

Complex
Complex::operator+(const Complex& rhv) const
{
    return Complex(real_ + rhv.real_, imaginary_ + rhv.imaginary_);
}

Complex
Complex::operator-(const Complex& rhv) const
{
    return Complex(real_ - rhv.real_, imaginary_ - rhv.imaginary_);
}

Complex
Complex::operator*(const Complex& rhv) const
{
    return Complex(real_ * rhv.real_ - imaginary_ * rhv.imaginary_, real_ * rhv.imaginary_ + rhv.real_ * imaginary_);
}

bool
Complex::operator==(const Complex& rhv) const
{
    return real_ == rhv.real_ && imaginary_ == rhv.imaginary_;
}

bool
Complex::operator!=(const Complex& rhv) const
{
    return !(*this == rhv);
}

std::istream&
operator>>(std::istream& input, Complex& rhv)
{
    input >> rhv.real_ >> rhv.imaginary_;
    return input;
}

std::ostream&
operator<<(std::ostream& output, const Complex& rhv)
{
    output << "(" << rhv.real_ << ", " << rhv.imaginary_ << "i" << ")";
    return output;
}

