/// Exercise_11_13
/// main.cpp
#include "headers/Complex.hpp"

#include <iostream>
#include <iomanip>

int
main()
{
    Complex x;
    Complex y;
    Complex z;
    std::cout << "x: " << x;
    std::cout << "\ny: " << y;
    if (x == y) {
        std::cout << "\n" << x << " is equal to " << y << std::endl;
    }
    std::cin >> y >> z;
    std::cout << "x: " << x;
    std::cout << "\ny: " << y;
    std::cout << "\nz: " << z;
    if (x != y) {
        std::cout << "\n" << x << " is not equal to " << y << std::endl;
    }
    x = y + z;
    std::cout << "\n\nx = y + z:" << std::endl;
    std::cout << "x: " << x;
    std::cout << " = ";
    std::cout << "y: " << y;
    std::cout << " + ";
    std::cout << "z: " << z;
    x = y - z;
    std::cout << "\n\nx = y - z:" << std::endl;
    std::cout << "x: " << x;
    std::cout << " = ";
    std::cout << "y: " << y;
    std::cout << " - ";
    std::cout << "z: " << z;
    x = y * z;
    std::cout << "\n\nx = y * z:" << std::endl;
    std::cout << "x: " << x;
    std::cout << " = ";
    std::cout << "y: " << y;
    std::cout << " * ";
    std::cout << "z: " << z;


    return 0;
}

