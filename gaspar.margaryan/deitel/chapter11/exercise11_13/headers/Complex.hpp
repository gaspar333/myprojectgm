/// Exercise_11_13
/// Complex.hpp header file

#ifndef __COMPLEX__
#define __COMPLEX__

#include <iostream>

class Complex
{
    friend std::ostream& operator<<(std::ostream& output, const Complex& rhv);
    friend std::istream& operator>>(std::istream& input, Complex& rhv);

public:
    Complex(double realPart = 0.0, double imaginaryPart = 0.0);
    ~Complex();
    Complex operator+(const Complex& rhv) const;
    Complex operator-(const Complex& rhv) const;
    Complex operator*(const Complex& rhv) const;
    bool operator==(const Complex& rhv) const;
    bool operator!=(const Complex& rhv) const;

private:
    double real_;
    double imaginary_;
};

#endif /// __COMPLEX__

