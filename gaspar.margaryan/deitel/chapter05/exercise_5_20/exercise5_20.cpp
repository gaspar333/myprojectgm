/// Exercise 5_20
/// Program that finds all Pythagorean integer triples for side1, side2 
/// and hypotenuse all no larger than 500
#include <iostream>  
#include <iomanip>
#include <cmath>

int
main()
{
    int sizeLimit = 500;
    /// the maximal value of hypotenuse for formula side1^2 + side2^2 = hypotenuse^2 
    int sizeLimitSquared = sizeLimit * sizeLimit;
    /// we shall consider that side1 is the smallest from other sides
    /// in order if side1 = side2 using pythagorean formula we shall have
    /// 2*side1^2 = hypotenuse^2 => side1 = hypotenuse / square root 2
    /// which is the maximal size the smallest side may have
    double sqrtOf2 = std::sqrt(2);
    /// the maximal size that smallest side can have as an integer value compared
    /// with hypotenuse is approximately less then 70%
    int minSideLimit = static_cast<int>(sizeLimit / sqrtOf2);
    /// the header
    std::cout << "Side 1: \t" << "Side 2: \t" << "Hypotenuse: \n" << std::endl;
    for (int side1 = 3; side1 <= minSideLimit; ++side1) {
        int side1Sq = side1 * side1; /// loops 351 times
        for (int side2 = side1 + 1; side2 <= sizeLimit; ++side2) {
            int equation = side1Sq + side2 * side2; /// loops 97098 times
            if (equation > sizeLimitSquared) {
                break;
            }
            double equationSqrt = std::sqrt(equation);
            int hypotenuse = static_cast<int>(equationSqrt);
            double epsilon = 0.000001;
            if (equationSqrt - hypotenuse < epsilon) {
                std::cout << std::setw(7) << side1 << "\t" << std::setw(15)
                          << side2 << "\t" << std::setw(19) << hypotenuse << std::endl;
            }
        }
    }

    return 0; /// indicates that program finished successfully
} /// end of function main

