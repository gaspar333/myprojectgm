/// Exercise5_08
/// Program that that finds the smallest number from inputed values
#include <iostream>
#include <unistd.h>
#include <limits.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) { /// prompting user to input a number for loop process count
        std::cout << "Enter a positive value to specify the number of input process: " << std::endl;
    }
    int count;
    std::cin >> count;
    if (count < 0) {
        std::cout << "Error 1: invalid number. " << std::endl;
        return 1;
    }
    if (0 == count) {
        std::cout << "Nothing is input. Exiting... " << std::endl;
        return 0; /// indicate successful termination without a process
    }
    int smallest = INT_MAX;
    for (int counter = 0; counter < count; ++counter) {
        if (::isatty(STDIN_FILENO)) { /// prompting user to input next value(s)
            std::cout << "Enter an integer: " << std::endl;
        }
        int value;
        std::cin >> value;
        if (value <= smallest) { /// compare smallest with next value integers input
            smallest = value;
        }
    }
    std::cout << "The smallest value is " << smallest << std::endl;
    /// print out the smallest value

    return 0; /// indicate successful termination
} /// end main

