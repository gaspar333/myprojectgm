/// Exercise5_27
/// moderate Fig. 5.14: from chapter 5 to change continue statement
/// into other structured way for skiping the for statement.
#include <iostream>

int
main()
{
    for (int count = 1; count <= 10; ++count) { /// loop 10 times
        if (count != 5) {
            std::cout << count << " ";
        }
    } /// end for
    std::cout << "\nUsed if statement to skip printing 5" << std::endl;
    return 0; /// indicate successful termination
} /// end main

