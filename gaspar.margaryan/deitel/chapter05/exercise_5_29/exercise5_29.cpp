/// Exercise 5_29
/// Peter Minuit Problem
#include <iostream>
#include <iomanip>

int
main()
{
    /// display headers
    const int totalYears = 2021 - 1626;
    for (int percentage = 5; percentage <= 10 ; ++percentage) {
        double amount = 24.0; /// initial amount before
        double rate = static_cast<double>(percentage) / 100;
        std::cout << "Year" << std::setw(23) << "Deposit with rate " 
                  << percentage << " %" << std::endl;
        for (int year = 1; year <= totalYears; ++year) { 
            amount += amount * rate;
            std::cout << std::fixed << std::setprecision(2) << std::setw(4)
                      << year << std::setw(25) << amount << "$ " << std::endl;
        }
        std::cout << std::endl;
    }

    return 0; /// indicate successful termination
} /// end main

