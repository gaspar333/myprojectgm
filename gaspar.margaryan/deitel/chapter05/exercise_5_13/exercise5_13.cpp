/// Exercise5_13
/// Program that inputs 5 valid numbers from 1 to 30 and prints "*" patterns for each number
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) { /// prompting user to input 5 valid numbers from 1 to 30
        std::cout << "Enter 5 numbers from 1 to 30: " << std::endl;
    }
    for (int input = 1; input <= 5; ++input) { /// loop until 5 numbers are input
        int number;
        std::cin >> number;
        if (number < 1 || number > 30) { /// when input number is invalid
	    std::cout << "Error 1: " << number << " is invalid number. Unable to make pattern... ";
            return 1;
	}
        for (int count = 1; count <= number; ++count) {
            std::cout << "*";
        }
        std::cout << std::endl;
    }

    return 0; /// indicate successful termination
} /// end main

