/// Exercise 5_22
/// Program that compares De Morgan's law expressions
#include <iostream>  /// allows the program to perform input and output

/// function main begins program execution
int
main()
{
    int x = 6, y = 3;

    if ((x >= 5) && (y < 7) == !((x < 5) || (y >= 7))) {
        std::cout << "!(x < 5) && !(y >= 7)  is equivalent to     !((x < 5) || (y >= 7))"  << std::endl;
    } else {
        std::cout << "!(x < 5) && !(y >= 7)  is not equivalent to !((x < 5) || (y >= 7))"  << std::endl;
    }

    int a = 2, b = 3, g = 7; 

    if ((a != b) || (g == 5) == !((a == b) && (g != 5))) {
        std::cout << "!(a == b) || !(g != 5) is equivalent to     !((a == b) && (g != 5))" << std::endl;
    } else {
        std::cout << "!(a == b) || !(g != 5) is not equivalent to !((a == b) && (g != 5))" << std::endl;
    }

    x = 9;
    y = 7;

    if (!((x <= 8) && (y > 4)) == (x > 8) || (y <= 4)) {
        std::cout << "!((x <= 8) && (y > 4)) is equivalent to     !(x <= 8) || !(y > 4)"   << std::endl;
    } else {
        std::cout << "!((x <= 8) && (y > 4)) is not equivalent to !(x <= 8) || !(y > 4)"   << std::endl;
    }

    int i = 2, j = 8;

    if (!((i > 4) || (j <= 6)) == (i <= 4) && (j > 6)) {
        std::cout << "!((i > 4) || (j <= 6)) is equivalent to     !(i > 4) && !(j <= 6)"   << std::endl;
    } else {
        std::cout << "!((i > 4) || (j <= 6)) is not equivalent to !(i > 4) && !(j <= 6)"   << std::endl;
    }

    return 0; /// indicates that program finished successfully
} /// end of function main

