/// Exercise5_14
/// Program that calculates and displays the total retail value of products sold
#include <iostream>
#include <unistd.h>

int
main()
{
    double finalResult = 0.0;
    while (true) {
        if (::isatty(STDIN_FILENO)) { /// prompting user to enter the number of product or -1 to exit the program
            std::cout << "Enter the number of product from 1 - 5 (or -1 to exit program): " << std::endl;
        }
	int product;
        std::cin >> product;
	if (-1 == product) { /// sentinel value to make program finish
            break; /// come out from while loop if inputed sentinel value
	}
	if (product < 1 || product > 5) {
            std::cerr << "Error 1: Invalid product number!. " << std::endl;
            return 1;
        }
        if (::isatty(STDIN_FILENO)) { /// prompting user to enter the quintity 
            std::cout << "Enter quantity of product sold (should be positive value): " << std::endl;
        }
        int quantity;
	std::cin >> quantity;
        if (quantity < 0) {
	    std::cerr << "Error 2: Quantity should be positive value! " << std::endl;
            return 2;
        }
        switch (product) {
        case 1: finalResult += quantity * 2.98; break;
        case 2: finalResult += quantity * 4.50; break;
        case 3: finalResult += quantity * 9.98; break;
        case 4: finalResult += quantity * 4.49; break;
        case 5: finalResult += quantity * 6.87; break;
        default: break;
        }

    }
    /// prints the total result
    std::cout << "The total value of products sold is " << '$' << finalResult << std::endl;

    return 0; /// indicate successful termination
} /// end main

