/// Exercise 5_19
/// Program that prints the approximate value of "pi" after each of the first 1,000 terms
#include <iostream>  /// allows the program to perform input and output

/// function main begins program execution
int
main()
{
    double piValue = 3.141592;
    std::cout << "Value of 'pi':" << "\t" << "Terms:\n" << std::endl; /// the header
    int plus = 1; /// pi's +4/n-th value
    int minus = 3; /// pi's -4-th value
    int counter = 1; /// counter for while loop pi's calculation process
    double pi = 0.0; /// the initial value of pi for further calculation
    double epsilon = 0.000001; /// smalles value to compare current pi's floating truncation
    int precision = 100; /// for calculating pi's precision after floating point
    while (precision <= 1000000) { /// limit the process from infinite loop for program to work properly
        pi += 4.0 / plus - 4.0 / minus; /// pi's calculation
        double delta = piValue * precision;
        if ((static_cast<int>(delta) - pi * precision) <= epsilon) {
            precision *= 10;
            std::cout << std::fixed << pi << "\t" << counter << std::endl;
        }
        plus += 4;
        minus += 4;
        ++counter;
    }
    return 0; /// indicates that program finished successfully
} /// end of function main

