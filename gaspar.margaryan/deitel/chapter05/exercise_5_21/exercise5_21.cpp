/// Exercise 5_21
/// Program that calculate employee's pay according to that
/// employee's paycode.
#include <iostream>  /// allows the program to perform input and output
#include <iomanip>
#include <unistd.h>

/// function main begins program execution
int
main()
{
    double total1 = 0; /// total salary of code 1 employees
    double total2 = 0; /// total salary of code 2 employees
    double total3 = 0; /// total salary of code 3 employees
    double total4 = 0; /// total salary of code 4 employees
    int item = 1; /// initalize item number for peaceworkers as a unique type
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Enter employee's code (or -1 to exit): " << std::endl;
        }
        int code;
        std::cin >> code;
        if (-1 == code) {
            break;
        }
        switch (code) {
        case 1: {
            if (::isatty(STDIN_FILENO)) {
                std::cout << "Enter the salary of employee of code No " << code << ": " << std::endl;
            }
            double salary1;
            std::cin >> salary1;
            if (salary1 < 0) {
                std::cout << "Error 1: Invalid data input. " << std::endl;
                return 1;
            }
            total1 += salary1;
            break;
        } /// end of case 1
        case 2: {
            if (::isatty(STDIN_FILENO)) {
                std::cout << "Enter the hourly wage of employee of code No " << code << ": " << std::endl;
            }
            double hourlyWage;
            std::cin >> hourlyWage;
            if (::isatty(STDIN_FILENO)) {
                std::cout << "Enter the hours worked by employee of code No " << code << ": " << std::endl;
            }
            int hours;
            std::cin >> hours;
            if (hourlyWage < 0 || hours < 0) {
                std::cout << "Error 2: Invalid data input. " << std::endl;
                return 2;
            }
            double salary2 = hourlyWage * (hours <= 40 ? hours : hours * 1.5 - 20);
            total2 += salary2;
            break;
        } /// end of case 2
        case 3: {
            if (::isatty(STDIN_FILENO)) {
                std::cout << "Enter the weekly gross sales of employee of code No " << code << ": " << std::endl;
            }
            int grossSales;
            std::cin >> grossSales;
            if (grossSales < 0) {
                std::cout << "Error 3: Invalid data input. " << std::endl;
                return 3;
            }
            double salary3 = 250.0 + (grossSales * 5.7) / 100;
            total3 += salary3;
            break;
        } /// end of case 3
        case 4: {
            if (::isatty(STDIN_FILENO)) { /// 
                std::cout << "Enter the amount of money from item No " << item
                    << " for employee of code No " << code << ": " << std::endl;
            }
            double moneyAmount;
            std::cin >> moneyAmount;
            if (::isatty(STDIN_FILENO)) {
                std::cout << "Enter the produced quantity of item No " << item << ": " << std::endl;
            }
            int quantity;
            std::cin >> quantity;
            if (moneyAmount < 0 || quantity < 0) {
                std::cout << "Error 4: Invalid data input. " << std::endl;
                return 4;
            }
            double salary4 = moneyAmount * quantity;
            total4 += salary4;
            ++item; /// each time when entered code 4 increment item number as a one type of item
            break;
        } /// end of case 4
        default: {
            std::cout << "Error 5: Wrong code." << std::endl;
            return 5;
        }
        }  /// end of switch
    } /// end of while
    std::cout << "Total weekely salary: \n" << "Code 1: " << "\t$" << total1
       << "\nCode 2: " << "\t$" << total2 << "\nCode 3: " << "\t$" << total3
       << "\nCode 4: " << "\t$" << total4 << std::endl;

    return 0; /// indicates that program finished successfully
} /// end of function main

