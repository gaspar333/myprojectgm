/// Exercise5_15
/// Definition of class GradeBook that counts A, B, C, D and F grades.
/// Member functions are defined in main.cpp

#include <string> /// program uses C++ standard string class
/// GradeBook class definition
class GradeBook
{
public:
    GradeBook(std::string name); /// constructor initializes course name
    void setCourseName(std::string name); /// function to set the course name
    std::string getCourseName(); /// function to retrieve the course name
    void displayMessage(); /// display a welcome message
    void inputGrades(); /// input arbitrary number of grades from user
    void displayGradeReport(); /// display a report based on the grades
private:
    std::string courseName_; /// course name for this GradeBook
    int aCount_; /// count of A grades
    int bCount_; /// count of B grades
    int cCount_; /// count of C grades
    int dCount_; /// count of D grades
    int fCount_; /// count of F grades
    double gradeCount_; /// count of all grades
}; /// end class GradeBook

