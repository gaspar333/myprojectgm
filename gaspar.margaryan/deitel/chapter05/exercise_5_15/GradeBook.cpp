/// Exercise5_15
/// Member-function definitions for class GradeBook that
/// uses a switch statement to count A, B, C, D and F grades.
#include <iostream>
#include <unistd.h>
#include <iomanip>
#include "GradeBook.hpp" /// include definition of class GradeBook

/// constructor initializes courseName with string supplied as argument;
/// initializes counter data members to 0
GradeBook::GradeBook(std::string name)
{
    setCourseName(name); /// validate and store courseName
    aCount_ = 0; /// initialize count of A grades to 0
    bCount_ = 0; /// initialize count of B grades to 0
    cCount_ = 0; /// initialize count of C grades to 0
    dCount_ = 0; /// initialize count of D grades to 0
    fCount_ = 0; /// initialize count of F grades to 0
} /// end GradeBook constructor

/// function to set the course name; limits name to 25 or fewer characters
void
GradeBook::setCourseName(std::string name)
{
    courseName_ = name; /// store the course name in the object
    if (name.length() > 25) { /// if name is longer than 25 characters
        /// set courseName to first 25 characters of parameter name
        courseName_ = name.substr(0, 25); /// select first 25 characters
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Name \"" << name << "\" exceeds maximum length (25).\n"
                << "Limiting courseName to first 25 characters.\n" << std::endl;
        }
    } /// end if...else
} /// end function setCourseName

/// function to retrieve the course name
std::string
GradeBook::getCourseName()
{
    return courseName_;
} /// end function getCourseName

/// display a welcome message to the GradeBook user
void
GradeBook::displayMessage()
{
    /// this statement calls getCourseName to get the
    /// name of the course this GradeBook represents
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Welcome to the grade book for\n"
            << getCourseName() << "!\n" << std::endl;
    }
} /// end function displayMessage

/// input arbitrary number of grades from user; update grade counter
void
GradeBook::inputGrades()
{
    int grade; /// grade entered by user
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter the letter grades." << std::endl
            << "Enter the EOF character to end input." << std::endl;
    }
    /// loop until user types end-of-file key sequence
    gradeCount_ = 0.0;
    while ((grade = std::cin.get()) != EOF) {
        /// determine which grade was entered
        switch (grade) { /// switch statement nested in while
        case 'A': case 'a': ++aCount_; gradeCount_ += 4; break;
        case 'B': case 'b': ++bCount_; gradeCount_ += 3; break;
        case 'C': case 'c': ++cCount_; gradeCount_ += 2; break; 
        case 'D': case 'd': ++dCount_; gradeCount_ += 1; break;
        case 'F': case 'f': ++fCount_; break; 
        case '\n': case '\t': case ' ': break; /// ignore newlines, tabs,and spaces in input
        default: /// catch all other characters
            if (::isatty(STDIN_FILENO)) {
                std::cout << "Incorrect letter grade entered."
                    << " Enter a new grade." << std::endl;
            }
                break; /// optional; will exit switch anyway
        } /// end switch
    } /// end while
} /// end function inputGrades

/// display a report based on the grades entered by user
void
GradeBook::displayGradeReport()
{
    /// output summary of results
    std::cout << "Number of students who received each letter grade:"
        << "\nA: " << aCount_ /// display number of A grades
        << "\nB: " << bCount_ /// display number of B grades
        << "\nC: " << cCount_ /// display number of C grades
        << "\nD: " << dCount_ /// display number of D grades
        << "\nF: " << fCount_ /// display number of F grades
        << std::endl;
    double averageGrade = 0.0;
    int countSum = aCount_ + bCount_ + cCount_ + dCount_ + fCount_;
    if (countSum > 0) {
        averageGrade = gradeCount_ / countSum;
    }
    std::cout << "The grade point average is " << std::fixed << std::setprecision(2) 
        << averageGrade << std::endl;
} /// end function displayGradeReport

