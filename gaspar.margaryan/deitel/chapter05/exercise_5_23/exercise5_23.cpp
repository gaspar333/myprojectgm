/// Exercise 5_23
/// Program that print out a pattern with diamond shape 
#include <iostream>  /// allows the program to perform input and output
#include <cmath>

/// function main begins program execution
int
main()
{
    int size = 9;
    int halfSize = size / 2; /// initialize the half size
    for (int column = halfSize; column >= -halfSize; --column) {
        int range = halfSize - std::abs(column);
        for (int row = -halfSize; row <= halfSize; ++row) {
            std::cout << ((row >= -range && row <= range) ? "*" : " ");
        } /// end of inner for
        std::cout << "\n"; /// new line
    } /// end of outer for

    return 0; /// indicates that program finished successfully
} /// end of function main

