There are several ways to replace break statement
- using return 0
  in this case the program will just end. There are some tasks
  that are needed just to brake a loop operation once, then continue
  the task ignorint a process or data we wanted to skip.
- using sentinel value
  We know that each repetition control statement such as while can
  be used instead of for. While statements are best for sentinel
  controling. Which means the best alternate for break statement is
  sentinel value.
- using goto
  Though goto statement is out of structured programming, and many
  modern programing lessons propagate not to use goto, but as for
  one of source programming statements it can be used.
