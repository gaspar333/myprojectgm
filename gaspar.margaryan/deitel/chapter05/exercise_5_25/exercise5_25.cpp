/// Exercise5_25
/// Moderate Fig. 5.13: from chapter 5 to change break statement
/// into other structured way for exiting the for statement.
#include <iostream>

int
main()
{
    int count = 1;
    while (count != 5) { /// loop until meet sentinel value
        std::cout << count << " ";
        ++count;
        if (5 == count) {
            std::cout << "\nExit the loop at count = " << count << std::endl;
        }
    } /// end while
    
    return 0; /// indicate successful termination
} /// end main

