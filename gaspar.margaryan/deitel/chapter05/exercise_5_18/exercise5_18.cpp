/// Exercise 5_18
/// Program that print table of decimal numbers from 1 to 256 into binary,
/// octal and hexadecimal equivalent
#include <iostream>  /// allows the program to perform input and output

/// function main begins program execution
int
main()
{
    /// table header
    std::cout << "Number\t" << "Binary\t\t" << "Octal\t" << "Hexadecimal" << std::endl;
    for (int number = 1; number <= 256; ++number) { /// count binary equivalent
        std::cout << number << "\t";
        for (int radix = 2; radix <= 16; radix *= 2) {
            if (4 == radix) { /// skip the loop when redix is 4
                continue;
            }
            for (int binary = 512; binary >= 1; binary /= radix) {
                int value = (number / binary) % radix;
                if (16 == radix) { /// value correction when radix is 16
                    value = (number / (binary / 2)) % radix;
                }
                switch (value) { /// print hexadeciaml letters if value is higher than 9
                case 10: std::cout << 'a'; break;
                case 11: std::cout << 'b'; break;
                case 12: std::cout << 'c'; break;
                case 13: std::cout << 'd'; break;
                case 14: std::cout << 'e'; break;
                case 15: std::cout << 'f'; break;
                default: std::cout << value; break;
                }  /// end of switch
            } /// end of 3-rd inner for
            16 == radix ? std::cout << std::endl : std::cout << "\t"; /// print new line instead of next tabulation 
        } /// end of 2-nd inner for
    } /// end of outer for
    return 0; /// indicates that program finished successfully
} /// end of function main

