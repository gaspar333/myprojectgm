/// Exercise 5_28
/// Program that prints "The Twelve Days of Christmas" Song
/// using loop and switch statements.
#include <iostream>  /// allows the program to perform input and output

/// function main begins program execution
int
main()
{
    for (int days = 1; days <= 12; ++days) {
        std::cout << "On the "; 
        switch (days) {
        case 1:  std::cout << "first";    break;
        case 2:  std::cout << "second";   break;
        case 3:  std::cout << "third";    break;
        case 4:  std::cout << "forth";    break;
        case 5:  std::cout << "fifth";    break;
        case 6:  std::cout << "sixth";    break;
        case 7:  std::cout << "seventh";  break;
        case 8:  std::cout << "eighth";   break;
        case 9:  std::cout << "ninth";    break;
        case 10: std::cout << "tenth";    break;
        case 11: std::cout << "eleventh"; break;
        case 12: std::cout << "twelfth";  break;
        default: break;
        } /// end of switch
        std::cout << " day of Christmas\n"
                  << "my true love sent to me:" << std::endl;
        /// print the first two rows of song with proper day number
        switch (days) {
        case 12: std::cout << "12 Drummers Drumming"        << std::endl;
        case 11: std::cout << "Eleven Pipers Piping"        << std::endl;
        case 10: std::cout << "Ten Lords a Leaping"         << std::endl;
        case 9:  std::cout << "Nine Ladies Dancing"         << std::endl;
        case 8:  std::cout << "Eight Maids a Milking"       << std::endl;
        case 7:  std::cout << "Seven Swans a Swimming"      << std::endl;
        case 6:  std::cout << "Six Geese a Laying"          << std::endl;
        case 5:  std::cout << "Five Golden Rings"           << std::endl;
        case 4:  std::cout << "Four colly birds"            << std::endl;
        case 3:  std::cout << "Three French Hens"           << std::endl;
        case 2:  std::cout << "Two Turtle Doves" << std::endl;
                 std::cout << "and ";
        case 1:  std::cout << "A Partridge in a Pear Tree." << std::endl;
        default: break;
        } /// end of switch
        std::cout << std::endl; /// new line after each loop
    } /// end of for

    return 0; /// indicates that program finished successfully
} /// end of function main

