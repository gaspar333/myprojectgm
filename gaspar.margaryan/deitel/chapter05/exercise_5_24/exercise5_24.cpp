/// Exercise 5_24
/// Program that print out a pattern with diamond shape by given odd numbers 
#include <iostream>  /// allows the program to perform input and output
#include <unistd.h>

/// function main begins program execution
int
main()
{
    if (::isatty(STDIN_FILENO)) { /// promt user to input an integer
       std::cout << "Enter an odd integer for size (from 1 to 19): " << std::endl;
    }
    int size;
    std::cin >> size;
    if (0 == size % 2 || (size < 1 || size > 19)) { /// print error and exit when enter wrong integer
        std::cerr << "Error 1: Invalid input! " << std::endl;
        return 1;
    }
    int halfSize = size / 2 + 1; /// initialize the half size of odd integers
    for (int column = 1; column <= size; ++column) {
        for (int row = size; row >= 1; --row) {
            if (column <= halfSize) { /// pattern shape fade in
                if (row <= (halfSize - column)) {
                    break;
                }
                std::cout << (( row >= halfSize + column) ? " " : "*");
            } else {
                if (row <= (column - halfSize)) {
                    break;
                }
                std::cout << (row > size - (column - halfSize) ? " " : "*");
            /// pattern shape fade out
            } /// end of inner if else statement
        } /// end of inner for
        std::cout << "\n"; /// new line
    } /// end of outer for

    return 0; /// indicates that program finished successfully
} /// end of function main

