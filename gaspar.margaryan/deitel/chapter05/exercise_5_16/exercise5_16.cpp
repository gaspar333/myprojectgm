/// Exercise 5_16
/// Modified program for compound interest calculations with for,
/// integer type input in dollars and cents

#include <iostream>
#include <iomanip>

int
main()
{
    int rate = 5;
    /// display headers
    std::cout << "Year" << std::setw(24) << "Amount on deposit" << std::endl;
    int amount = 100000; /// initial amount before interest in cents
    for (int year = 1; year <= 10; ++year) { /// calculate amount for specified year
        amount += amount * rate / 100;
        /// display the year and the amount in dollars and cents
        std::cout << std::setw(4) << year << std::setw(21) << amount / 100 << ".";
        int amountInCents = amount % 100;
        if (amountInCents < 10) { /// if cent is less than 10 print 0 before value (e.g. 03)
            std::cout << "0";
        }
        std::cout << amountInCents << std::endl;
    } /// end for

    return 0; /// indicate successful termination
} /// end main

