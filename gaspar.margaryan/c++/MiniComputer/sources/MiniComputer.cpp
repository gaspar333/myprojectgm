#include <iostream>
#include <cassert>
#include <unistd.h>
#include <stdlib.h>

#include "headers/MiniComputer.hpp"

MiniComputer::MiniComputer()
{
    isInteractive_ = ::isatty(STDIN_FILENO);
    programName_ = "Mini Computer";
    version_ = "Version 1.0";
}

int
MiniComputer::run()
{
    /// Print program name and version
    /// Print main menu
    /// Get command from user
    /// Execute the command
    /// Start again from main menu
    printProgramNameAndVersion();
    
    int command = -1;
    while (command != 0) {
        printMenu(MAIN_MENU);
        command = getInputFromUser("Command");
        executeCommand(command);
    }

    return 0;
}

inline void
MiniComputer::printProgramNameAndVersion()
{
    std::cout << programName_ << " - " << version_ << std::endl;
}

void
MiniComputer::printMenu(const Menu menu)
{
    if (!isInteractive_) {
        return;
    }

    assert(menu < MENU_SIZE && "Wrong menu");

    static const std::string MENU_TITLE[MENU_SIZE] = {
        /*MAIN_MENU*/           "Main Menu",
        /*LOAD_MENU*/           "Load Menu",
        /*PRINT_MENU*/          "Print Menu",
        /*ADD_MENU*/            "Add Menu (Register 1 + Register 2 = Register 3)",
        /*PRINT_REGISTER_MENU*/ "Registers"
    };
    static const size_t MENU_ITEMS_SIZE[MENU_SIZE] = {
        /*MAIN_MENU*/           5,
        /*LOAD_MENU*/           REGISTER_SIZE,
        /*PRINT_MENU*/          2,
        /*ADD_MENU*/            REGISTER_SIZE,
        /*PRINT_REGISTER_MENU*/ REGISTER_SIZE
    };
    static const size_t MENU_ITEMS_MAX_SIZE = 5; /// Later on we will change this
    static const std::string MENU_ITEMS[MENU_SIZE][MENU_ITEMS_MAX_SIZE] = {
        /*MAIN_MENU*/           { "Exit", "Load", "Store", "Print", "Add" },
        /*LOAD_MENU*/           { "a", "b", "c", "d" },
        /*PRINT_MENU*/          { "Register", "String" },
        /*ADD_MENU*/            { "a", "b", "c", "d" },
        /*PRINT_REGISTER_MENU*/ { "a", "b", "c", "d" } /// The later on we will solve this duplication
    };

    std::cout << MENU_TITLE[menu] << '\n';
    for (size_t i = 0; i < MENU_ITEMS_SIZE[menu]; ++i) {
        printMenuItem(MENU_ITEMS[menu], i);
    }
}

void
MiniComputer::printLoadIntoMenu(const int registerNumber)
{
    if (isInteractive_) {
        const char registerName = getRegisterName(registerNumber);
        std::cout << "Load Into " << registerName << '\n';
        std::cout << "\tInput the value to load into register " << registerName << '\n';
    }
}

void
MiniComputer::printMenuItem(const std::string menu[], const size_t index)
{
    std::cout << '\t' << index << " - " << menu[index] << '\n';
}

inline int
MiniComputer::getInputFromUser(const std::string& prompt)
{
    if (isInteractive_) {
        std::cout << "> " << prompt << ": ";
    }

    int command;
    std::cin >> command;
    return command;
}

inline char
MiniComputer::getRegisterName(const int registerNumber)
{
    return 'a' + static_cast<char>(registerNumber);
}

int
MiniComputer::getRegisterValue(const int registerNumber)
{
    assert(registerNumber < REGISTER_SIZE && "Assert 1: Invalid register");
    return register_[registerNumber];
}

std::string
MiniComputer::getStringFromUser(const std::string& prompt, const int stringLength)
{
    if (isInteractive_) {
        std::cout << "> " << prompt << ": ";
    }
    std::string text;
    std::cin.ignore();
    for (int i = 0; i < stringLength; ++i) {
        char character;
        std::cin.get(character);
        text += character;
    }
    return text;
}

void
MiniComputer::checkRegister(const int registerNumber)
{
    if (registerNumber < 0 || registerNumber >= REGISTER_SIZE) {
        std::cout << "Error 2: Invalid register" << std::endl;
        ::exit(2);
    }
}

void
MiniComputer::setRegisterValue(const int registerNumber, const int registerValue)
{
    assert(registerNumber < REGISTER_SIZE && "Assert 1: Invalid register");
    register_[registerNumber] = registerValue;
}

void
MiniComputer::executeCommand(const int command)
{
    switch (command) {
    case 0: executeExitCommand();  break;
    case 1: executeLoadCommand();  break;
    case 2: executeStoreCommand(); break;
    case 3: executePrintCommand(); break;
    case 4: executeAddCommand();   break;
    default:
        std::cout << "Error 1: Invalid command" << std::endl;
        ::exit(1);
    }
}

void
MiniComputer::executeExitCommand()
{
    std::cout << "Exiting..." << std::endl;
}

void
MiniComputer::executeLoadCommand()
{
    printMenu(LOAD_MENU);
    const int registerNumber = getInputFromUser("Register");
    checkRegister(registerNumber);
    executeLoadIntoCommand(registerNumber);
}

void
MiniComputer::executeLoadIntoCommand(int registerNumber)
{
    printLoadIntoMenu(registerNumber);
    /// call std::string(int count, char symbol) constructor to convert from char to std::string
    std::string registerName = std::string(1, getRegisterName(registerNumber));
    const int registerValue = getInputFromUser(registerName);
    executeLoadIntoRegisterCommand(registerNumber, registerValue);
}

void
MiniComputer::executeLoadIntoRegisterCommand(int registerNumber, int registerValue)
{
    setRegisterValue(registerNumber, registerValue);
    std::cout << getRegisterName(registerNumber) << " = "
              << getRegisterValue(registerNumber) << std::endl;
}

void
MiniComputer::executeStoreCommand()
{
    assert(0);
}

void
MiniComputer::executePrintCommand()
{
    printMenu(PRINT_MENU);
    const int print = getInputFromUser("Choose");
    if (0 != print && 1 != print) {
        std::cerr << "Error 3: Invalid menu item:" << std::endl;
    }
    (0 == print) ? executePrintRegister() : executePrintString();
}

void
MiniComputer::executePrintRegister()
{
    printMenu(PRINT_REGISTER_MENU);
    std::cout << getRegisterValue(getInputFromUser("Register"));
}

void
MiniComputer::executePrintString()
{
    const int stringLength = getInputFromUser("String length");
    const std::string text = getStringFromUser("String", stringLength);
    std::cout << text;
}

void
MiniComputer::executeAddCommand()
{
    printMenu(ADD_MENU);

    const int register1Number = getInputFromUser("Register 1");
    checkRegister(register1Number);

    const int register2Number = getInputFromUser("Register 2");
    checkRegister(register2Number);

    const int register3Number = getInputFromUser("Register 3");
    checkRegister(register3Number);

    executeAdd(register1Number, register2Number, register3Number);
}

void
MiniComputer::executeAdd(const int register1Number, const int register2Number, const int register3Number)
{
    const int register1Value = getRegisterValue(register1Number);
    const int register2Value = getRegisterValue(register2Number);
    const int register3Value = register1Value + register2Value;
    setRegisterValue(register3Number, register3Value);

    const char register1Name = getRegisterName(register1Number);
    const char register2Name = getRegisterName(register2Number);
    const char register3Name = getRegisterName(register3Number);

    std::cout << register3Name  << " = "
              << register1Name  << " + " << register2Name  << " = "
              << register1Value << " + " << register2Value << " = "
              << register3Value << std::endl;
}

