#include <string>

class MiniComputer
{
private:
    enum Menu {
        MAIN_MENU,
        LOAD_MENU,
        PRINT_MENU,
        ADD_MENU,
        PRINT_REGISTER_MENU,
        MENU_SIZE
    };

public:
    MiniComputer();
    int run();

private:
    void printProgramNameAndVersion();
    void printMenu(const Menu menu);
    void printLoadIntoMenu(const int registerNumber);
    void printMenuItem(const std::string menu[], const size_t index);
    int getInputFromUser(const std::string& prompt);
    char getRegisterName(const int registerNumber);
    int getRegisterValue(const int registerNumber);
    std::string getStringFromUser(const std::string& prompt, const int stringLength);
    void setRegisterValue(const int registerNumber, const int registerValue);
    void checkRegister(const int registerNumber);
    void executeCommand(const int command);
    void executeExitCommand();
    void executeLoadCommand();
    void executeLoadIntoCommand(const int registerNumber);
    void executeLoadIntoRegisterCommand(const int registerNumber, const int registerValue);
    void executeStoreCommand();
    void executePrintCommand();
    void executePrintRegister();
    void executePrintString();
    void executeAddCommand();
    void executeAdd(const int register1Number, const int register2Number, const int register3Number);

private:
    static const int REGISTER_SIZE = 4;
    int register_[REGISTER_SIZE];
    bool isInteractive_;
    std::string programName_;
    std::string version_;
};
