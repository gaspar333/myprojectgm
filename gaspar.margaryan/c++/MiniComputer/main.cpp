#include "headers/MiniComputer.hpp"

int
main()
{
    /// Create & Run the Mini Computer
    MiniComputer mini;
    return mini.run();
}

